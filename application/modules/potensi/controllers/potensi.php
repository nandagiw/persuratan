<?php
class Potensi extends CI_Controller{
    
    public function __construct() {
       
        parent::__construct();
		$this->atos_tiasa_leubeut();

    }
    
	public function atos_tiasa_leubeut(){
		
		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}

    }
	
    public function index() {
	
		$data["test"]="";
		
	    // $sql_potensi = "select 
					// a.*,
					// (select (count(z.id_jawab_kuesioner) * 100) / 
					// (select count(z.id_jawab_kuesioner) from tr_jawab_kuesioner z, tr_survey y, tr_detail_survey b, ref_peruntukkan c, ref_kategori d where b.kat_izin=2 and y.status_transaksi !='3' and z.kode_transaksi=b.kode_transaksi and b.id_peruntukkan=c.id_peruntukkan and c.id_param=a.id_param and d.id_kategori=b.kat_izin and z.kode_transaksi=y.kode_transaksi) as nett_tidakberizin 
					// from tr_jawab_kuesioner z, tr_survey y, tr_detail_survey b, ref_peruntukkan c, ref_kategori d where z.jawaban='Tidak' and y.status_transaksi !='3' and b.kat_izin=2 and z.kode_transaksi=b.kode_transaksi and b.id_peruntukkan=c.id_peruntukkan and c.id_param=a.id_param and d.id_kategori=b.kat_izin and z.kode_transaksi=y.kode_transaksi)
					// from m_parameter_potensi a";

		$sql_potensi = "select 
						a.*,
						(select distinct (select (count(z.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner z, tr_survey y, tr_detail_survey b, ref_peruntukkan c, ref_kategori d where z.jawaban='Tidak' and y.status_transaksi !='3' and b.kat_izin=2 and z.kode_transaksi=b.kode_transaksi and b.id_peruntukkan=c.id_peruntukkan and c.id_param=a.id_param and d.id_kategori=b.kat_izin and z.kode_transaksi=y.kode_transaksi)  

						/ (select  count(z.id_jawab_kuesioner) from tr_jawab_kuesioner z, tr_survey y, tr_detail_survey b, ref_peruntukkan c, ref_kategori d where b.kat_izin=2 and y.status_transaksi !='3' and z.kode_transaksi=b.kode_transaksi and b.id_peruntukkan=c.id_peruntukkan and c.id_param=a.id_param and d.id_kategori=b.kat_izin and z.kode_transaksi=y.kode_transaksi) as nett_tidakberizin

						from tr_jawab_kuesioner z, tr_survey y, tr_detail_survey b, ref_peruntukkan c, ref_kategori d where y.status_transaksi !='3' and b.kat_izin=2 and z.kode_transaksi=b.kode_transaksi and b.id_peruntukkan=c.id_peruntukkan and c.id_param=a.id_param and d.id_kategori=b.kat_izin and z.kode_transaksi=y.kode_transaksi)
						from m_parameter_potensi a";

		$data['ListData'] = $this->db->query($sql_potensi)->result_array();
		
		$this->template->load('rorompok','v_index_param_potensi',$data);
		
    }

    function tambah(){

    	$data['judul_form']="Tambah Data Parameter Potensi";
		$data['sub_judul_form']="";

		$query = "select 
				  a.*,
				  (select (count(z.id_jawab_kuesioner) / (select count(*) from tr_jawab_kuesioner z where z.jawaban='Ya' ) * 100) as nett_tidakberizin from tr_jawab_kuesioner z, m_kuesioner w, m_pilihan_jawaban x, tr_survey y where z.id_survey=y.id_survey and w.id_kuesioner=z.id_kuesioner and x.id_pil_jawab=z.id_pil_jawab)
				  from m_parameter_potensi a";

		$maxid = "select max(a.id_param) + 1 as auto_id_param from m_parameter_potensi a";

		$query1 = $this->db->query($query);
		$query2 = $this->db->query($maxid);
   		$data['field1'] = $query1->row_array();
   		$data['field2'] = $query2->row_array();
		$this->template->load('rorompok','v_add_param_potensi',$data);

    }
	
	function ubah(){

		
		$id_param = $this->input->post('id_param');
		$id_param=$this->uri->segment(3);
   		$data['judul_form']="Ubah Data Paramater Potensi";
		$data['sub_judul_form']="";

		$sql1 = "select 
					a.*,
					(select (count(z.id_jawab_kuesioner) / (select count(*) from tr_jawab_kuesioner z 
					where z.jawaban='Ya' ) * 100) as nett_tidakberizin from tr_jawab_kuesioner z,
					m_kuesioner w, m_pilihan_jawaban x, tr_survey y where z.id_survey=y.id_survey 
					and w.id_kuesioner=z.id_kuesioner and x.id_pil_jawab=z.id_pil_jawab)
					from m_parameter_potensi a where a.id_param='".$id_param."' ";

		$query1 = $this->db->query($sql1);
   		$data['field1'] = $query1->row_array();
		$this->template->load('rorompok','v_update_param_potensi',$data);

	}

	function ubahSatuanRupiah(){

		$id_param = $this->input->post('id_param');
		$id_param=$this->uri->segment(3);
   		$data['judul_form']="Ubah Data Paramater Potensi";
		$data['sub_judul_form']="";

		$sql1 = "SELECT A.nilai_rupiah
				FROM m_parameter_potensi A ";

		$query1 = $this->db->query($sql1);
   		$data['field1'] = $query1->row_array();
		$this->template->load('rorompok','v_update_rupiah',$data);

	}

	function hapus($id_param){
		   		
		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 14-Des-2016
		Desc     : Proses Menghapus Data By ID parameter potensi
		*/

		$id_param = $this->uri->segment(3);

		$this->db->where('id_param', $id_param);
		$this->db->delete('m_parameter_potensi');

		$data['judul_form']="Tambah Data Parameter Potensi";
		$data['sub_judul_form']="";
		//  $data['sub_judul_form']="Data KUesioner";

		$this->session->set_flashdata('message_sukses', 'Data Parameter Potensi Berhasil Dihapus');
   		redirect('potensi');
	}

	function detail(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 14-Des-2016
		Desc     : Proses Menghapus Data By ID
		*/

		$id_param = $this->uri->segment(3);

		$id_user=$this->uri->segment(3);
		
		$per_page = 30;  

		$qry = "select 
				a.*,
				(select (count(z.id_jawab_kuesioner) / (select count(*) from tr_jawab_kuesioner z 
				where z.jawaban='Ya' ) * 100) as nett_tidakberizin from tr_jawab_kuesioner z, m_kuesioner w,
				m_pilihan_jawaban x, tr_survey y where z.id_survey=y.id_survey and 
				w.id_kuesioner=z.id_kuesioner and x.id_pil_jawab=z.id_pil_jawab)
				from m_parameter_potensi a where a.id_param='".$id_param."'order by a.id_param asc";

		$offset = ($this->uri->segment(4) != '' ? $this->uri->segment(4):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/potensi/index_log/'.$id_user.'/'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(4);      
		$this->data['offset'] = $offset ;
		
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}

		$qry .= " limit {$per_page} offset {$offset} ";
		$this->data['ListData'] = $this->db->query($qry)->result_array();     
		
	    $query = $this->db->query($qry);
	    $this->data['field_kuesioner'] = $query->row_array();
		
		$this->data['sub_judul_form']="Detail Potensi";	
		$this->template->load('rorompok','v_index_log',$this->data);

	}

	function cari(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 15-Des-2016
		Desc     : Operasi mencari data parameter potensi berdasarkan keyword	
		*/

		$this->data['sub_judul_form']="Hasil Pencarian";
		
		$id_user=$this->uri->segment(3);

		$nama_param = $_POST['nama_param'];

		$qry2 = " SELECT 
				A.*
				FROM
				m_parameter_potensi A
				WHERE A.nama_param LIKE '%$nama_param%' ";

		$data['ListData1'] = $this->db->query($qry2)->result_array();  
		$this->template->load('rorompok','v_param_potensi_search',$data);

	}

	function simpanTambahPotensi(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 14-Des-2016
		Desc     : Operasi Simpan (Tambah Data Parameter Potensi)
		*/

		 try {

				$id_param = $this->input->post('id_param');
				
				if($id_param != null){
				
				$query = $this->db->query("SELECT A.* 
											FROM m_parameter_potensi A");
				$cek=$query->num_rows(); 

					$data = array(			
					'id_param' 			=> $this->input->post('id_param'),
					'nama_param' 		=> $this->input->post('nama_param'),
					'nilai_koefisien' 	=> $this->input->post('nilai_koefisien'),
					'nilai_rupiah' 		=> $this->input->post('nilai_rupiah'),
					'pers_netto_berizin'=> $this->input->post('pers_netto_berizin')
					);
					
					$this->db->insert('m_parameter_potensi', $data);
					
				$this->session->set_flashdata('message_sukses', 'Data Parameter Potensi Berhasil Ditambah');
				redirect('potensi');

				} else {
					$this->session->set_flashdata('message_gagal', 'Username Yang Anda Masukan Sudah Ada, Silahkan Ganti Dengan Yang Lain !!!');
					$this->tambah();
				
				}

			}
			catch(Exception $err)
			{
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}

	}

	function simpanUbahPotensi(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 14-Des-2016
		Desc     : Operasi Update / Perbarui Data Parameter Potensi
		*/

		 try {

				$id_param = $this->input->post('id_param');
				if($id_param != null){
				
				$query = $this->db->query("SELECT A.*,
											B.id_pil_jawab,
											B.nama_pil_jawab 
											FROM m_kuesioner A
											LEFT JOIN m_pilihan_jawaban B ON A.id_kuesioner=B.id_kuesioner ");
				$cek=$query->num_rows(); 

				$data = array(
					'id_param' 			=> $this->input->post('id_param'),
					'nama_param' 		=> $this->input->post('nama_param'),
					'nilai_koefisien' 	=> $this->input->post('nilai_koefisien'),
					'nilai_rupiah' 		=> $this->input->post('nilai_rupiah'),
					'pers_netto_berizin'=> $this->input->post('pers_netto_berizin')
				);

				// $inserts = $this->db->insert_id();
				
				$this->db->where('id_param', $id_param);
				$this->db->update('m_parameter_potensi', $data);
				}else{
					echo "salah";
				}
				$this->session->set_flashdata('message_sukses', 'data parameter potensi berhasil diubah');
				redirect('potensi');

			}
			catch(Exception $err)
			{
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}

	}

	function simpanUbahSatuanRupiah(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 14-Des-2016
		Desc     : Operasi Update / Perbarui Data Parameter Potensi
		*/

		 try {

				$nilai_rupiah = $this->input->post('nilai_rupiah');

				if($nilai_rupiah != null){
					$query = $this->db->query("SELECT A.nilai_rupiah
												FROM m_parameter_potensi A ");
					$cek=$query->num_rows(); 

					$data = array(
						'nilai_rupiah' => $this->input->post('nilai_rupiah')
					);

					$this->db->update('m_parameter_potensi', $data);
				}else{
					echo "salah";
				}
				$this->session->set_flashdata('message_sukses', 'data nilai rupiah berhasil diubah');
				redirect('potensi');

			}
			catch(Exception $err)
			{
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}

	}

}