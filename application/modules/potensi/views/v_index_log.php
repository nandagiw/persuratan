			<div class="container-fluid" style="margin-top: 45px;">
				<br>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>kuesioner">Data Master Parameter Potensi</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="">Detail Data Parameter Potensi</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-title">
							<h3>
								<i class="icon-reorder"></i>
									<?php echo $sub_judul_form;?>
							</h3>
						</div>
						<div class="box-content">
		
						<table width="100%" class="table table-hover">
	    					<thead>
								<tr>
									<th>ID Parameter</th>
									<th>Nama Parameter</th>
									<th>Nilai Koefisien</th>
									<th>Nilai Rupiah</th>
									<th>Persentase Netto Berizin</th>
									<th>Persentase Netto Tidak Berizin</th>
	    						</tr>
							</thead>
							<tbody>
								<?php
								if (count($ListData) > 0) {
									foreach($ListData as $row)
									{
								
									?>

								<tr>
									<td><?php echo $row['id_param']; ?></td>
				  					<td><?php echo $row['nama_param']; ?></td>
									<td><?php echo $row['nilai_koefisien']; ?></td>
									<td><?php echo $row['nilai_rupiah']; ?></td>
									<td><?php echo $row['pers_netto_berizin']; ?></td>
									<td><?php echo $row['nett_tidakberizin']; ?></td>
								</tr>

								<?php
								
								$paging=(!empty($pagermessage) ? $pagermessage : '');
										
									}
									echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
								} else {
									echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
								}
								?>
							</tbody>
						</table>	
					</form>		
				</div>
			</div>
		</div>
	</div> 
