	<link rel="stylesheet" href="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
	<script src="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
	<script src="<?php echo base_url(); ?>https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

	<div class="container-fluid" style="margin-top: 45px;">
		<br>
		<div class="breadcrumbs">
			<ul>
				<li>
					<a href="<?php echo site_url();?>kuesioner">Data Pertanyaan Kuesioner</a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#">Tambah Data Kuesioner</a>
					<i class="icon-angle-right"></i>
				</li>
				
			</ul>
			<div class="close-bread">
				<a href="#"><i class="icon-remove"></i></a>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12">
			<div class="box">
				<div class="box-title">
					<h3><i class=" icon-plus-sign"></i><?php echo $judul_form." ".$sub_judul_form;?> </h3>
				</div>       
 				<div class="box-content">
 
					<!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
					<?php echo form_open('potensi/simpanUbahPotensi',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
						
						<?php 
	                        if ($this->session->flashdata('message_gagal')) {
	                        echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
	                        }

	                        if ($this->session->flashdata('message_sukses')) {
	                        echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
	                        }
	                    ?>
											
						<input type="hidden" name="id_kuesioner" id="id_kuesioner" class="input-xxlarge"  value="<?php echo isset($field1['id_kuesioner'])?$field1['id_kuesioner']:'';?>">

						<input type="hidden" name="id_pil_jawab" id="id_pil_jawab" class="input-xxlarge"  value="<?php echo isset($field1['id_pil_jawab'])?$field1['id_pil_jawab']:'';?>">

						<?php 
						if (isset($field1['id_kuesioner'])) { $dis="disabled";} else { $dis="";}							  
						$nama_pil_jawab= isset($field1['nama_pil_jawab'])?$field1['nama_pil_jawab']:$this->input->post('nama_pil_jawab'); 
						$status_aktif= isset($field1['status_aktif'])?$field1['status_aktif']:$this->input->post('status_aktif'); 
						$id_pil_jawab= isset($field1['id_pil_jawab'])?$field1['id_pil_jawab']:$this->input->post('id_pil_jawab'); 	
				  		?>
	                    <div class="control-group">
							<label for="textfield" class="control-label">ID Parameter</label>
							<div class="controls">
								<!-- <?php echo "tes : ".$id_pil_jawab; ?> -->
								<input type="text" name="id_param" id="id_param" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['id_param'])?$field1['id_param']:'';?>">
							</div>
						</div>
							
					 	<div class="control-group">
							<label for="textfield" class="control-label">Nama Parameter</label>
							<div class="controls">
								<input type="text" name="nama_param" id="nama_param" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['nama_param'])?$field1['nama_param']:'';?>">
							</div>
						</div>

						 <div class="control-group">
							<label for="textfield" class="control-label">Nilai Koefisien</label>
							<div class="controls">
								
								<input type="text" name="nilai_koefisien" id="nilai_koefisien" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['nilai_koefisien'])?$field1['nilai_koefisien']:'';?>">
							</div>
						</div>

						<div class="control-group">
							<label for="textfield" class="control-label">Nilai Rupiah</label>
							<div class="controls">
								
								<input type="text" name="nilai_rupiah" id="nilai_rupiah" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['nilai_rupiah'])?$field1['nilai_rupiah']:'';?>" readonly="readonly">
							</div>
						</div>

						<div class="control-group">
							<label for="textfield" class="control-label">Persentase Netto Berizin</label>
							<div class="controls">
								
								<input type="text" name="pers_netto_berizin" id="pers_netto_berizin" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['pers_netto_berizin'])?$field1['pers_netto_berizin']:'';?>" >
							</div>
						</div>

						<div class="form-actions">
							<button class="btn btn-primary" type="submit">Simpan</button>
	                        <a class="btn btn-danger" href="<?php echo site_url();?>kuesioner/">Kembali</a>
							
						</div>
					</div>       
				</div>
			</div>
		</div>