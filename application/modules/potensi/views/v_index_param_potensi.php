		<div class="container-fluid" style="margin-top: 45px;">
			<div class="page-header">
				<div class="pull-left">
					<h1>Data Parameter Potensi</h1>
				</div>
				<div class="pull-right">
					<ul class="stats">
						<li class="lightred">
							<i class="icon-calendar"></i>
							<div class="details">
								<span class="big">October 20, 2016</span>
								<span>Thursday, 11:17</span>
							</div>
						</li>
					</ul>
				</div>
			</div>
				
            <div class="breadcrumbs">
				<ul>
					<li>
						<a href="#">Setting</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo site_url();?>kuesioner">Data Parameter Potensi</a>
						<i class="icon-angle-right"></i>
					</li>
				</ul>
				<div class="close-bread">
					<a href="#"><i class="icon-remove"></i></a>
				</div>
			</div>
		</div>

		<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">
								<form action="<?php echo site_url('potensi/cari'); ?>" method="post" name="form1" class="form-horizontal form-bordered">

									<?php 
                                        if ($this->session->flashdata('message_gagal')) {
                                        echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                    }

                                        if ($this->session->flashdata('message_sukses')) {
                                        echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                        }

                                   ?>

							<div align="right">
                            <a class="btn btn-warning" href="<?php echo site_url();?>potensi/ubahSatuanRupiah"><i class="icon-plus-sign"></i>Ubah Satuan Rupiah</a>
                            <a class="btn btn-green" href="<?php echo site_url();?>potensi/tambah"><i class="icon-plus-sign"></i>Tambah Data Parameter</a>
                            </div>

                            
							<?php 
								if (isset($field['id_kuesoner'])) { $dis="disabled";} else { $dis="";}				  
								$pertanyaan= isset($field['pertanyaan'])?$field['pertanyaan']:$this->input->post('pertanyaan');				
							?>

                            <?php 
								if (isset($field['id_kuesoner'])) { $dis="disabled";} else { $dis="";}			  
								$nama_param= isset($field['nama_param'])?$field['nama_param']:$this->input->post('nama_param');			
							?>
								
							<div class="control-group">
								<label class="control-label" for="textfield">Pencarian</label>
								<div class="controls">
								<input type="text" value="<?php echo $this->session->userdata('nama_param'); ?>" "<?php if ($nama_param== ['nama_param']) { echo "selected";}  ?> class="form-control" name="nama_param" placeholder="Masukan kata kunci..."  >	
							  	</div>
							</div>
					
							<table width="100%" class="table table-hover">
	    						<thead>
									<tr>
										<th>ID Parameter</th>
										<th>Nama Parameter</th>
										<th>Nilai Index</th>
										<th>Nilai Rupiah</th>
										<th>Persentase Netto Berizin</th>
										<th>Persentase Netto Tidak Berizin</th>
										<th>aksi</th>
									    <th>&nbsp;</th>
									    <th>&nbsp;</th>
	    							</tr>
								</thead>
								<tbody>
									<?php
									if (count($ListData) > 0) {
										foreach($ListData as $row)
										{
									
										?>

									<tr>
					  					<td><?php echo $row['id_param']; ?></td>
										<td><?php echo $row['nama_param']; ?></td>
										<td><?php echo $row['nilai_koefisien']; ?></td>
										<td><?php echo $row['nilai_rupiah']; ?></td>
										<td><?php echo $row['pers_netto_berizin'].'%'; ?></td>
										<td><?php echo $row['nett_tidakberizin'].'%'; ?><td>
										<td><a class="btn btn-mini btn-danger" href="<?php echo site_url('potensi/hapus/'.$row['id_param']);?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>
										</td>
									  	<td><a class="btn btn-mini btn-warning" href="<?php echo site_url('potensi/ubah/'.$row['id_param']); ?>"><i class="icon-pencil	"></i> Ubah</a>
									  	</td>
									</tr>

									<?php
									
									$paging=(!empty($pagermessage) ? $pagermessage : '');
											
										}
										echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
									} else {
										echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
									}
									?>
								</tbody>
							</table>	
						</form>	
					</div>
				</div>
			</div>
		</div> 
