			<div class="container-fluid" style="margin-top: 45px;">
			<br>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Data Master</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo site_url();?>ref_user_backend">Parameter Potensi</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>	
			</div>

			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-title">
							<h3><i class=" icon-plus-sign"></i><?php echo $judul_form." ".$sub_judul_form;?> </h3>
						</div>
                            
 						<div class="box-content">
								<!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
								<?php echo form_open('potensi/simpanTambahPotensi',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
									
								<?php 
                                    if ($this->session->flashdata('message_gagal')) {
                                    echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                    }

                                    if ($this->session->flashdata('message_sukses')) {
                                    echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                    }
                                   ?>

									<input type="hidden" name="id" id="id" class="input-xxlarge"  value="<?php echo isset($field['id'])?$field['id']:'';?>">

							  <?php 
								if (isset($field['id'])) { $dis="disabled";} else { $dis="";}							  
								$tipe_pengguna= isset($field['tipe_pengguna'])?$field['tipe_pengguna']:$this->input->post('tipe_pengguna'); 
								$status_aktif= isset($field['status_aktif'])?$field['status_aktif']:$this->input->post('status_aktif'); 
											
							  ?>
							  								  <div class="control-group">
											<label for="textfield" class="control-label">ID Parameter</label>
											<div class="controls">
												
												<input type="text" name="id_param" id="id_param" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field2['auto_id_param'])?$field2['auto_id_param']:'';?> " readonly="readonly">
											</div>
										</div>

								  <div class="control-group">
											<label for="textfield" class="control-label">Nama Parameter</label>
											<div class="controls">
												
												<input type="text" <?php echo $dis; ?>  name="nama_param" id="nama_param" class="input-xxlarge" data-rule-required="true">
											</div>
										</div>
										
								 <div class="control-group">
											<label for="textfield" class="control-label">Nilai Index</label>
											<div class="controls">
												
												<input type="text" name="nilai_koefisien" id="nilai_koefisien" class="input-xxlarge" data-rule-required="true">
												
											</div>
										</div>
                                    
									<div class="control-group">
											<label for="textfield" class="control-label">Nilai Rupiah</label>
											<div class="controls">
												
												<input type="text" name="nilai_rupiah" id="nilai_rupiah" class="input-xxlarge" data-rule-required="true" data-rule-number="true">
												
											</div>
										</div>

									<div class="control-group">
											<label for="textfield" class="control-label">Persentase Netto Berizin</label>
											<div class="controls">
												
												<input type="text" name="pers_netto_berizin" id="pers_netto_berizin" class="input-xxlarge" data-rule-required="true" data-rule-number="true">
												
											</div>
										</div>
									
									<div class="form-actions">
										<button class="btn btn-primary" type="submit">Simpan</button>
                                        <a class="btn btn-danger"  href="<?php echo site_url();?>potensi/">Kembali</a>
										
									</div>
								</form>
							</div>       
						</div>
					</div>
				</div>

				<script type="text/javascript">

				/*
				$(document).ready(function(){
				  $("#lookup_table_div").hide();
				});
				*/

				<?php 
				if ($tipe_pengguna==3 or $tipe_pengguna==4) { ?>
				$("#lookup_table_div").show();
				<?php } else { ?>
				$("#lookup_table_div").hide();
				<?php } ?>

				function doShow(tipe_isian) {

					if (tipe_isian==3 || tipe_isian==4) {
					$("#lookup_table_div").show();
					//document.getElementById("lookup_table_div").style.visibility = "visible"; 
					
					} else {
					$("#lookup_table_div").hide();
					//document.getElementById("lookup_table_div").style.visibility = "hidden"; 
					
					}
					

				}
				</script>


