		<div class="container-fluid" style="margin-top: 45px;">
			<div class="page-header">
				<div class="pull-left">
					<h1>Peta Sebaran</h1>
				</div>
				<div class="pull-right">
					<ul class="stats">
						<li class="lightred">
							<i class="icon-calendar"></i>
							<div class="details">
								<span class="big">October 20, 2016</span>
								<span>Thursday, 11:17</span>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="breadcrumbs">
				<ul>
					<li>
						<a href="#">Home</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="#">Peta</a>
					</li>
				</ul>
				<div class="close-bread">
					<a href="#"><i class="icon-remove"></i></a>
				</div>
			</div>

		   <?php echo form_open('peta/tampil_klasifikasi',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>

							  <?php 
			if (isset($field['id_kuesoner'])) { $dis="disabled";} else { $dis="";}							  
			$wilayah= isset($field['wilayah'])?$field['wilayah']:$this->input->post('wilayah');		
		 	?>
			<br>
											
			<select name="wilayah" id="wilayah" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >
             <option value="">-Pilih Klasifikasi-</option>
            
              <option value="1" <?php if ($wilayah== "1") { echo "selected";} ?>>Tampil Semua Wilayah Hasil Survey</option>
              <option value="2" <?php if ($wilayah== "2") { echo "selected";} ?>>Wilayah Berizin</option>
			  <option value="3" <?php if ($wilayah== "3") { echo "selected";} ?>>Wilayah Tidak Berizin</option>
			  <option value="4" <?php if ($wilayah== "4") { echo "selected";} ?>>Wilayah Lahan Kosong</option>	
            </select>
            
			<input class="btn btn-primary" type="submit" value="cari" name="cari" />
										
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Peta 
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content">
			
	  							<div id="map" style="width: 100%; height:550px;"></div>
	  							<link rel="stylesheet" href="<?php echo site_url();?>tema/leaflet/leaflet.css" />
	  							<script src="<?php echo site_url();?>tema/leaflet/leaflet.js"></script>
	  
	  							<script>
		
								   var map = L.map('map').setView([-6.903274, 107.5731161], 13);

									L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
										attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
									}).addTo(map);

									var LeafIcon = L.Icon.extend({
										options: {
											shadowUrl: '<?php echo site_url();?>tema/leaflet/images/marker-shadow.png'
											/*,iconSize:     [38, 95],
											shadowSize:   [50, 64],
											iconAnchor:   [22, 94],
											shadowAnchor: [4, 62],
											popupAnchor:  [-3, -76]*/
										}
									});

									var greenIcon = new LeafIcon({iconUrl: '<?php echo site_url();?>tema/leaflet/images/marker-icon.png'})
									//	,redIcon = new LeafIcon({iconUrl: '<?php echo site_url();?>tema/leaflet/images/marker-icon.png'}),
										//orangeIcon = new LeafIcon({iconUrl: '<?php echo site_url();?>tema/leaflet/images/marker-icon.png'})
										;

									<?php foreach($ListData as $row) { ?>
								
									L.marker([<?php echo $row['latitude']; ?>, <?php echo $row['longitude']; ?>], {icon: greenIcon}).bindPopup("<img src='<?php echo base_url();?>assets/uploads/<?php echo $row['directory']; ?>' ; width=200 height=100 ; ><br><br> Petugas Survey : <?php echo $row['nama_lengkap'];?> <br><br> nama pemilik : <?php echo $row['nama_pemilik'];?> <br><br> Alamat Persil : <?php echo $row['alamat_persil'];?> <br><br> Tanggal Survey : <?php echo $row['created_at'];?> <br><br> Kategori Izin : <?php echo $row['nama_kat'];?> <br><br> <a class='btn btn-primary' type='submit' href='<?php echo site_url();?>datasurvey/lihat/<?php echo $row['kode_transaksi']; ?>'>Lihat Detail</a>").addTo(map);
									
									<?php } ?>
									
									//L.marker([-7.0155274, 110.4232224], {icon: redIcon}).bindPopup("I am a red leaf.").addTo(map);
									//L.marker([-7.0024508, 110.4294451], {icon: orangeIcon}).bindPopup("I am an orange leaf.").addTo(map);

								</script>		
							</div>
						</div>
					</div>
				</div>
		</div>
				
				
				