<?php
class Peta extends CI_Controller{
    //put your code here
    public function __construct() {

        parent::__construct();
		$this->atos_tiasa_leubeut();

    }
    
    
	public function atos_tiasa_leubeut(){
		
		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}
    }
	
	public function index() {

		$data["test"]="";
		
		$sql_kec = "SELECT
				 Y.*,
				 F.*,
				 B.nama_lengkap,
				 C.nama_pemilik,
				 C.alamat_persil,
				 D.nama_kat
				 FROM tr_survey Y
				 LEFT JOIN sis_pengguna B ON Y.id_user=B.id
				 LEFT JOIN tr_detail_survey C ON Y.kode_transaksi=C.kode_transaksi
				 LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
				 LEFT JOIN tr_foto F ON C.kode_transaksi=F.kode_transaksi
				 WHERE Y.status_transaksi ='1' ";

	    $data['ListData'] = $this->db->query($sql_kec)->result_array();  

		$this->template->load('rorompok','tenjo',$data);

    }
	
	public function tampil_klasifikasi(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 11-Nov-2016
		Desc     : Menampilkan Data tertentu berdasarkan klasifikasi kategori wilayah		
		*/

		// query untuk melihat lokasi yang berizin =====

		$id_user=$this->uri->segment(3);

		$wilayah = $this->input->post('wilayah');

		if ($wilayah == "1") {

		$qry1 = "SELECT
				 Y.*,
				 F.*,
				 B.nama_lengkap,
				 C.nama_pemilik,
				 C.alamat_persil,
				 D.nama_kat
				 FROM tr_survey Y
				 LEFT JOIN sis_pengguna B ON Y.id_user=B.id
				 LEFT JOIN tr_detail_survey C ON Y.kode_transaksi=C.kode_transaksi
				 LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
				 LEFT JOIN tr_foto F ON C.kode_transaksi=F.kode_transaksi
				 WHERE Y.status_transaksi ='1' ";

		$data['ListData'] = $this->db->query($qry1)->result_array();  
		$this->template->load('rorompok','v_index_log',$data);

		}
		else if ($wilayah == "2") {

		$qry2 = "SELECT
				 Y.*,
				 F.*,
				 B.nama_lengkap,
				 C.nama_pemilik,
				 C.alamat_persil,
				 D.nama_kat
				 FROM tr_survey Y
				 LEFT JOIN sis_pengguna B ON Y.id_user=B.id
				 LEFT JOIN tr_detail_survey C ON Y.kode_transaksi=C.kode_transaksi
				 LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
				 LEFT JOIN tr_foto F ON Y.kode_transaksi=F.kode_transaksi
				 WHERE C.kat_izin=1
				 AND Y.status_transaksi ='1' ";

		$data['ListData'] = $this->db->query($qry2)->result_array();  
		$this->template->load('rorompok','v_index_log',$data);
				
		//==========================================================
		
		}
		// query untuk melihat lokasi yang tidak berizin

		else if($wilayah == "3"){

		$qry3 = "SELECT
				 Y.*,
				 F.*,
				 B.nama_lengkap,
				 C.nama_pemilik,
				 C.alamat_persil,
				 D.nama_kat
				 FROM tr_survey Y
				 LEFT JOIN sis_pengguna B ON Y.id_user=B.id
				 LEFT JOIN tr_detail_survey C ON Y.kode_transaksi=C.kode_transaksi
				 LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
				 LEFT JOIN tr_foto F ON Y.kode_transaksi=F.kode_transaksi
				 WHERE C.kat_izin=2
				 AND Y.status_transaksi ='1' ";

		$data['ListData'] = $this->db->query($qry3)->result_array();  
		$this->template->load('rorompok','v_index_log',$data);

		//==========================================================
		
		}
		// query untuk melihat lokasi yang kosong =======

		else if($wilayah == "4"){
			
		$qry4 = "SELECT
				 Y.*,
				 F.*,
				 B.nama_lengkap,
				 C.nama_pemilik,
				 C.alamat_persil,
				 D.nama_kat
				 FROM tr_survey Y
				 LEFT JOIN sis_pengguna B ON Y.id_user=B.id
				 LEFT JOIN tr_detail_survey C ON Y.kode_transaksi=C.kode_transaksi
				 LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
				 LEFT JOIN tr_foto F ON Y.kode_transaksi=F.kode_transaksi
				 WHERE C.kat_izin=3
				 AND Y.status_transaksi ='1' ";

		$data['ListData'] = $this->db->query($qry4)->result_array();  
		$this->template->load('rorompok','v_index_log',$data);
		
		//===========================================================

		} else{
			echo "do nothing";

		}

  	}

}