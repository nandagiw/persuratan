		<div class="container-fluid" style="margin-top: 45px;">
			<div class="page-header">
				<div class="pull-left">
					<h1>Data Log Pengguna</h1>
				</div>
				<div class="pull-right">
					<ul class="stats">
						<li class="lightred">
							<i class="icon-calendar"></i>
							<div class="details">
								<span class="big">October 20, 2016</span>
								<span>Thursday, 11:17</span>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="breadcrumbs">
				<ul>
					<li>
						<a href="#">Setting</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo site_url();?>log">Log</a>
						<i class="icon-angle-right"></i>
					</li>
				</ul>
				<div class="close-bread">
					<a href="#"><i class="icon-remove"></i></a>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="box">
					<div class="box-content">
						<form action="<?php echo site_url('log/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
							<div class="control-group">
								<label class="control-label" for="textfield">Pencarian</label>
								<div class="controls">
								<input type="text" value="<?php echo $this->session->userdata('s_cari_global'); ?>" class="form-control" name="cari_global" placeholder="Masukan kata kunci..."  >	
							  	</div>
							</div>			
							<table width="100%" class="table table-hover">
	    						<thead>
									<tr>
										<th>Waktu</th>
										<th>Action</th>
										<th>Modules</th>
										<th>IP</th>
										<th>Users</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if (count($ListData) > 0) {
										foreach($ListData as $row)
										{
										$enc_idx=$this->encrypt->encode($row['id']);
											$enc_idx=str_replace(array('+', '/', '='), array('-', '_', '~'), $enc_idx);
											
											?>

									<tr>
										<td><?php echo $row['tgl']; ?> <?php echo $row['time']; ?></td>
										<td><?php echo $row['action']; ?></td>
										<td><?php echo $row['modules']; ?></td>
										<td><?php echo $row['ip']; ?></td>
									    <td><?php echo $row['nama_pengguna']; ?></td>
		      						</tr>

									<?php
									
									$paging=(!empty($pagermessage) ? $pagermessage : '');
											
										}
										echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
									} else {
										echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
									}
									?>
								</tbody>
							</table>
						</form>			
					</div>
				</div>
			</div>
		</div> 
