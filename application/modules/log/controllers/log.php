<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log extends CI_Controller{

    public function __construct() {
        parent::__construct();
		$this->load->library('encrypt');
		$this->atos_tiasa_leubeut();
    }
    
    
	public function atos_tiasa_leubeut(){
	
	   /*
		Author   : Joe
		Modified :
		Date     : 24-Okt-2016
		Desc     : Pengecekan Session Login	
		
		*/
	
	
		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}
    }

   public function index( $offset = 0 ) {
			
		/*
		Author   : Joe
		Modified :
		Date     : 24-Okt-2016
		Desc     : Menampilkan List Data Log All
		*/
			
				
			if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);			
		}
			
			$per_page = 20;  
			$qry = "select b.* from log  b ";
			
			
			if ($this->session->userdata('s_cari_global')!="") {
			$qry.="  where (b.action 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			or b.modules 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			or b.nama_pengguna 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			)
			";
			} 
			elseif ($this->session->userdata('s_cari_global')=="") {
			$this->session->unset_userdata('s_cari_global');
			} 	

			$qry.= " order by b.tgl desc,b.time desc
			"; 
			//echo $qry;
		
			$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
			$config['total_rows'] = $this->db->query($qry)->num_rows();
			$config['per_page']= $per_page;
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['uri_segment'] = 3;
			$config['base_url']= base_url().'/log/index/'; 
			$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
			$this->pagination->initialize($config);
			$this->data['paginglinks'] = $this->pagination->create_links();    
			$this->data['per_page'] = $this->uri->segment(3);      
			$this->data['offset'] = $offset ;
			if($this->data['paginglinks']!= '') {
			  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
			}   
			$qry .= " limit {$per_page} offset {$offset} ";
			$this->data['ListData'] = $this->db->query($qry)->result_array();     

	$this->data['sub_judul_form']="Data Log Pengguna  ";	
	$this->template->load('rorompok','v_index_log',$this->data);	

	
   }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
