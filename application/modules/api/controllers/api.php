<?php
class Api extends CI_Controller{

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
	
	/*
		Feature  : API Android
		Author   : Joe
		Modified :
		Date     : 24-Okt-2016
		Desc     : Segala sesuatu yanng berhunungan dengan API Disimpan disini
		*/
		echo "API POTRET BPPT By Joe ".sha1(md5("A1"));
        
    }

    /*
    * Author : Mohamad Halim Bimantara
    * Date   : 27-10-2016
    * Desc   : Post data survey
    * Params {
				tr_survey : id_kategori,lat,long
    		 }
    */

    public function PostDataSurvey(){
    		$post_lat  	   = $this->input->post("p_latitude");
    		$post_long 	   = $this->input->post("p_longitude");
    		$id_user   	   = $this->input->post("id");
    		$id_kategori   = $this->input->post("id_kategori");//kategori perizinan
    		$token         = $this->input->post("token");
    		$created_at    = date("Y-m-d H:i:s");

    }

     /*
    * Author :Mohamad Halim Bimantara
    * Date   : 27-10-2016
    * Desc   : Post data Gambar
    * Params {
				tr_photo  :	id_survey,lat_pot,long_pot,dir
    		 }
    */
    public function PostdataImage(){
    	$this->load->library('upload');
    	$response=array();
    	    $g_lat  	   = $this->input->post("p_latitude");
    		$g_long 	   = $this->input->post("p_longitude");
    		$kode_transaksi     = $this->input->post("kode_transaksi");
    		$namaFolder    = $this->input->post("fldr");
    	    $path_='./assets/uploads/';

    	//get id_survey
		$sql_idsurvey="SELECT id_survey FROM tr_survey WHERE kode_transaksi='$kode_transaksi'"; 
		$residsurvey=$this->db->query($sql_idsurvey);
		$idsurveyresult=$residsurvey->row()->id_survey;    

    	$new_path=$path_.$namaFolder;

    	if(!is_dir($new_path)){
    		mkdir($new_path,0755,TRUE);
    	}
    	$nmfile="name".time();
    	$config['upload_path']   ='./assets/uploads/'.$namaFolder.'/';
    	$config['allowed_types'] ='jpg|png|jpeg|JPEG';
    	$config['max_size']      ='1024';//maximum 1mb 
    	$config['max_width']     ='1288';
    	$config['max_height']    ='768';

    	$this->upload->initialize($config);
    	if($_FILES['filefoto']['name'])
    	{
    		if ($this->upload->do_upload('filefoto')) 
    		{
	    		$gbr  = $this->upload->data();
	    		$data = array(
	    					  'id_survey' =>$idsurveyresult,
	    					  'latitude'  =>$g_lat,
	    					  'longitude' =>$g_long,
	    					  'is_primary'=>1,
	    			  	      'directory' =>$namaFolder."/".$gbr['file_name'],
	    			  	      'kode_transaksi'=>$kode_transaksi
	    			  	     );
	    		//query insert
				$this->db->insert('tr_foto', $data);

				$datasurvey =array( 'latitude'  =>$g_lat,
	    					       'longitude'  =>$g_long);

	    		$this->db->where('kode_transaksi', $kode_transaksi);			       	
				$this->db->update('tr_survey',$datasurvey);

	    		$response['status']  = true;
	    		$response['message'] ='Berhasil upload';
    	     }else{
    	     	$error = $this->upload->display_errors();
    	     	$response['status']  = false;
	    		$response['message'] ='Gagal...'.$error;
    	     }
     	 }else{
     	 	$response['status']  = false;
	    	$response['message'] ='Ada kesalahan upload File...';
     	 }
     	 echo json_encode($response);
    }
	
	public function GetLoginSurveyor() {
		/*
		Author   : Joe
		Modified :
		Date     : 24-Okt-2016
		Desc     : Pengecekan Login Untuk Request Android
		
		*/
			$nama_pengguna = $_POST['nama_pengguna'];
			$kunci_masuk   = $_POST['kunci_masuk'];
			$ip            = $_POST['ip'];

$sqlcek   ="SELECT * FROM sis_pengguna WHERE status_aktif='Y' and nama_pengguna=? 
			AND kunci_masuk=?";
$querycek = $this->db->query($sqlcek, array($nama_pengguna, sha1(md5($kunci_masuk))));
						$ceking=$querycek->num_rows();
						$response=array();
						if ($ceking == 0) {
								/*
								  Message Login Gagal
								*/
							    $emparray['status']  = false;
								$emparray['message'] = "Login Gagal";
								echo json_encode($emparray);	
							
							} else {
								$row = $querycek->row();
								$sesi_id_pengguna=$row->id;
								$sesi_nama_pengguna=$row->nama_pengguna;
								$sesi_nama_lengkap=$row->nama_lengkap;
								$sesi_token=$row->token;
								
								
								$data_log = array(
								'id_pengguna'   =>$sesi_id_pengguna,
								'nama_pengguna' =>$sesi_nama_pengguna,
								'modules' => "Login",
								'action'  => "LOGIN via ANDROID",
								'tgl'     =>date("Y-m-d"),
								'time'    =>date("H:i:s"),
								'ip'      =>$ip

								);
					//insert Log
					$this->db->insert('log', $data_log);
					
					$response["status"]   = true;
                    $response["message"]  = "Login Berhasil";
                    $response['id']       = $sesi_id_pengguna;
                    $response['nama']     = $sesi_nama_pengguna;
                    $response['bagian']   = $sesi_nama_lengkap;
                    $response['token']    =	$sesi_token;
                    echo json_encode($response);
							
		}				
	
        
    }
	
	
	public function GetLogoutSurveyor() {
	
	/*
		Feature  : Keluar
		Author   : Joe
		Modified : 
		Date     : 24-Okt-2016
		Desc     : Memutuskan session login dan mencatat log.	
		*/
		
		$id_pengguna = $_POST["id_pengguna"];
		
		$data_log = array(
		'id_pengguna' =>$id_pengguna,
		'modules' => "Login",
		'action' => "LOGOUT via ANDROID",
		'tgl' =>date("Y-m-d"),
		'time' =>date("H:i:s"),
		'ip'=>$this->input->ip_address()
		);
		
		$this->db->insert('log', $data_log);
		
		$token1 = openssl_random_pseudo_bytes(16);
		$token  = bin2hex($token1);
				
		$data1    = array('token' => $token);
		// $this->db->where('id',$id_pengguna);
		$response = $this->db->where('id',$id_pengguna)->update('sis_pengguna',$data1);
		if ($response == false) 
		{
		  $emparray['status']  = false;
		  $emparray['message'] = "Login Gagal";
		  echo json_encode($emparray);	
	    }else{
	      $emparray['status']  = true;
		  $emparray['message'] = "Logout Berhasil";
		  echo json_encode($emparray);	
	    }
	}

	public function GetCekToken() {
	
	/*
		Feature  : Cek Token
		Author   : Joe
		Modified :
		Date     : 24-Okt-2016
		Desc     : Pengecekan Token Session Login By Id
		*/
		
		$id_pengguna = $_POST["id_pengguna"];
		$token       = $_POST["token"];

		$sqlcek   = "SELECT id,token FROM sis_pengguna WHERE id=? AND token=?";
		$querycek = $this->db->query($sqlcek, array($id_pengguna, $token));
		$ceking   = $querycek->num_rows();
		
		
		if ($ceking==0) {
						
				$cek=false;
				$emparray['status_token'] = $cek;
				echo json_encode($emparray);
				
				} else {
				
				$cek=true;
				$emparray['status_token'] = $cek;
				echo json_encode($emparray);
		}
		
	
	}
	
	public function CekToken($id,$token) {
	/*
		Feature  : Cek Token
		Author   : Joe Modified By Halim
		Modified :
		Date     : 26-Okt-2016
		Desc     : Pengecekan Token Session Login By Id
		*/

		$sqlcek   = "SELECT id,token FROM sis_pengguna WHERE id=? AND token=?";
		$querycek = $this->db->query($sqlcek, array($id, $token));
		$ceking   = $querycek->num_rows();
		
		$result=0;
		if ($ceking == 0) {
			//jika tidak cocok
			return $result;	
		} else {
			//jika cocok
			return $result=1;
		}
	}


	/*
	* Author : Mohamad Halim Bimantara
	* Date   : 27-10-2016
	* Desc   : menampilkan semua data survey 
	*/

	public function getAllMapsPotret(){
		$id_ 		 = $_POST["id"];
		$token       = $_POST["token"];
		//table tr suervey
		$sql="SELECT id_survey,latitude,longitude FROM tr_survey WHERE status_transaksi='1'";
		$querycek = $this->db->query($sql);
		$ceking   = $querycek->num_rows();

		$emparray=array();
		$cek=true;
		$cekToken=$this->CekToken($id_,$token);

		
		 if ($ceking > 0) {
					$emparray['status'] = true;
					$emparray['message'] = "Berhasil Load";
			if( $cekToken != 0){
				foreach ($querycek->result_array() as $key) {
				 	$response['id']   = $key['id_survey'];
				 	$response['lat']  = $key['latitude'];
				 	$response['long'] = $key['longitude'];
				 	$results[]=$response;
				 }
				 $emparray['data']=$results;
		}else{
				$emparray['status'] = false;
				$emparray['message'] = "Gagal Load Data";	
		}
	}else{
			$emparray['status'] = false;
			$emparray['message'] = "Data Anda Tidak Valid ....";	
	}
	echo json_encode($emparray);
			
}
	/*
	*/
 public function getDetailMaps()
  {
  		$id_survey   = $_POST["id_survey"];
		//table tr suervey
		$sql="SELECT id_survey,latitude,longitude FROM tr_survey WHERE status_transaksi='1'";
		$querycek = $this->db->query($sql);
		$ceking   = $querycek->num_rows();

		$emparray=array();
		$cek=true;
  

		 if ($ceking > 0) {
					$emparray['status'] = true;
					$emparray['message'] = "Berhasil Load";

				foreach ($querycek->result_array() as $key) {
				 	$response['id']   = $key['id_survey'];
				 	$response['lat']  = $key['latitude'];
				 	$response['long'] = $key['longitude'];
				 	
				 	$results[]=$response;
				 }

				 $emparray['data']=$results;
	}else{
			$emparray['status'] = false;
			$emparray['message'] = "Data Anda Tidak Valid ....";	
	}
	echo json_encode($emparray);
  }

/*
*
*/

public function getMyHistory()
{
	$id_user   = $_POST["id_pengguna"];
	$sql="SELECT A.*,B.nama_lengkap,C.nama_pemilik,C.alamat_persil
					FROM tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					WHERE A.status_transaksi='1' AND A.id_user='".$id_user."' ";
	$querycek = $this->db->query($sql);
	$ceking   = $querycek->num_rows();
	$emparray=array();
	$cek=true;
		
	if ($ceking > 0) {
					$emparray['status'] = true;
					$emparray['message'] = "Berhasil Load";	
				foreach ($querycek->result_array() as $key) {
				 	$response['id']              = $key['id_survey'];
				 	$response['kode_transaksi']  = $key['kode_transaksi'];
				 	$response['nama_lengkap']    = $key['id_survey'];
				 	$response['nama_pemilik']    = $key['kode_transaksi'];
				 	$response['alamat_persil']   = $key['alamat_persil'];
				 	$results[] = $response;
				 }
				    $emparray['data']=$results;
	}else{
			$emparray['status'] = false;
			$emparray['message'] = "Data Anda Tidak Valid ....";	
	}
	echo json_encode($emparray);	
}
	
	/*
	*
	*/
 public function getDetailMyHistory(){
 	$kode_transaksi=$_POST['kode_transaksi'];
 	$sql="SELECT A.*,
				 B.nama_kec ,
				 C.nama_kel,
				 D.nama_kat,
				 E.nama_peruntukkan,
				 PIU.*,
				 refp.nama_status_piutang
				FROM tr_detail_survey A
					LEFT JOIN ref_kategori D ON A.kat_izin=D.id_kategori
					LEFT JOIN ref_peruntukkan E ON A.id_peruntukkan=E.id_peruntukkan
					LEFT JOIN ref_kecamatan B ON A.kode_kecamatan=B.kode_kecamatan
					LEFT JOIN ref_kelurahan C ON A.kode_kelurahan=C.kode_kelurahan
					LEFT JOIN tr_piutang_retribusi PIU  ON A.kode_transaksi=PIU.kode_transaksi
					LEFT JOIN ref_status_piutang   refp ON PIU.id_status_piutang=refp.id_status_piutang

				AND A.kode_kecamatan=C.kode_kecamatan 
				WHERE A.kode_transaksi='".$kode_transaksi."'";

	$querycek = $this->db->query($sql);
	$ceking   = $querycek->num_rows();
	$emparray=array();
	$cek=true;

   $sql_foto = "SELECT directory FROM tr_foto WHERE kode_transaksi='".$kode_transaksi."' ";
   $datafoto = $this->db->query($sql_foto);  
   	
	if ($ceking > 0) {
					$emparray['status'] = true;
					$emparray['message'] = "Berhasil Load";	
				foreach ($querycek->result_array() as $key) {
				 	$response['nama_pemilik']     = $key['nama_pemilik'];
				 	$response['nama_perusahaan']  = $key['nama_perusahaan'];
				 	$response['alamat_persil']    = $key['alamat_persil'];

				 	$response['tanggal_izin']     = $key['tanggal_izin'];
				 	$response['luas_persil']      = $key['luas_persil'];
				 	$response['alamat_persil']    = $key['alamat_persil'];

				 	$response['kode_transaksi']   = $key['kode_transaksi'];
				 	$response['nama_kec']         = $key['nama_kec'];
				 	$response['nama_kel']         = $key['nama_kel'];
				 	$response['nama_peruntukkan']    = $key['nama_peruntukkan'];
					$response['nama_status_piutang'] = $key['nama_status_piutang'];
				 	$results[]=$response;
				 }
				   $emparray['data']=$results;

				 foreach ($datafoto->result_array() as $key) {
				 	$responsefoto['nama_foto']=$key['directory'];
				 	$resfoto=[$responsefoto];
				 }
				    $emparray['foto']=$resfoto;
	}else{
			$emparray['status'] = false;
			$emparray['message'] = "Data Anda Tidak Valid ....";	
	}
	echo json_encode($emparray);	
 }

 public function getCategory(){

 }

 public function getPeruntukan(){
 	$id_ 		 = $_POST["id"];
	$token       = $_POST["token"];

 	$sql="SELECT id_peruntukkan,nama_peruntukkan FROM ref_peruntukkan";
 	$querycek = $this->db->query($sql);
	$ceking   = $querycek->num_rows();

		$emparray=array();
		$cek=true;
		$cekToken=$this->CekToken($id_,$token);

		
		 if ($ceking > 0) {
					$emparray['status'] = true;
					$emparray['message'] = "Berhasil Load";
			if( $cekToken != 0){
				foreach ($querycek->result_array() as $key) {
				 	$response['id']                = $key['id_peruntukkan'];
				 	$response['nama_peruntukkan']  = $key['nama_peruntukkan'];
				 	$results[]=$response;
				 }
				 $emparray['data']=$results;
		}else{
				$emparray['status'] = false;
				$emparray['message'] = "Gagal Load Data";	
		}
	}else{
			$emparray['status'] = false;
			$emparray['message'] = "Data Anda Tidak Valid ....";	
	}
	echo json_encode($emparray);
 }

 public function getSearchJalan(){
 	    $id_ 		 = $_POST["id"];
		$token       = $_POST["token"];
		//table tr suervey
		$sql="SELECT * FROM ";
		$querycek = $this->db->query($sql);
		$ceking   = $querycek->num_rows();

		$emparray=array();
		$cek=true;
		$cekToken=$this->CekToken($id_,$token);

		
		 if ($ceking > 0) {
					$emparray['status'] = true;
					$emparray['message'] = "Berhasil Load";
			if( $cekToken != 0){
				foreach ($querycek->result_array() as $key) {
				 	$response['id']   = $key['id_survey'];
				 	$response['lat']  = $key['latitude'];
				 	$response['long'] = $key['longitude'];
				 	$results[]=$response;
				 }
				 $emparray['data']=$results;
		}else{
				$emparray['status'] = false;
				$emparray['message'] = "Gagal Load Data";	
		}
	}else{
			$emparray['status'] = false;
			$emparray['message'] = "Data Anda Tidak Valid ....";	
	}
	echo json_encode($emparray);
 }

 /*
	* Author : Joe
	* Date   : 01-nov-2016
	* Desc   : Inisiasi Proses Pendataan
	*/

	public function CreateProsesSurvey(){
		// $param_pilihan = $_POST["pilihan"];
		// 1. Isi data dengan pemilik
		// 2. Isi data dengan tidak ada pemiliknya
	   $id_pengguna = $_POST["id_pengguna"];
		$kode_transaksi_rnd = strtoupper(substr(md5(uniqid(rand(), true)),0,6));
		$sqlresi = "select count(kode_transaksi) as kode_transaksi_rnd from tr_survey where kode_transaksi = '".$kode_transaksi_rnd."'";
		$rowresi = $this->db->query($sqlresi)->row_array();
		
		if ($rowresi['kode_transaksi_rnd']!=0) {
			$kode_transaksi_rnd1 = substr(md5(uniqid(rand(), true)),0,6);
			$kode_transaksi      = $kode_transaksi_rnd1;
		}else{
			$kode_transaksi  = $kode_transaksi_rnd;
		}
		
		$data_trans = array(
			'id_user'    =>$id_pengguna,
			'tgl_survey' =>date("Y-m-d"),
			'kode_transaksi'   =>$kode_transaksi,
			'status_transaksi' =>0,
			'created_by' =>$id_pengguna,
			'created_at' =>date("Y-m-d")
		);
		
		$result=$this->db->insert('tr_survey', $data_trans);
		
		$data_log = array(
			'id_pengguna' =>$id_pengguna,
			'modules' => "ISI SURVEY",
			'action' => "STEP-2 PENGISIAN SURVEY (TIDAK DENGAN PEMILIK)",
			'tgl' =>date("Y-m-d"),
			'time' =>date("H:i:s"),
			'ip'=>$this->input->ip_address()
		);
		
		$this->db->insert('log', $data_log);


		if($result){
			$emparray['status'] = true;
			$emparray['message'] = "Berhasil";
			$emparray['kode_transaksi'] = $kode_transaksi;
		
		}else{
			$emparray['status'] = false;
			$emparray['message'] = "Berhasil";
		}
		echo json_encode($emparray);
	}

		
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	
	public function SelectKategori() {
	
	$sql="SELECT id_kategori,nama_kat FROM ref_kategori";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
			$emparray['message'] = "Berhasil";
	foreach ($querycek->result_array() as $key) {
				 	$response['id_kategori']   = $key['id_kategori'];
				 	$response['nama_kat']  = $key['nama_kat'];
				 	
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	public function SelectKecamatan() {
	
	$sql="SELECT kode_kecamatan,nama_kec FROM ref_kecamatan";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
	$emparray['message'] = "Berhasil Load";
	foreach ($querycek->result_array() as $key) {
				 	$response['kode_kecamatan']   = $key['kode_kecamatan'];
				 	$response['nama_kec']  = $key['nama_kec'];
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	public function SelectKelurahan() {
	$kode_kecamatan = $_POST["kode_kecamatan"];
	
	$sql="SELECT kode_kelurahan,nama_kel FROM ref_kelurahan WHERE kode_kecamatan='".$kode_kecamatan."'";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
	$emparray['message'] = "Berhasil Load";
	foreach ($querycek->result_array() as $key) {
				 	$response['kode_kelurahan']   = $key['kode_kelurahan'];
				 	$response['nama_kel']  = $key['nama_kel'];
				 	
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	public function SelectPeruntukan() {
	$sql="SELECT id_peruntukkan,nama_peruntukkan FROM ref_peruntukkan ";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
	$emparray['message'] = "Berhasil Load";
	foreach ($querycek->result_array() as $key) {
				 	$response['id_peruntukkan']    = $key['id_peruntukkan'];
				 	$response['nama_peruntukkan']  = $key['nama_peruntukkan'];
				 	
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	public function SelectStatusPiutang() {

	$sql="SELECT id_status_piutang,nama_status_piutang FROM ref_status_piutang ";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
	$emparray['message'] = "Berhasil Load";
	foreach ($querycek->result_array() as $key) {
				 	$response['id_status_piutang']   = $key['id_status_piutang'];
				 	$response['nama_status_piutang']  = $key['nama_status_piutang'];
				 	
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	public function SelectJenisIzin() {
	$sql="SELECT id_jenis_izin,kode_jenis_izin,nama_jenis FROM ref_jenis_izin ";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
	$emparray['message'] = "Berhasil Load";
	foreach ($querycek->result_array() as $key) {
				 	$response['kode_jenis_izin']   = $key['kode_jenis_izin'];
				 	$response['nama_jenis']  = $key['nama_jenis'];
				 	$response['id_jenis_izin']  = $key['id_jenis_izin'];
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Post Data  Survey
	*/
	public function PostProsesSurvey(){

		$kode_transaksi = $_POST["kode_transaksi"]; // yang dibawa terus
	    // variabel utk update ke master tr_survey
	    $id_pengguna = $_POST["id_pengguna"];
	    $no_surat_perintah = $_POST["no_surat_perintah"];
	    $latitude = $_POST["latitude"];
	    $longitude = $_POST["longitude"];
		
		
		// variabel utk insert ke detail tr_survey
		$nama_pemilik=$_POST["nama_pemilik"];
		$nama_perusahaan=$_POST["nama_perusahaan"];
		$alamat_persil=$_POST["alamat_persil"];
		$luas_persil=$_POST["luas_persil"];
		$nomor_izin=$_POST["nomor_izin"];
		$tanggal_izin=$_POST["tanggal_izin"];
		$id_peruntukkan=$_POST["id_peruntukkan"];
		$kode_kecamatan=$_POST["kode_kecamatan"];
		$kode_kelurahan=$_POST["kode_kelurahan"];
		$id_kategori=$_POST["id_kategori"];
		$id_jenisperizinan = $_POST["jenis_izin"];
	    
		// variabel utk insert ke piutang
		$id_status_piutang=$_POST["id_status_piutang"];
		$nilai_piutang=$_POST["nilai_piutang"];
		$tgl_skrd=$_POST["tgl_skrd"];
		$nilai_piutang_denda=$_POST["nilai_piutang_denda"];
		$alasan_blm_bayar=$_POST["alasan_blm_bayar"];
		
		//get id_survey
		$sql_idsurvey="SELECT id_survey FROM tr_survey WHERE kode_transaksi='$kode_transaksi'"; 
		$residsurvey=$this->db->query($sql_idsurvey);
		$idsurveyresult=$residsurvey->row()->id_survey;

	    $this->db->trans_begin();
	
		// update tr_survey karena sebelumnya sudah di buat transaksinya
		//'id_user'    =>$id_pengguna,
		$data_tr_survey = array(
			'id_kategori'=>$id_kategori,
			'tgl_survey' =>date("Y-m-d"),
			'kode_transaksi' =>$kode_transaksi,
			'created_by' =>$id_pengguna,
			'created_at' =>date("Y-m-d"),
			'created_by' =>$id_pengguna,
			'no_surat_perintah' =>$no_surat_perintah,
			'latitude' =>$latitude,
			'longitude' =>$longitude
		);
		
		 $this->db->where('kode_transaksi', $kode_transaksi);
         $this->db->update('tr_survey', $data_tr_survey);

         //
		 
		 
		 // insert detail survey		 
		 $data_tr_detail_survey = array(
		 	'id_jenis_izin'=>$id_jenisperizinan,
			'nama_pemilik' =>$nama_pemilik,
			'nama_perusahaan' =>$nama_perusahaan,
			'alamat_persil' =>$alamat_persil,
			'luas_persil' =>$luas_persil,
			'nomor_izin' =>$nomor_izin,
			'tanggal_izin' =>$tanggal_izin,
			'id_peruntukkan' =>$id_peruntukkan,
			'kode_kecamatan' =>$kode_kecamatan,
			'kode_kelurahan' =>$kode_kelurahan,
			'kat_izin' =>$id_kategori,
			'kode_transaksi' =>$kode_transaksi,
			'id_survey' => $idsurveyresult
		);
		
		$this->db->insert('tr_detail_survey', $data_tr_detail_survey);
		 
		
		
		 // insert detail tr_piutang_retribusi		 
		 $data_tr_piutang_retribusi = array(
			'id_status_piutang' =>$id_status_piutang,
			'nilai_piutang' =>$nilai_piutang,
			'tgl_skrd' =>$tgl_skrd,
			'nilai_piutang_denda' =>$nilai_piutang_denda,
			'alasan_blm_bayar' =>$alasan_blm_bayar,
			'kode_transaksi' =>$kode_transaksi,
			'id_survey' => $idsurveyresult
		);
		
		$this->db->insert('tr_piutang_retribusi', $data_tr_piutang_retribusi);
		
		
		$data_log = array(
			'id_pengguna' =>$id_pengguna,
			'modules' => "INSERT TRANSAKSI SURVEY",
			'action' => "STEP-3 PENGISIAN SURVEY",
			'tgl' =>date("Y-m-d"),
			'time' =>date("H:i:s"),
			'ip'=>$this->input->ip_address()
		);
		
		$this->db->insert('log', $data_log);
		
		
	if ($this->db->trans_status() == FALSE)
			{
					$this->db->trans_rollback();
					$emparray['status_proses'] = false;
			}
			else
			{
					$this->db->trans_commit();
					$emparray['status_proses'] = true;
	  }
	  echo json_encode($emparray);
						
	}
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	public function SelectPertanyaan() {
	$sql="SELECT id_kuesioner,pertanyaan FROM m_kuesioner ";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
	$emparray['message'] = "Berhasil Load";
	foreach ($querycek->result_array() as $key) {
				 	$response['id_kuesioner']   = $key['id_kuesioner'];
				 	$response['pertanyaan']  = $key['pertanyaan'];
				 	
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Get Data 
	*/
	public function SelectPilihanJawaban() {
	
	$id_kuesioner=$_POST["id_kuesioner"];
	//$id_kuesioner=1;
	$sql="SELECT id_pil_jawab,id_kuesioner,nama_pil_jawab FROM m_pilihan_jawaban WHERE id_kuesioner='".$id_kuesioner."' ";
	$querycek = $this->db->query($sql);
	$emparray['status'] = true;
	$emparray['message'] = "Berhasil Load";
	foreach ($querycek->result_array() as $key) {
				 	$response['id_pil_jawab']   = $key['id_pil_jawab'];
				 	$response['id_kuesioner']  = $key['id_kuesioner'];
					$response['nama_pil_jawab']  = $key['nama_pil_jawab'];
				 	
				 	$results[]=$response;
				 }
	$emparray['data']=$results;
	echo json_encode($emparray);
	
	}
	
	
	
	
	public function PostJawaban(){

		$kode_transaksi = $_POST["kode_transaksi"]; // yang dibawa terus
	   
	   
	    $id_pil_jawab = $_POST["id_pil_jawab"];
	    $id_kuesioner = $_POST["id_kuesioner"];
	    $jawaban = $_POST["jawaban"];
	
	    $this->db->trans_begin();
	
		
		$sqlcek   ="SELECT * FROM tr_jawab_kuesioner WHERE id_pil_jawab='$id_pil_jawab' and id_kuesioner='$id_kuesioner' and kode_transaksi='$kode_transaksi'";
		$querycek = $this->db->query($sqlcek);
						$ceking=$querycek->num_rows();
						$response=array();
						
						if ($ceking == 0) {
								/*
								  INSERT
								*/
							    
								 $data_tr_jawab_kuesioner = array(
								'kode_transaksi' =>$kode_transaksi,
								'id_pil_jawab' =>$id_pil_jawab,
								'id_kuesioner' =>$id_kuesioner,
								'jawaban' =>$jawaban								
								);
								
								$this->db->insert('tr_jawab_kuesioner', $data_tr_jawab_kuesioner);
								
							
							} else {
							
								/*
								DELETE INSERT
								*/
								
								$this->db->query("delete from tr_jawab_kuesioner where id_pil_jawab='$id_pil_jawab' and id_kuesioner='$id_kuesioner' and kode_transaksi='$kode_transaksi' ");
								
								
								$data_tr_jawab_kuesioner = array(
								'kode_transaksi' =>$kode_transaksi,
								'id_pil_jawab' =>$id_pil_jawab,
								'id_kuesioner' =>$id_kuesioner,
								'jawaban' =>$jawaban								
								);
								
								$this->db->insert('tr_jawab_kuesioner', $data_tr_jawab_kuesioner);
								
								
					   
						    }
							
							
							$data_log = array(
					'id_pengguna' =>$id_pengguna,
					'modules' => "INSERT TRANSAKSI ISI KUISIONER",
					'action' => "STEP-4 PENGISIAN KUISIONER",
					'tgl' =>date("Y-m-d"),
					'time' =>date("H:i:s"),
					'ip'=>$this->input->ip_address()
					);
					
					$this->db->insert('log', $data_log);
		
	if ($this->db->trans_status() === FALSE)
			{
					$this->db->trans_rollback();
					$emparray['status_proses'] = false;
			}
			else
			{
					$this->db->trans_commit();
					$emparray['status_proses'] = true;
	  }
						
	}	
	
	
	/*
	* Author : Joe
	* Date   : 02-nov-2016
	* Desc   : Post Data  Survey
	*/
	public function Penyelesaian(){

		$kode_transaksi = $_POST["kode_transaksi"]; // yang dibawa terus
	   
	
	    $this->db->trans_begin();
	
		// update tr_survey karena sudah selesai
		$data_tr_survey = array(
		'status_transaksi' =>1
		);
		
		 $this->db->where('kode_transaksi', $kode_transaksi);
         $this->db->update('tr_survey', $data_tr_survey);
		 
		
		
		$data_log = array(
			'id_pengguna' =>$id_pengguna,
			'modules' => "PENYELESAIAN TRANSAKSI SURVEY",
			'action' => "STEP-5 PENGISIAN SURVEY",
			'tgl' =>date("Y-m-d"),
			'time' =>date("H:i:s"),
			'ip'=>$this->input->ip_address()
		);
		
		$this->db->insert('log', $data_log);
		
	if ($this->db->trans_status() === FALSE)
			{
					$this->db->trans_rollback();
					$emparray['status_proses'] = false;
			}
			else
			{
					$this->db->trans_commit();
					$emparray['status_proses'] = true;
	        }
			echo json_encode($emparray);
	}
	

}