<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>POTRET - BPPT KOTA BANDUNG</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/font-awesome.min.css.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/design.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/themes.css">


	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>tema/js/jquery.min.js"></script>
	
	
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	
	<script src="<?php echo base_url(); ?>tema/js/bootstrap.min.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/form/jquery.form.min.js"></script>
	

	<!-- Theme framework -->
	<script src="<?php echo base_url(); ?>tema/js/eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo base_url(); ?>tema/js/application.min.js"></script>
	
	
	<!-- Validation -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/validation/jquery.validate.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/validation/additional-methods.min.js"></script>
	

</head>

<body>
	
	
	<div class="row-fluid">
					<div class="span12" align="center">
					    <br>
						<img src="<?php echo base_url(); ?>tema/img/header_frontend.png">
						<hr>
					
					</div>
	</div>
	
	
	<div class="row-fluid">
					<div class="span12">
						<div class="box">
							
							<div class="box-content">
								
								
								
    
	
	<div class="row-fluid">
<div class="span3"></div>
					<div class="span6">
						<div class="box">
							<div class="box-content">
								
								
								
								<div class="box box-color box-bordered">
<div class="box-title">
								<h3><i class="icon-user"></i>Login/Masuk</h3>
								
							</div>
                            
 <div class="box-content">
								<form class="form-horizontal form-validate" id="bb" name="bb" accept-charset="utf-8" method="post" action="<?php echo site_url();?>ijinmasuk/leubeut" novalidate="novalidate">								
								
								<?php if (isset($pesan)) { ?><hr><div class="alert alert-danger alert-dismissable"><?php echo $pesan; ?></div><?php } ?>
				<?php echo validation_errors(); ?>
																
									<div class="control-group">
										<label for="textfield" class="control-label">Username</label>
										<div class="controls">
											<input type="text" data-rule-number="false" data-rule-required="true" placeholder="" id="nama_pengguna" class="input-xlarge" name="nama_pengguna">
										</div>
									</div>
									<div class="control-group">
										<label for="password" class="control-label">Password</label>
										<div class="controls">
											<input type="password" data-rule-required="true" name="kunci_masuk" id="kunci_masuk" placeholder="" class="input-xlarge">
										</div>
									</div>
									
										
									
										<div class="control-group">
										<label for="textfield" class="control-label">Kode Keamanan</label>
										
										<div class="controls">
																						<?php echo $image; ?>
											<input type="text" data-rule-required="true" maxlength="4" class="input-small" name="secutity_code">
									  </div>
									
									<div class="control-group">
									<div class="controls">
										<!--<label class="control-label"><small><a href="#">Lupa Password?</a></small></label>-->
										
									</div>	
									</div>
									
								
									
									
										
                                        <button type="submit" class="btn btn-primary"><i class="icon-check"></i>&nbsp;LOGIN</button>
										
                                    
								
							</div></form>       
 
 
</div>
								
								
								
								
							</div>
						</div>
					</div>
				</div>				
				
				
			</div>

	
	
	
	
     
    
	
	
	
								
								
							</div>
						</div>
					</div>
				</div>
	
	
		
	</body>

	</html>




