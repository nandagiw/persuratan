		<link rel="stylesheet" href="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
	   <script src="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
	   <script src="<?php echo base_url(); ?>https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

		<div class="container-fluid" style="margin-top: 45px;"> 
			<br>
			<div class="breadcrumbs">
				<ul>
					<li>
						<a href="<?php echo site_url();?>peruntukkan">Data Peruntukkan</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="#">Ubah Data Peruntukkan</a>
						<i class="icon-angle-right"></i>
					</li>
				</ul>
				<div class="close-bread">
					<a href="#"><i class="icon-remove"></i></a>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<div class="box">
					<div class="box-title">
						<h3><i class=" icon-plus-sign"></i><?php echo $judul_form." ".$sub_judul_form;?> </h3>
					</div>
	                    
					<div class="box-content">

							<?php echo form_open('peruntukkan/simpanUbahPeruntukkan',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>

							<?php 
                                if ($this->session->flashdata('message_gagal')) {
                                echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                }

                                if ($this->session->flashdata('message_sukses')) {
                                echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                }
	                        ?>
									
								<!--<input type="hidden" name="id_kuesioner" id="id_kuesioner" class="input-xxlarge"  value="<?php echo isset($field1['id_kuesioner'])?$field1['id_kuesioner']:'';?>">

								<input type="hidden" name="id_pil_jawab" id="id_pil_jawab" class="input-xxlarge"  value="<?php echo isset($field1['id_pil_jawab'])?$field1['id_pil_jawab']:'';?>">

								<?php 
								if (isset($field3['id_param'])) { $dis="disabled";} else { $dis="";}							  
								$xxx= isset($field3['nama_param'])?$field3['nama_param']:$this->input->post('nama_param'); 
								$status_aktif= isset($field1['status_aktif'])?$field1['status_aktif']:$this->input->post('status_aktif'); 
								$id_pil_jawab= isset($field1['id_pil_jawab'])?$field1['id_pil_jawab']:$this->input->post('id_pil_jawab'); 
		
						  		?> -->

	                            <div class="control-group">
									<label for="textfield" class="control-label">ID Peruntukkan</label>
									<div class="controls">
										<!-- <?php echo "tes : ".$id_pil_jawab; ?> -->
										<input type="text" name="id_peruntukkan" id="id_peruntukkan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['id_peruntukkan'])?$field1['id_peruntukkan']:'';?> " readonly="readonly">
									</div>
								</div>
									
							 <div class="control-group">
										<label for="textfield" class="control-label">Nama Peruntukkan</label>
										<div class="controls">
											
											<input type="text" name="nama_peruntukkan" id="nama_peruntukkan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['nama_peruntukkan'])?$field1['nama_peruntukkan']:'';?>">
										</div>
									</div>

							 <div class="control-group">
									<label for="textfield" class="control-label">Kategori Parameter
									</label>
									<div class="controls">
										<select name="id_param" id="id_param" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >
											<option value="<?php echo isset($field1['id_param'])?$field1['nama_param']:'';?>"><?php echo $field1['nama_param']; ?></option>
											<?php foreach($field3 as $row2) { ?>
		                             		<option value="<?php echo isset($row2['id_param'])?$row2['id_param']:'';?>"><?php echo $row2['nama_param']; ?></option>
		                            		<?php } ?>
		                            	</select>
		                            </div>
								</div>

								<div class="form-actions">
									<button class="btn btn-primary" type="submit">Simpan</button>
	                                <a class="btn btn-danger" href="<?php echo site_url();?>peruntukkan/">Kembali</a>
									
								</div>
							</form>
						</div>       

						</div>

						</div>
					
						</div>

		<script type="text/javascript">

						/*
						$(document).ready(function(){
						  $("#lookup_table_div").hide();
						});
						*/



						<?php 
						if ($jawaban==3 or $jawaban==4) { ?>
						$("#lookup_table_div").show();
						<?php } else { ?>
						$("#lookup_table_div").hide();
						<?php } ?>

						function doShow(tipe_isian) {

							if (tipe_isian==3 || tipe_isian==4) {
							$("#lookup_table_div").show();
							//document.getElementById("lookup_table_div").style.visibility = "visible"; 
							
							} else {
							$("#lookup_table_div").hide();
							//document.getElementById("lookup_table_div").style.visibility = "hidden"; 
							
							}
							

						}

						</script>


