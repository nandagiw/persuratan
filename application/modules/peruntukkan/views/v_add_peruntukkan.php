			<div class="container-fluid" style="margin-top: 45px;">
				<br>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Data Master</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo site_url();?>peruntukkan">Data Peruntukkan</a>
							<i class="icon-angle-right"></i>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-title">
							<h3><i class=" icon-plus-sign"></i><?php echo $judul_form." ".$sub_judul_form;?> 
							</h3>		
						</div>
                            
 						<div class="box-content">
							<?php echo form_open('peruntukkan/simpanTambahPeruntukkan',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));
							?>
								
							<?php 
                               if ($this->session->flashdata('message_gagal')) {
                               echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                }

                                 if ($this->session->flashdata('message_sukses')) {
                                  echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                }
                            ?>
							
							<input type="hidden" name="id" id="id" class="input-xxlarge"  value="<?php echo isset($field['id'])?$field['id']:'';?>">

							 <?php 
								if (isset($field1['id_peruntukkan'])) { $dis="disabled";} else { $dis="";}							  
								$id_param= isset($field2['id_param'])?$field2['id_param']:$this->input->post('id_param');
								$id_peruntukkan= isset($field1['id_peruntukkan'])?$field1['id_peruntukkan']:$this->input->post('id_peruntukkan');	
							?>

					  		<div class="control-group">
								<label for="textfield" class="control-label">ID Peruntukkan</label>
								<div class="controls">
									<input type="text" name="id_peruntukkan" id="id_peruntukkan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($autoincid['auto_id_peruntukkan'])?$autoincid['auto_id_peruntukkan']:'';?> " readonly="readonly">
								</div>
							</div>

							<div class="control-group">
								<label for="textfield" class="control-label">Nama Peruntukkan</label>
								<div class="controls">
									
									<input type="text" <?php echo $dis; ?>  name="nama_peruntukkan" id="nama_peruntukkan" class="input-xxlarge" data-rule-required="true">
								</div>
							</div>
										
							<div class="control-group">
								<label for="textfield" class="control-label">Kategori Parameter</label>
								<div class="controls">
									<select name="id_param" id="id_param" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >
										<option value="general">Tampil Kategori</option>
										<?php foreach($field2 as $row2) { ?>
	                             
	                             		<option value="<?php echo $row2['id_param']; ?>" ><?php echo $row2['nama_param']; ?></option>
	                            		<?php } ?>
	                            	</select>
		                        </div>
							</div>
                                    
							<div class="form-actions">
								<button class="btn btn-primary" type="submit">Simpan</button>
                                <a class="btn btn-danger"  href="<?php echo site_url();?>peruntukkan/">Kembali</a>
                            </div>
						</div>       
					</div>
				</div>
			</div>


