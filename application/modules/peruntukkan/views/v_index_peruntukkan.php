		<div class="container-fluid" style="margin-top: 45px;">
			<div class="page-header">
				<div class="pull-left">
					<h1>Data Peruntukkan</h1>
				</div>
				<div class="pull-right">
					<ul class="stats">
						<li class="lightred">
							<i class="icon-calendar"></i>
							<div class="details">
								<span class="big">October 20, 2016</span>
								<span>Thursday, 11:17</span>
							</div>
						</li>
					</ul>
				</div>
			</div>
            <div class="breadcrumbs">
				<ul>
					<li>
						<a href="#">Setting</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo site_url();?>Peruntukkan">Data Peruntukkan</a>
						<i class="icon-angle-right"></i>
					</li>
				</ul>
				<div class="close-bread">
					<a href="#"><i class="icon-remove"></i></a>
				</div>
			</div>	
		</div>
			
		<div class="row-fluid">
			<div class="span12">
				<div class="box">
					<div class="box-content">
						<form action="<?php echo site_url('peruntukkan/cari'); ?>" method="post" name="form1" class="form-horizontal form-bordered">

							<?php 
                                if ($this->session->flashdata('message_gagal')) {
                                echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                }

                                if ($this->session->flashdata('message_sukses')) {
                                echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                }
                            ?>

							<div align="right">
                            	<a class="btn btn-green" href="<?php echo site_url();?>peruntukkan/tambah"><i class="icon-plus-sign"></i>Tambah Data Peruntukkan</a>
                            </div>

                            <?php 
								if (isset($ListDataPeruntukkan['id_peruntukkan'])) { $dis="disabled";} else { $dis="";}			  
								$nama_peruntukkan= isset($ListDataPeruntukkan['nama_peruntukkan'])?$ListDataPeruntukkan['nama_peruntukkan']:$this->input->post('nama_peruntukkan');			
							?>
								
							<div class="control-group">
								<label class="control-label" for="textfield">Pencarian</label>
								<div class="controls">
								<input type="text" value="<?php echo $this->session->userdata('nama_peruntukkan'); ?>" "<?php if ($nama_peruntukkan== ['nama_peruntukkan']) { echo "selected";} ?> class="form-control" name="nama_peruntukkan" placeholder="Masukan kata kunci..."  >	
							  	</div>
							</div>
					
							<table width="100%" class="table table-hover">
	    						<thead>
									<tr>
										<th>ID Peruntukkan</th>
										<th>Nama Peruntukkan</th>
										<th>kategori</th>
										<th>aksi</th>
										<th>&nbsp;</th>
	    							</tr>
								</thead>
								<tbody>
									<?php
									if (count($ListDataPeruntukkan) > 0) {
										foreach($ListDataPeruntukkan as $row)
										{
									
										?>
									<tr>
					  					<td><?php echo $row['id_peruntukkan']; ?></td>
										<td><?php echo $row['nama_peruntukkan']; ?></td>
										<td><?php echo $row['nama_param']; ?></td>
									    <td><a class="btn btn-mini btn-warning" href="<?php echo site_url('peruntukkan/ubah/'.$row['id_peruntukkan']); ?>"><i class="icon-pencil"></i> Ubah</a></td>
									    <td>
										<a class="btn btn-mini btn-danger" href="<?php echo site_url('peruntukkan/hapus/'.$row['id_peruntukkan']);?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>	
									    </td>
								    </tr>

									<?php
									
									$paging=(!empty($pagermessage) ? $pagermessage : '');
											
										}
										echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
									} else {
										echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
									}
									?>
								</tbody>
							</table>									
						</form>
					</div>
				</div>
			</div>
		</div> 
