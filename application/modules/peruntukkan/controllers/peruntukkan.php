<?php
class Peruntukkan extends CI_Controller{
    //put your code here
    public function __construct() {
       
        parent::__construct();
		$this->atos_tiasa_leubeut();

    }
    
	public function atos_tiasa_leubeut(){
		
		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}
    }
	
    public function index() {
	
		$data["test"]="";

		$per_page = 10;
		
	    $sql_peruntukkan = "select 
					p.*,
					q.*
					from ref_peruntukkan p 
					left join m_parameter_potensi q on p.id_param=q.id_param
					order by p.id_peruntukkan asc";

		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
		$config['total_rows'] = $this->db->query($sql_peruntukkan)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 3;
		$config['base_url']= base_url().'/peruntukkan/index'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(3);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($sql_peruntukkan)->num_rows();
		}   
		
		$sql_peruntukkan .= " limit {$per_page} offset {$offset} ";

		$this->data['ListDataPeruntukkan'] = $this->db->query($sql_peruntukkan)->result_array();
		
		$this->template->load('rorompok','v_index_peruntukkan',$this->data);
		
    }

    function tambah(){

    	$data['judul_form']="Tambah Data Parameter Peruntukkan";
		$data['sub_judul_form']="";

		$qry1 = "SELECT 
				  P.*,
				  Q.* 
				 FROM ref_peruntukkan P
				 LEFT JOIN m_parameter_potensi Q ON P.id_param=Q.id_param";

		$maxid = "select max(p.id_peruntukkan) + 1 as auto_id_peruntukkan from ref_peruntukkan p";
		// echo $maxid;

		$qry2 = "SELECT 
				  Q.* 
				 FROM m_parameter_potensi Q ";

		$query1 = $this->db->query($qry1);
		$query2 = $this->db->query($qry2);
		$query3 = $this->db->query($maxid);
   		// $data['field1'] = $query1->row_array();
   		$data['autoincid'] = $query3->row_array();
   		$data['field2'] = $this->db->query($qry2)->result_array();
		$this->template->load('rorompok','v_add_peruntukkan',$data);

    }
	
	function ubah(){

		$id_param = $this->input->post('id_peruntukkan');
		$id_param = $this->uri->segment(3);
   		$data['judul_form']="Ubah Data Paramater Peruntukkan";
		$data['sub_judul_form']="";
		// echo "id peruntukkan : '".$id_param."' ";

		$sql1 = "select 
					p.*,
					q.*
					from ref_peruntukkan p 
					left join m_parameter_potensi q on p.id_param=q.id_param
				  where p.id_peruntukkan = '".$id_param."' ";

		$sql3 = "select 
					q.*
					from  m_parameter_potensi q ";

		$query1 = $this->db->query($sql1);
		$query3 = $this->db->query($sql3);
   		$data['field1'] = $query1->row_array();
   		$data['field3'] = $this->db->query($sql3)->result_array();
		$this->template->load('rorompok','v_update_peruntukkan',$data);

	}

	function hapus($id_peruntukkan){
		   		
		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 14-Des-2016
		Desc     : Proses Menghapus Data By ID parameter potensi
		*/

		$id_peruntukkan = $this->uri->segment(3);

		$this->db->where('id_peruntukkan', $id_peruntukkan);
		$this->db->delete('ref_peruntukkan');

		$data['judul_form']="Tambah Data Parameter Potensi";
		$data['sub_judul_form']="";
		//  $data['sub_judul_form']="Data KUesioner";

		$this->session->set_flashdata('message_sukses', 'Data Peruntukkan Berhasil Dihapus');
   		redirect('peruntukkan');
	}

	function detail(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 14-Des-2016
		Desc     : Proses Menghapus Data By ID
		*/

		$id_param = $this->uri->segment(3);

		$id_user=$this->uri->segment(3);
			
		$per_page = 30;  

		$qry = "SELECT 
		M.*
		FROM
		m_parameter_potensi M
		WHERE M.id_param = '".$id_param."' ";

		$offset = ($this->uri->segment(4) != '' ? $this->uri->segment(4):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/potensi/index_log/'.$id_user.'/'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(4);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		$qry .= " limit {$per_page} offset {$offset} ";
		$this->data['ListData'] = $this->db->query($qry)->result_array();     
		
	    $query = $this->db->query($qry);
	    $this->data['field_kuesioner'] = $query->row_array();
		
		$this->data['sub_judul_form']="Detail Potensi";	
		$this->template->load('rorompok','v_index_log',$this->data);

	}

	function cari(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 15-Des-2016
		Desc     : Operasi mencari data parameter peruntukkan berdasarkan keyword	
		*/

		$this->data['sub_judul_form']="Hasil Pencarian";
		
		$id_user=$this->uri->segment(3);

		$nama_peruntukkan = $_POST['nama_peruntukkan'];



		if ($nama_peruntukkan == "") {
			redirect('peruntukkan');
		}else{

			$nama_peruntukkan = $_POST['nama_peruntukkan'];

		$qry2 = " SELECT 
				P.id_peruntukkan,
				P.nama_peruntukkan,
				Q.nama_param
				FROM
				ref_peruntukkan P
				LEFT JOIN m_parameter_potensi Q ON P.id_param=Q.id_param
				WHERE P.nama_peruntukkan LIKE '%".$nama_peruntukkan."%'
				";

		$data['ListDataCariPeruntukkan'] = $this->db->query($qry2)->result_array();  
		$this->template->load('rorompok','v_peruntukkan_search',$data);
		
		}
	}

	function simpanTambahPeruntukkan(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 01-Februari-2017
		Desc     : Operasi Simpan (Tambah Data Peruntukkan)
		*/

		 try {

				$id_peruntukkan = $this->input->post('id_peruntukkan');
				if($id_peruntukkan != null){

				$query = $this->db->query("SELECT A.* 
											FROM ref_peruntukkan A");
				$cek=$query->num_rows(); 

					$data = array(			
					'id_peruntukkan' 		=> $this->input->post('id_peruntukkan'),
					'nama_peruntukkan' 		=> $this->input->post('nama_peruntukkan'),
					'id_param' 				=> $this->input->post('id_param'));
					
					$this->db->insert('ref_peruntukkan', $data);
					
				$this->session->set_flashdata('message_sukses', 'Data Peruntukkan Berhasil Ditambah');
				redirect('peruntukkan');

				} else {

					$this->session->set_flashdata('message_gagal', 'Username Yang Anda Masukan Sudah Ada, Silahkan Ganti Dengan Yang Lain !!!');
					$this->tambah();
				
				}

			}
			catch(Exception $err){
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}

	}

	function simpanUbahPeruntukkan(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 1-Februari-2017
		Desc     : Operasi Update / Perbarui Data Parameter Peruntukkan
		*/

		try{

			$id_peruntukkan = $this->input->post('id_peruntukkan');

			if($id_peruntukkan != null){

			$data = array(
				'id_peruntukkan' 		=> $this->input->post('id_peruntukkan'),
				'nama_peruntukkan' 		=> $this->input->post('nama_peruntukkan'),
				'id_param' 				=> $this->input->post('id_param'));
			
			$this->db->where('id_peruntukkan', $id_peruntukkan);
			$this->db->update('ref_peruntukkan', $data);

			}else{
				echo "salah";
			}
			
			$this->session->set_flashdata('message_sukses', 'data peruntukkan berhasil diubah');
			redirect('peruntukkan');

			}
			catch(Exception $err)
			{
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}

	}
	
}