			<div class="container-fluid" style="margin-top: 45px;">
				<br>
				<div class="page-header">
					<div class="pull-left"></div>
					<div class="pull-right">
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<br>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>kuesioner">Data Master Kuesioner</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="">Detail Data Kuesioner</a>
							<i class="icon-angle-right"></i>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									<?php echo $sub_judul_form;?>
								</h3>
							</div>
							<div class="box-content">	
								<table width="100%" class="table table-hover">
							    <thead>
									<tr>
										<th>ID Jawaban</th>
										<th>ID Kuesioner</th>
										<th>Pertanyaan</th>
										<th>Jawaban</th>
										<th>Status Keaktifan</th>
										<th>Aksi</th>
									    <th>&nbsp;</th>
	    							</tr>
								</thead>
								<tbody>
									<?php
									if (count($ListData) > 0) {
										foreach($ListData as $row)
										{
									
										?>
									<tr>
										<td><?php echo $row['id_pil_jawab']; ?></td>
					  					<td><?php echo $row['no_pertanyaan']; ?></td>
										<td><?php echo $row['pertanyaan']; ?></td>
										<td><?php echo $row['nama_pil_jawab']; ?></td>
										<td><?php 
										if ($row['status_aktif']==1) { echo "Aktif";}  
										elseif ($row['status_aktif']==0) { echo "Tidak Aktif";}  
										?></td>
									  	<td><a class="btn btn-mini btn-warning" href="<?php echo site_url();?>kuesioner/ubah/<?php echo $row['id_pil_jawab']; ?>"><i class="icon-pencil	"></i> Ubah</a></td>
									</tr>
									<?php
									
									$paging=(!empty($pagermessage) ? $pagermessage : '');
											
										}
										echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
									} else {
										echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
									}
									?>
								</tbody>
							</table>	
						</form>		
					</div>
				</div>
			</div>
		</div> 
