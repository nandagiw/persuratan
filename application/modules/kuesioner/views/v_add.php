		<link rel="stylesheet" href="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <script src="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
        <script src="<?php echo base_url(); ?>https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

		<div class="container-fluid" style="margin-top: 45px;">
			<br>
			<div class="breadcrumbs">
				<ul>
					<li>
						<a href="<?php echo site_url();?>kuesioner">Data Pertanyaan Kuesioner</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="#">Tambah Data Kuesioner</a>
						<i class="icon-angle-right"></i>
					</li>
				</ul>
				<div class="close-bread">
					<a href="#"><i class="icon-remove"></i></a>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<div class="box">
					<div class="box-title">
						<h3><i class=" icon-plus-sign"></i><?php echo $judul_form." ".$sub_judul_form;?></h3>
					</div>
                        
					<div class="box-content">
					
						<?php echo form_open('kuesioner/simpan',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
							
						<?php 
	                        if ($this->session->flashdata('message_gagal')) {
	                        echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
	                        }

	                        if ($this->session->flashdata('message_sukses')) {
	                        echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
	                        }
	                    ?>
                               
						<input type="hidden" name="id_kuesioner" id="id_kuesioner" class="input-xxlarge"  value="<?php echo isset($field['id_kuesioner'])?$field['id_kuesioner']:'';?>">

						<?php 
						if (isset($field['id_kuesioner'])) { $dis="disabled";} else { $dis="";}							  
						$jawaban= isset($field['nama_pil_jawab'])?$field['nama_pil_jawab']:$this->input->post('jawaban'); 
						$status_aktif= isset($field['status_aktif'])?$field['status_aktif']:$this->input->post('status_aktif'); 
						$id_pil_jawab= isset($field['id_pil_jawab'])?$field['id_pil_jawab']:$this->input->post('id_pil_jawab'); 	
				  		?>

                        <div class="control-group">
							<label for="textfield" class="control-label">Nomor Pertanyaan</label>
							<div class="controls">
								<input type="text" name="no_pertanyaan" id="no_pertanyaan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['auto_id_kuesioner'])?$field1['auto_id_kuesioner']:'';?> " readonly="readonly">
							</div>
						</div>
								
						<div class="control-group">
							<label for="textfield" class="control-label">Pertanyaan</label>
							<div class="controls">
								<input type="text" name="pertanyaan" id="pertanyaan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['pertanyaan'])?$field['pertanyaan']:'';?>">
							</div>
						</div>

						<div class="control-group">
							<label for="textfield" class="control-label">Jawaban</label>
	                       <table id="dynamic_field">
		                        <div class="controls">
			                        <input type="text" name="nama_pil_jawab[]" id="nama_pil_jawab[]" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nama_pil_jawab'])?$field['nama_pil_jawab']:'';?>" visible="false">
			                        <button type="button" name="add" id="add" class="btn btn-success">Tambah Jawaban</button>  
		                        </div>
	                       </table>  
						</div> 

					 	<script>  
						 $(document).ready(function(){  
						      var i=1;  
						      $('#add').click(function(){  
						           i++;  
						           $('#dynamic_field').append(
						           	'<table id="dynamic_field" style="margin-left:160px; margin-top:-12px;"><div class="control-group"><label for="textfield" class="control-label"></label><div class="controls"><tr id="row'+i+'"><td><input type="text" name="nama_pil_jawab[]" id="nama_pil_jawab[]" placeholder="Masukkan jawaban anda" class="input-xxlarge" value="<?php echo isset($field["nama_pil_jawab"])?$field["nama_pil_jawab"]:'';?>"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr></div></div></table>');  
						      });  
						      $(document).on('click', '.btn_remove', function(){  
						           var button_id = $(this).attr("id");   
						           $('#row'+button_id+'').remove();  
						      });  
						      $('#submit').click(function(){            
						           $.ajax({  
						                url:"name.php",  
						                method:"POST",  
						                data:$('#add_name').serialize(),  
						                success:function(data)  
						                {  
						                     alert(data);  
						                     $('#add_name')[0].reset();  
						                }  
						           });  
						      });  
							 });  
							 </script>

			            <table id="dynamic_field">
							<div class="control-group">
								<label for="textfield" class="control-label">Status Keaktifan</label>
								<div class="controls">
									<select name="status_aktif" id="status_aktif" class="input-xxlarge" data-rule-required="true" onchange="doShow(this.value);" >
			                         <option value="">-Pilih-</option>
			                          <option value="1" <?php if ($status_aktif==1) { echo "selected";} ?>>Aktif</option>
									  <option value="0" <?php if ($status_aktif==0) { echo "selected";} ?>>Tidak Aktif</option>
			                        </select>		
								</div>
							</div>
						</table>

						<div class="form-actions">
							<button class="btn btn-primary" type="submit">Simpan</button>
		                    <a class="btn btn-danger" href="<?php echo site_url();?>kuesioner/">Kembali</a>
						</div>
					</div>
				</div>
			</div>		
		</div>

		<script type="text/javascript">

		/*
		$(document).ready(function(){
		  $("#lookup_table_div").hide();
		});
		*/

		<?php 
		if ($jawaban==3 or $jawaban==4) { ?>
		$("#lookup_table_div").show();
		<?php } else { ?>
		$("#lookup_table_div").hide();
		<?php } ?>

		function doShow(tipe_isian) {
			
			if (tipe_isian==3 || tipe_isian==4) {
			$("#lookup_table_div").show();
			//document.getElementById("lookup_table_div").style.visibility = "visible"; 
			
			} else {
			$("#lookup_table_div").hide();
			//document.getElementById("lookup_table_div").style.visibility = "hidden"; 
			
			}
			
		}

		</script>


