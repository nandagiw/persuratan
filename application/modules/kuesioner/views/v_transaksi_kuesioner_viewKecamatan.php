		<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Data Transaksi Jawaban Kuesioner</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
                <div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Setting</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo site_url();?>kuesioner">Data Pertanyaan Kuesioner</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				
			</div>


						<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">

							<?php echo form_open('kuesioner/LihatTransaksiKuesionerbyKecamatan',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));
							?>
							<?php 							  
								$kode_kecamatan= isset($field['kode_kecamatan'])?$field['kode_kecamatan']:$this->input->post('kode_kecamatan');				
							?>
						<div class="box box-color box-bordered" >
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Statistik Jawaban Kuesioner Berdasarkan Kecamatan
								</h3>
							</div>
							<div class="box-content" style="height: 480px;overflow-y: scroll;">
								
							<select name="kode_kecamatan" id="kode_kecamatan" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

											<option value="all">Tampil Data Kelurahan berdasarkan Kecamatan</option>

											<?php foreach($ComboKec as $row1) { ?>
				                             
				                             <option value="<?php echo $row1["kode_kecamatan"]; ?>" <?php if ($kode_kecamatan== $row1['kode_kecamatan']) { echo "selected";} ?>><?php echo $row1['nama_kec']; ?></option>
				                            <?php } ?>
				                            </select>

				                            <div>
										<br>
										<button class="btn btn-primary" type="submit" name="kecamatan">Cari</button>
											</div>

							<script src="<?php echo base_url()?>tema/amcharts/amcharts.js" type="text/javascript"></script>
				<script src="<?php echo base_url()?>tema/amcharts/serial.js" type="text/javascript"></script>
				<script src="<?php echo base_url()?>tema/amcharts/pie.js" type="text/javascript"></script>

							<script type="text/javascript">

			var chart = AmCharts.makeChart("xx", {
    "type": "serial",
	"theme": "light",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
		"useGraphSettings": true,
		"markerSize": 10
    },
    "dataProvider": [
	<?php $s=1; foreach($GrafikTransaksiKuesionerKecamatan as $row)
					{ ?>

	{
                    "year": "<?php echo 'pertanyaan ' .$row['id_kuesioner']; ?>",
                    "KOL1": <?php echo $row['jml_ya']; ?>,
                    "KOL2": <?php echo $row['jml_tidak']; ?>
                } ,
				<?php #if ($s==$x) { echo "";} else { echo ",";} ?>
				
			<?php $s++; } ?>
	
	],
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]] %</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Jawaban Ya",
        "type": "column",
		"color": "#000000",
        "valueField": "KOL1"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]] %</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Jawaban Tidak",
        "type": "column",
		"color": "#000000",
        "valueField": "KOL2"
    }],
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left",
		"labelRotation": 45
    },
    "export": {
    	"enabled": true
     }
});
        </script>
			   <div id="xx" style="width: 100%; height: 500px;"></div>
						</div>
						</div>
						</div>
						</form>
			

			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">

							
								
								<form action="<?php echo site_url('kuesioner/cari'); ?>" method="post" name="form1" class="form-horizontal form-bordered">

							<div align="right">

                            </div>

                            
							<?php 
								if (isset($field['id_kuesoner'])) { $dis="disabled";} else { $dis="";}				  
								$pertanyaan= isset($field['pertanyaan'])?$field['pertanyaan']:$this->input->post('pertanyaan');				
							?>

                            <?php 
								if (isset($field['id_kuesoner'])) { $dis="disabled";} else { $dis="";}			  
								$pertanyaan= isset($field['pertanyaan'])?$field['pertanyaan']:$this->input->post('pertanyaan');			
							?>
								
									<div class="control-group">
										<!-- <label class="control-label" for="textfield">Pencarian</label>
										<div class="controls">
										<input type="text" value="<?php echo $this->session->userdata('pertanyaan'); ?>" "<?php if ($pertanyaan== ['pertanyaan']) { echo "selected";}  ?> class="form-control" name="pertanyaan" placeholder="Masukan kata kunci..."  >	
									  	</div> -->
									</div>

						<div class="box box-color box-bordered" >
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Statistik Jawaban Kuesioner Berdasarkan Kecamatan
								</h3>
							</div>
							<div class="box-content" style="height: 350px;overflow-y: scroll;">
					
		<table width="100%" class="table table-hover table-bordered dataTable-scroll-x dataTable-scroll-y" border="1">
	    <thead>
				<tr>
								<th rowspan="2" colspan="1" style="text-align: center;">Kecamatan</th>
								<th colspan="2">Pertanyaan1</th>
								<th colspan="2">Pertanyaan2</th>
								<th colspan="2">Pertanyaan3</th>
								<th colspan="9" style="text-align: center;">Pertanyaan4</th>
								<th colspan="2">Pertanyaan5</th>
								<th colspan="2">Pertanyaan6</th>
								<th colspan="2">Pertanyaan7</th>
								<th colspan="2" style="text-align: center;">Wilayah Berizin?</th>
								<th rowspan="2" colspan="1">Total</th>
	    </tr>
		</thead>
		<thead>
				<tr>
								<th>&nbsp;</th>
								<th>ya</th>
								<th>tidak</th>
								<th>ya</th>
								<th>tidak</th>
								<th>ya</th>
								<th>tidak</th>
								<th>Internet</th>
								<th>Sosial Media</th>
								<th>Radio</th>
								<th>Video Tron</th>
								<th>Billboard</th>
								<th>Media Cetak</th>
								<th>Kerabat</th>
								<th>Petugas Layanan</th>
								<th>Lainnya</th>
								<th>ya</th>
								<th>tidak</th>
								<th>ya</th>
								<th>tidak</th>
								<th>ya</th>
								<th>tidak</th>
								<th>ya</th>
								<th>tidak</th>
								<th>&nbsp;</th>
								</tr>
		</thead>
		<tbody cellpadding="20">
			<?php
			if (count($Hero) > 0) {
				foreach($Hero as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>

				<tr>
								<td><?php echo $row['nama_kec']; ?></td>
								<td><?php echo $row['jml_p1_ya']; ?></td>
								<td><?php echo $row['jml_p1_tidak']; ?></td>
								<td><?php echo $row['jml_p2_ya']; ?></td>
								<td><?php echo $row['jml_p2_tidak']; ?></td>
								<td><?php echo $row['jml_p3_ya']; ?></td>
								<td><?php echo $row['jml_p3_tidak']; ?></td>

								<td><?php echo $row['jml_p4_internet']; ?></td>
								<td><?php echo $row['jml_p4_sosmed']; ?></td>
								<td><?php echo $row['jml_p4_radio']; ?></td>
								<td><?php echo $row['jml_p4_videotron']; ?></td>
								<td><?php echo $row['jml_p4_billboard']; ?></td>
								<td><?php echo $row['jml_p4_medcetak']; ?></td>
								<td><?php echo $row['jml_p4_kerabat']; ?></td>
								<td><?php echo $row['jml_p4_petlay']; ?></td>
								<td><?php echo $row['jml_p4_lain']; ?></td>

								<td><?php echo $row['jml_p5_ya']; ?></td>
								<td><?php echo $row['jml_p5_tidak']; ?></td>
								<td><?php echo $row['jml_p6_ya']; ?></td>
								<td><?php echo $row['jml_p6_tidak']; ?></td>
								<td><?php echo $row['jml_p7_ya']; ?></td>
								<td><?php echo $row['jml_p7_tidak']; ?></td>
								<td><?php echo $row['jml_berizin']; ?></td>
								<td><?php echo $row['jml_tidak_berizin']; ?></td>
								<td><?php echo $row['total_tidak']; ?></td>
								

								<?php
							
							// $paging=(!empty($pagermessage) ? $pagermessage : '');
									
								}
								// echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
							} else {
								echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
							}
							?>
							</tr>
		</tbody>
		<br>
				<thead>

				<tr>
				<th>Total</th>
								<?php
			if (count($satu_ya) > 0) {
				foreach($satu_ya as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p1_ya'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($satu_tidak) > 0) {
				foreach($satu_tidak as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p1_tidak'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($dua_ya) > 0) {
				foreach($dua_ya as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p2_ya'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($dua_tidak) > 0) {
				foreach($dua_tidak as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p2_tidak'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($tiga_ya) > 0) {
				foreach($tiga_ya as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p3_ya'].'%'; ?></th>
								<?php
									}
								}
								?>
								
								<?php
			if (count($tiga_tidak) > 0) {
				foreach($tiga_tidak as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p3_tidak'].'%'; ?></th>
								<?php
									}
								}
								?>
								
									<?php
			if (count($empat_internet) > 0) {
				foreach($empat_internet as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_internet'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_sosmed) > 0) {
				foreach($empat_sosmed as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_sosmed'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_radio) > 0) {
				foreach($empat_radio as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_radio'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_videotron) > 0) {
				foreach($empat_videotron as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_videotron'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_billboard) > 0) {
				foreach($empat_billboard as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_billboard'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_mediacetak) > 0) {
				foreach($empat_mediacetak as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_mediacetak'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_kerabat) > 0) {
				foreach($empat_kerabat as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_kerabat'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_petugaslayanan) > 0) {
				foreach($empat_petugaslayanan as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_petugaslayanan'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($empat_lainnya) > 0) {
				foreach($empat_lainnya as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p4_lainnya'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($lima_ya) > 0) {
				foreach($lima_ya as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p5_ya'].'%'; ?></th>
								<?php
									}
								}
								?>
								
								<?php
			if (count($lima_tidak) > 0) {
				foreach($lima_tidak as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p5_tidak'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($enam_ya) > 0) {
				foreach($enam_ya as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p6_ya'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($enam_tidak) > 0) {
				foreach($enam_tidak as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>
								<th><?php echo $row['jml__total_p6_tidak'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($tujuh_ya) > 0) {
				foreach($tujuh_ya as $row)
				{
					
				?>
								<th><?php echo $row['jml__total_p7_ya'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($tujuh_tidak) > 0) {
				foreach($tujuh_tidak as $row)

				{
					
				?>
								<th><?php echo $row['jml__total_p7_tidak'].'%'; ?></th>
								<?php
									}
								}
								?>

								<?php
			if (count($wb_ya) > 0) {
				foreach($wb_ya as $row)

				{
					
				?>
								<th><?php echo $row['jml_total_berizin'].'%'; ?></th>
								<?php
									}
								}
								?>
								
								<?php
			if (count($wb_tidak) > 0) {
				foreach($wb_tidak as $row)

				{
					
				?>
								<th><?php echo $row['jml_total_tidak_berizin'].'%'; ?></th>
								<?php
									}
								}
								?>

								</tr>
		</thead>
						</table>								
						</form>
						</div>
						</div>
						</div>


			
							</div>
						</div>
					</div>
				</div> 
