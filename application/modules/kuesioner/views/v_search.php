		<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Data Pertanyaan Kuesioner (Hasil Pencarian)</h1>
					</div>
					<div class="pull-right">
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>

			<div class="container-fluid">
				<br>
                <div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Setting</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo site_url();?>kuesioner">Data Pertanyaan Kuesioner</a>
							<i class="icon-angle-right"></i>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">
								<form action="<?php echo site_url('kuesioner/cari'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
						
								<div align="right">
	                            <a class="btn btn-green" href="<?php echo site_url();?>kuesioner/tambah"><i class="icon-plus-sign"></i>Tambah Data</a>
	                            </div>

	                            <?php 
									if (isset($field['id_kuesoner'])) { $dis="disabled";} else { $dis="";}							  
									$pertanyaan= isset($field['pertanyaan'])?$field['pertanyaan']:$this->input->post('pertanyaan');
												
								  ?>
								
								<div class="control-group">
									<label class="control-label" for="textfield">Pencarian</label>
									<div class="controls">
									<input type="text" value="<?php echo $pertanyaan; ?>" class="form-control" name="pertanyaan" placeholder="Masukan kata kunci..."  >	
								  </div>
								</div>
					
								<table width="100%" class="table table-hover">
							    	<thead>
										<tr>
											<th>ID Kuesioner</th>
											<th>Pertanyaan</th>
											<th>Status Keaktifan</th>
											<th>Aksi</th>
										    <th>&nbsp;</th>
										    <th>&nbsp;</th>
										    <th>&nbsp;</th>
							    		</tr>
									</thead>
									<tbody>
										<?php
										if (count($ListData1) > 0) {
											foreach($ListData1 as $row)
											{
												?>

										<tr>
											<td><?php echo $row['no_pertanyaan']; ?></td>
											<td><?php echo $row['pertanyaan']; ?></td>
											<td><?php if ($row['status_aktif']==1) { echo "Aktif";}  
											elseif ($row['status_aktif']==0) { echo "Tidak Aktif";}  
											?></td>
								          	<td><a class="btn btn-mini btn-warning" href="<?php echo site_url('kuesioner/ubah/'.$row['id_kuesioner']);?>" ><i class="icon-pencil"></i>Ubah</a></td>
										  	<td><a class="btn btn-mini btn-danger" href="<?php echo site_url('kuesioner/hapus/'.$row['id_kuesioner']);?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a></td>
								         	<td><a class="btn btn-mini btn-primary " href="<?php echo site_url('kuesioner/index_log/'.$row['id_kuesioner']);?>"><i class="icon-eye-open"></i> Lihat Detail</a></td>
										</tr>

										<?php
										
										$paging=(!empty($pagermessage) ? $pagermessage : '');
												
											}
											echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
										} else {
											echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
										}
										?>
									</tbody>
								</table>	
							</form>	
						</div>
					</div>
				</div>
			</div> 
