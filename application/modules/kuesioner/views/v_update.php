			<link rel="stylesheet" href="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="<?php echo base_url(); ?>https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

			<div class="container-fluid" style="margin-top: 45px;">
				<br>
				<div class="page-header">
					<div class="pull-left">
					</div>
					<div class="pull-right">
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<br>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>kuesioner">Data Pertanyaan Kuesioner</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Tambah Data Kuesioner</a>
							<i class="icon-angle-right"></i>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-title">
							<h3><i class=" icon-plus-sign"></i><?php echo $judul_form." ".$sub_judul_form;?> </h3>
						</div>
                            
 						<div class="box-content">
 
							<!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
							<?php echo form_open('kuesioner/simpanUpdate',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
								
							<?php 
                                if ($this->session->flashdata('message_gagal')) {
                                echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                }

                                if ($this->session->flashdata('message_sukses')) {
                                echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                }

                            ?>
										
							<input type="hidden" name="id_kuesioner" id="id_kuesioner" class="input-xxlarge"  value="<?php echo isset($field1['id_kuesioner'])?$field1['id_kuesioner']:'';?>">

							<input type="hidden" name="id_pil_jawab" id="id_pil_jawab" class="input-xxlarge"  value="<?php echo isset($field1['id_pil_jawab'])?$field1['id_pil_jawab']:'';?>">

							<?php 
							if (isset($field1['id_kuesioner'])) { $dis="disabled";} else { $dis="";}							  
							$nama_pil_jawab= isset($field1['nama_pil_jawab'])?$field1['nama_pil_jawab']:$this->input->post('nama_pil_jawab'); 
							$status_aktif= isset($field1['status_aktif'])?$field1['status_aktif']:$this->input->post('status_aktif'); 
							$id_pil_jawab= isset($field1['id_pil_jawab'])?$field1['id_pil_jawab']:$this->input->post('id_pil_jawab'); 
					  		?>

                            <div class="control-group">
								<label for="textfield" class="control-label">Nomor Pertanyaan</label>
								<div class="controls">
									<!-- <?php echo "tes : ".$id_pil_jawab; ?> -->
									<input type="text" name="no_pertanyaan" id="no_pertanyaan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['no_pertanyaan'])?$field1['no_pertanyaan']:'';?>">
								</div>
							</div>
										
							<div class="control-group">
								<label for="textfield" class="control-label">Pertanyaan</label>
								<div class="controls">
									<input type="text" name="pertanyaan" id="pertanyaan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['pertanyaan'])?$field1['pertanyaan']:'';?>">
								</div>
							</div>
 
							<div class="control-group">
								<label for="textfield" class="control-label">Jawaban</label>
									<div class="controls">
										<input type="text" name="nama_pil_jawab" id="nama_pil_jawab" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field1['nama_pil_jawab'])?$field1['nama_pil_jawab']:'';?>">
									</div>
							</div>
								               
							<table id="dynamic_field">
								<div class="control-group">
									<label for="textfield" class="control-label">Status Keaktifan</label>
									<div class="controls">
										<select name="status_aktif" id="status_aktif" class="input-xxlarge" data-rule-required="true" onchange="doShow(this.value);" >
				                            <option value="">-Pilih-</option>
				                            <option value="1" <?php if ($status_aktif==1) { echo "selected";} ?>>Aktif</option>
											<option value="0" <?php if ($status_aktif==0) { echo "selected";} ?>>Tidak Aktif</option>
         								</select>												
									</div>
								</div>
							</table>

							<div class="form-actions">
								<button class="btn btn-primary" type="submit">Simpan</button>
                                <a class="btn btn-danger" href="<?php echo site_url();?>kuesioner/">Kembali</a>
								
							</div>
						</form>
					</div>  
				</div>
			</div>
		</div>

		<script type="text/javascript">

			/*
			$(document).ready(function(){
			  $("#lookup_table_div").hide();
			});
			*/



			<?php 
			if ($jawaban==3 or $jawaban==4) { ?>
			$("#lookup_table_div").show();
			<?php } else { ?>
			$("#lookup_table_div").hide();
			<?php } ?>

			function doShow(tipe_isian) {


				
				if (tipe_isian==3 || tipe_isian==4) {
				$("#lookup_table_div").show();
				//document.getElementById("lookup_table_div").style.visibility = "visible"; 
				
				} else {
				$("#lookup_table_div").hide();
				//document.getElementById("lookup_table_div").style.visibility = "hidden"; 
				
				}
				

			}

		</script>


