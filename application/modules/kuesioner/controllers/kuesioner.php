<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kuesioner extends CI_Controller{

    public function __construct() {
       
        parent::__construct();
		$this->load->library('encrypt');
		$this->atos_tiasa_leubeut();
    
    }
    
	public function atos_tiasa_leubeut(){
	
	   /*
		Author   : Joe
		Modified :
		Date     : Date     : 4-Nov-2016
		Desc     : Data Survey
		*/

		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}
    }
	
	
	public function index( $offset = 0 ) {
	
		/*
		Author   : Joe
		Modified :
		Date     : 4-Nov-2016
		Desc     : Menampilkan List Data dan Hasil Pencarian		
		*/

		if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);			
		}

		$per_page = 20;  
		$qry = "SELECT 
		A.*
		FROM
		m_kuesioner A";
		
		$qry.= "
		"; 


		$qry2 = "select 
				 w.*,
				 (select count(v.id_jawab_kuesioner) as jml_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Ya' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
				 (select count(v.id_jawab_kuesioner) as jml_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y where v.jawaban='Tidak' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)
				 from m_kuesioner w ";
		//echo $qry;

		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 3;
		$config['base_url']= base_url().'/datasurvey/index'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(3);      
		$this->data['offset'] = $offset ;
		
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   

		$qry .= " limit {$per_page} offset {$offset} ";

		$this->data['ListData'] = $this->db->query($qry)->result_array(); 
		$this->data['GrafikTransaksiKuesioner'] = $this->db->query($qry2)->result_array();     
		$this->data['sub_judul_form']="Data Kuesioner";	
		$this->template->load('rorompok','v_index',$this->data);	
   }
	
   public function hapus($id_kuesioner) {
   
   		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 09-Nov-2016
		Desc     : Proses Menghapus Data By ID
		*/

		$id_kuesioner = $this->uri->segment(3);

		$this->db->where('id_kuesioner', $id_kuesioner);
		$this->db->delete('m_pilihan_jawaban');

		$this->db->where('id_kuesioner', $id_kuesioner);
		$this->db->delete('m_kuesioner');

		$data['judul_form']="Tambah Data Kuesioner";
		$data['sub_judul_form']="";
		//  $data['sub_judul_form']="Data KUesioner";
   		redirect('kuesioner');
   
   }
   
   public function index_log( $offset = 0 ) {
			
		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 09-Nov-2016
		Desc     : Menampilkan List Data Log By User ID	
		*/
			
		$id_user=$this->uri->segment(3);
		
		$per_page = 30;  

		$qry = "SELECT 
		M.*,
		P.id_pil_jawab,
		P.nama_pil_jawab
		FROM
		m_kuesioner M
		LEFT JOIN m_pilihan_jawaban P ON M.id_kuesioner=P.id_kuesioner
		WHERE M.id_kuesioner='".$id_user."' ";

		$offset = ($this->uri->segment(4) != '' ? $this->uri->segment(4):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/kuesioner/index_log/'.$id_user.'/'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(4);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		$qry .= " limit {$per_page} offset {$offset} ";
		$this->data['ListData'] = $this->db->query($qry)->result_array();     
		
	    $query = $this->db->query($qry);
	    $this->data['field_kuesioner'] = $query->row_array();
		
		$this->data['sub_judul_form']="Detail Kuesioner";	
		$this->template->load('rorompok','v_index_log',$this->data);	

   }

   function tambah(){

   		$data['judul_form']="Tambah Data Kuesioner";
		$data['sub_judul_form']="";

		$query = "SELECT A.*,
							B.id_pil_jawab
							B.nama_pil_jawab 
							FROM m_kuesioner A
							LEFT JOIN m_pilihan_jawaban B ON A.id_kuesioner=B.id_kuesioner";
		
		$maxid = "select max(k.id_kuesioner) + 1 as auto_id_kuesioner from m_kuesioner k";

		$query1 = $this->db->query($maxid);
		$data['field1'] = $query1->row_array();

		$this->template->load('rorompok','v_add',$data);

   }

   function ubah(){

		$id_user=$this->uri->segment(3);
		$id_kuesioner = $this->input->post('id_kuesioner');
   		$data['judul_form']="Ubah Data Kuesioner";
		$data['sub_judul_form']="";

		$sql1 = "SELECT A.*,
				B.id_pil_jawab,			
				B.nama_pil_jawab 
				FROM m_kuesioner A
				LEFT JOIN m_pilihan_jawaban B ON A.id_kuesioner=B.id_kuesioner
				WHERE B.id_pil_jawab ='".$id_user."' ";

		$query1 = $this->db->query($sql1);
   		$data['field1'] = $query1->row_array();
		$this->template->load('rorompok','v_update',$data);

   }

	function simpan(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 18-Nov-2016
		Desc     : Operasi Simpan (Tambah Data Kuesioner)
		*/

		 try {

				
			$jawaban = $this->input->post('jawaban');
			$id_kuesioner = $this->input->post('id_kuesioner');
			if($id_kuesioner=='' || $id_kuesioner==null){
			
			
			$query = $this->db->query("SELECT A.*,
										B.nama_pil_jawab 
										FROM m_kuesioner A
										LEFT JOIN m_pilihan_jawaban B ON A.id_kuesioner=B.id_kuesioner");
			$cek=$query->num_rows(); 

			$number = count($_POST["nama_pil_jawab"]);
			$array = $_POST["nama_pil_jawab"];

			$arr = json_encode($array);

			if ($cek!=0) {

				if(count($array) > 1){
					$jp = 2;
				}else{
					$jp = 1;
				}

				$data = array(			
				'no_pertanyaan' => $this->input->post('no_pertanyaan'),
				'jenis_pertanyaan' => $jp,
				'pertanyaan' => $this->input->post('pertanyaan'),
				'status_aktif' => $this->input->post('status_aktif'));
				$this->db->insert('m_kuesioner', $data);

				$insert_id = $this->db->insert_id();

				for($i=0;$i<count($array); $i++){

				$data1 = array(		
				'id_kuesioner' => $insert_id,	
				'nama_pil_jawab' => $array[$i]);
				$this->db->insert('m_pilihan_jawaban', $data1);
				}
					
				redirect('kuesioner');

				} else {
					$this->session->set_flashdata('message_gagal', 'Username Yang Anda Masukan Sudah Ada, Silahkan Ganti Dengan Yang Lain !!!');
					$this->tambah();
				
				}

				}else{

				}

			}
			catch(Exception $err)
			{
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}
	}

	function simpanUpdate(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 18-Nov-2016
		Desc     : Operasi Update / Perbarui Data Kuesioner
		*/

		 try {
				$id_user=$this->uri->segment(3);
				$jawaban = $this->input->post('jawaban');
				$id_pil_jawab = $this->input->post('id_pil_jawab');

				if($id_pil_jawab!='' || $id_pil_jawab!=null){
				
				
				$query = $this->db->query("SELECT A.*,
											B.id_pil_jawab,
											B.nama_pil_jawab 
											FROM m_kuesioner A
											LEFT JOIN m_pilihan_jawaban B ON A.id_kuesioner=B.id_kuesioner ");
				$cek=$query->num_rows(); 

				$id= $this->input->post('id_kuesioner');

				$data1 = array(
				'nama_pil_jawab' => $this->input->post('nama_pil_jawab'),
				);

				$this->db->where('id_pil_jawab', $id_pil_jawab);
				$this->db->where('id_kuesioner', $id);
				$this->db->update('m_pilihan_jawaban', $data1);
				// echo "tes :".$id;
				// echo "tes :".$id_pil_jawab;

				$data = array(
				'no_pertanyaan' => $this->input->post('no_pertanyaan'),
				'pertanyaan' => $this->input->post('pertanyaan'),
				'status_aktif' => $this->input->post('status_aktif'),
				);

				// $inserts = $this->db->insert_id();
				
				$this->db->where('id_kuesioner', $id);
				$this->db->update('m_kuesioner', $data);
				}else{
					echo "salah";
				}

				redirect('kuesioner');

			}
			catch(Exception $err)
			{
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}
	}

	function cari(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 18-Nov-2016
		Desc     : Operasi mencari data kuesioner berdasarkan keyword	
		*/

		$this->data['sub_judul_form']="Hasil Pencarian";
		
		$id_user=$this->uri->segment(3);

		$pertanyaan = $_POST["pertanyaan"];


		if ($pertanyaan == "") {
			redirect('kuesioner');
		}else{

			$pertanyaan = $_POST["pertanyaan"];
			
		$qry2 = "SELECT 
			A.*
			FROM
			m_kuesioner A
			WHERE A.pertanyaan LIKE '%$pertanyaan%' ";

		$data['ListData1'] = $this->db->query($qry2)->result_array();  
		$this->template->load('rorompok','v_search',$data);
		
		}

	}

	function LihatTransaksiKuesionerbyKecamatan(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 13-Jan-2017
		Desc     : Operasi mencari data kecamatan untuk transaksi lihat grafik kuesioner	
		*/

		$kode_kecamatan = $this->input->post('kode_kecamatan');

		$sql_kueskec = "select 
					a.*,

					(select count(v.id_jawab_kuesioner) as jml_p1_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p1_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					(select count(v.id_jawab_kuesioner) as jml_p2_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p2_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner), 

					(select count(v.id_jawab_kuesioner) as jml_p3_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p3_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					(select count(v.id_jawab_kuesioner) as jml_p4_internet from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Internet' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_sosmed from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Sosial Media' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_radio from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Radio' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_videotron from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Videotron' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_billboard from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Billboard' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_medcetak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Media Cetak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_kerabat from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Kerabat' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_petlay from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Petugas Layanan' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p4_lain from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Lainnya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					(select count(v.id_jawab_kuesioner) as jml_p5_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p5_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					(select count(v.id_jawab_kuesioner) as jml_p6_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p6_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					
					(select count(v.id_jawab_kuesioner) as jml_p7_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_p7_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					(select sum(v.id_jawab_kuesioner) as sum_p1_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select sum(v.id_jawab_kuesioner) as sum_p1_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					(select sum(v.id_jawab_kuesioner) as sum_p2_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select sum(v.id_jawab_kuesioner) as sum_p2_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(z.id_detail) as jml_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=1 and z.kode_kecamatan=A.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi),
					(select count(z.id_detail) as jml_tidak_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=2 and z.kode_kecamatan=A.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi),
					(select count(v.id_jawab_kuesioner) as total_tidak from tr_detail_survey z,tr_survey y, tr_jawab_kuesioner v, m_pilihan_jawaban x, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)
					from ref_kecamatan a ";
				
				$data['Hero'] = $this->db->query($sql_kueskec)->result_array();

				// $qry1 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p1_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry01 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='1' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p1_ya 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry2 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p1_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry02 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p1_tidak 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry3 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p2_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry03 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p2_ya 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry4 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p2_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry04 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p2_tidak 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry5 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p3_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry05 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p3_ya 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry6 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p3_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry06 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p3_tidak

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry7 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_internet from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Internet' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry07 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Internet' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_internet

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";


				// $qry8 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_sosmed from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Sosial Media' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry08 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Sosial Media' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_sosmed

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry9 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_radio from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Radio' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry09 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Radio' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_radio

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry10 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_videotron from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Videotron' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry010 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Videotron' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_videotron

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry11 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_billboard from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Billboard' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry011 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Billboard' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_billboard

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry12 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_mediacetak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Media Cetak' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry012 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Media Cetak' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_mediacetak

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry13 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_kerabat from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Kerabat' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry013 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Kerabat' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_kerabat

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry14 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_petugaslayanan from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Petugas Layanan' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry014 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Petugas Layanan' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_petugaslayanan

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry15 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_lainnya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Lainnya' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry015 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Lainnya' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_lainnya

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry16 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p5_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry016 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p5_ya 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry17 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p5_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry017 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p5_tidak 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry18 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p6_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry018 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p6_ya 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry19 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p6_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry019 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p6_tidak 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry20 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p7_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry020 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p7_ya 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry21 = "
				// 		(select count(v.id_jawab_kuesioner) as jml__total_p7_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

				$qry021 = "
						select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p7_tidak 

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

				// $qry22 = "(select count(z.id_detail) as jml_total_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=1 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)";

				$qry022 = "
						select distinct (select (count(z.id_detail) * 100) from tr_detail_survey z,tr_survey y where z.kat_izin=1 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)  

						/ (select count(z.id_detail) from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi) as jml_total_berizin 

						from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi";

				// $qry23 = "(select count(z.id_detail) as jml_total_tidak_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=2 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)";

				$qry023 = "
						select distinct (select (count(z.id_detail) * 100) from tr_detail_survey z,tr_survey y where z.kat_izin=2 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)  

						/ (select count(z.id_detail) from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi) as jml_total_tidak_berizin 

						from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi";

				$qryTr = "select 
					w.*,
					(select count(v.id_jawab_kuesioner) as jml_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Ya' and z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
					(select count(v.id_jawab_kuesioner) as jml_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Tidak' and y.status_transaksi !='3' and z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)
					from m_kuesioner w 
					where id_kuesioner !='4' ";
				$data['GrafikTransaksiKuesioner'] = $this->db->query($qryTr)->result_array();

		    	$data['satu_ya'] 	= $this->db->query($qry01)->result_array();
		    	$data['satu_tidak'] = $this->db->query($qry02)->result_array();
		    	$data['dua_ya'] 	= $this->db->query($qry03)->result_array();
		    	$data['dua_tidak'] 	= $this->db->query($qry04)->result_array();
		    	$data['tiga_ya'] 	= $this->db->query($qry05)->result_array();
		    	$data['tiga_tidak'] = $this->db->query($qry06)->result_array();

		    	$data['empat_internet'] 		= $this->db->query($qry07)->result_array();
		    	$data['empat_sosmed'] 			= $this->db->query($qry08)->result_array();
		    	$data['empat_radio'] 			= $this->db->query($qry09)->result_array();
		    	$data['empat_videotron'] 		= $this->db->query($qry010)->result_array();
		    	$data['empat_billboard'] 		= $this->db->query($qry011)->result_array();
		    	$data['empat_mediacetak'] 		= $this->db->query($qry012)->result_array();
		    	$data['empat_kerabat']	 		= $this->db->query($qry013)->result_array();
		    	$data['empat_petugaslayanan'] 	= $this->db->query($qry014)->result_array();
		    	$data['empat_lainnya'] 			= $this->db->query($qry015)->result_array();

		    	$data['lima_ya'] 		= $this->db->query($qry016)->result_array();
		    	$data['lima_tidak'] 	= $this->db->query($qry017)->result_array();
		    	$data['enam_ya'] 		= $this->db->query($qry018)->result_array();
		    	$data['enam_tidak'] 	= $this->db->query($qry019)->result_array();
		    	$data['tujuh_ya'] 		= $this->db->query($qry020)->result_array();
		    	$data['tujuh_tidak'] 	= $this->db->query($qry021)->result_array();  
		    	$data['wb_ya'] 			= $this->db->query($qry022)->result_array();  
		    	$data['wb_tidak']		= $this->db->query($qry023)->result_array(); 

		// $qryTr = "select 
		// 			w.*,
		// 			(select count(v.id_jawab_kuesioner) as jml_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Ya' and z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
		// 			(select count(v.id_jawab_kuesioner) as jml_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Tidak' and y.status_transaksi !='3' and z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)
		// 			from m_kuesioner w where w.id_kuesioner !='4' ";

		$qryTr = "select 
					w.*,
					(select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Ya' and z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml_ya

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

					(select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Tidak' and z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml_tidak

						from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='".$kode_kecamatan."' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)
						from m_kuesioner w where w.id_kuesioner !='4' ";

		$combo_kec = "SELECT
    			  Y.*
    			  FROM ref_kecamatan Y";
	
		$data['ComboKec'] = $this->db->query($combo_kec)->result_array(); 
		
		$data['GrafikTransaksiKuesionerKecamatan'] = $this->db->query($qryTr)->result_array();
		$this->template->load('rorompok','v_transaksi_kuesioner_viewKecamatan',$data);
	
	}

	function lihatTransaksiKuesioner(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 29-Nov-2016
		Desc     : Menampilkan List Data Transaksi Jawaban Kuesioner dari surveyor	
		*/

		if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);			
		}
			$per_page = 20;  
			$qry = "SELECT 
			A.*,
			B.id_survey,
			C.kode_transaksi,
			D.id_kuesioner
			FROM
			tr_jawab_kuesioner A
			LEFT JOIN tr_survey B ON A.id_survey=B.id_survey
			LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
			LEFT JOIN m_kuesioner D ON A.id_kuesioner=D.id_kuesioner";
			
			
			$qry.= "
			"; 
			//echo $qry;

			$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
			$config['total_rows'] = $this->db->query($qry)->num_rows();
			$config['per_page']= $per_page;
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['uri_segment'] = 3;
			$config['base_url']= base_url().'/kuesioner/TransaksiJawabanKuesioner'; 
			$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
			$this->pagination->initialize($config);
			$this->data['paginglinks'] = $this->pagination->create_links();    
			$this->data['per_page'] = $this->uri->segment(3);      
			$this->data['offset'] = $offset ;
			if($this->data['paginglinks']!= '') {
			  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		
		$qry .= " limit {$per_page} offset {$offset} ";

		$sql_kueskec = "select 
						a.*,

						(select count(v.id_jawab_kuesioner) as jml_p1_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p1_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

						(select count(v.id_jawab_kuesioner) as jml_p2_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p2_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner), 

						(select count(v.id_jawab_kuesioner) as jml_p3_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p3_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

						(select count(v.id_jawab_kuesioner) as jml_p4_internet from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Internet' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_sosmed from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Sosial Media' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_radio from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Radio' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_videotron from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Videotron' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_billboard from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Billboard' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_medcetak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Media Cetak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_kerabat from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Kerabat' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_petlay from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Petugas Layanan' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p4_lain from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Lainnya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

						(select count(v.id_jawab_kuesioner) as jml_p5_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p5_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

						(select count(v.id_jawab_kuesioner) as jml_p6_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p6_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

						
						(select count(v.id_jawab_kuesioner) as jml_p7_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(v.id_jawab_kuesioner) as jml_p7_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

						(select sum(v.id_jawab_kuesioner) as sum_p1_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select sum(v.id_jawab_kuesioner) as sum_p1_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

						(select sum(v.id_jawab_kuesioner) as sum_p2_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select sum(v.id_jawab_kuesioner) as sum_p2_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=A.kode_kecamatan and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),
						(select count(z.id_detail) as jml_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=1 and z.kode_kecamatan=A.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi),
						(select count(z.id_detail) as jml_tidak_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=2 and z.kode_kecamatan=A.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi),
						
						(select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

						/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where y.status_transaksi !='3' and z.kode_kecamatan=a.kode_kecamatan and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as total_tidak

							from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)
							from ref_kecamatan a ";

		$combo_kec = "SELECT
					  Y.*
					  FROM ref_kecamatan Y";

		$data['ComboKec'] = $this->db->query($combo_kec)->result_array(); 

		$data['Hero'] = $this->db->query($sql_kueskec)->result_array();

		// $qry1 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p1_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry01 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='1' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p1_ya 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry2 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p1_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry02 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p1_tidak 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='1' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry3 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p2_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry03 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p2_ya 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry4 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p2_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry04 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p2_tidak 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='2' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry5 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p3_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry05 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p3_ya 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry6 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p3_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry06 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p3_tidak

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='3' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry7 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_internet from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Internet' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry07 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Internet' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_internet

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";


		// $qry8 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_sosmed from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Sosial Media' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry08 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Sosial Media' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_sosmed

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry9 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_radio from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Radio' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry09 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Radio' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_radio

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry10 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_videotron from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Videotron' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry010 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Videotron' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_videotron

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry11 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_billboard from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Billboard' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry011 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Billboard' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_billboard

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry12 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_mediacetak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Media Cetak' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry012 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Media Cetak' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_mediacetak

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry13 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_kerabat from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Kerabat' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry013 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Kerabat' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_kerabat

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry14 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_petugaslayanan from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Petugas Layanan' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry014 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Petugas Layanan' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_petugaslayanan

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry15 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p4_lainnya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Lainnya' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry015 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Lainnya' and v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p4_lainnya

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='4' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry16 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p5_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry016 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p5_ya 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry17 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p5_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry017 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p5_tidak 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='5' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry18 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p6_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry018 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p6_ya 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry19 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p6_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry019 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p6_tidak 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='6' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry20 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p7_ya from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry020 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Ya' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p7_ya 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry21 = "
		// 		(select count(v.id_jawab_kuesioner) as jml__total_p7_tidak from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)";

		$qry021 = "
				select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.jawaban='Tidak' and v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where  v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml__total_p7_tidak 

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, m_kuesioner w where v.id_kuesioner='7' and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner";

		// $qry22 = "(select count(z.id_detail) as jml_total_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=1 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)";

		$qry022 = "
				select distinct (select (count(z.id_detail) * 100) from tr_detail_survey z,tr_survey y where z.kat_izin=1 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)  

				/ (select count(z.id_detail) from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi) as jml_total_berizin 

				from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi";

		// $qry23 = "(select count(z.id_detail) as jml_total_tidak_berizin from tr_detail_survey z,tr_survey y where z.kat_izin=2 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)";

		$qry023 = "
				select distinct (select (count(z.id_detail) * 100) from tr_detail_survey z,tr_survey y where z.kat_izin=2 and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi)  

				/ (select count(z.id_detail) from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi) as jml_total_tidak_berizin 

				from tr_detail_survey z,tr_survey y where y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi";

		$qryTr = "select 
			w.*,
			(select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Ya' and z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml_ya

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner),

			(select distinct (select (count(v.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where v.jawaban='Tidak' and z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)  

				/ (select  count(v.id_jawab_kuesioner) from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner) as jml_tidak

				from tr_jawab_kuesioner v, m_pilihan_jawaban x, tr_detail_survey z,tr_survey y, ref_kecamatan a where z.kode_kecamatan='305' and z.kode_kecamatan=a.kode_kecamatan and y.status_transaksi !='3' and z.kode_transaksi=y.kode_transaksi and y.kode_transaksi=v.kode_transaksi and x.id_pil_jawab=v.id_pil_jawab and v.id_kuesioner=w.id_kuesioner)
				from m_kuesioner w where w.id_kuesioner !='4' ";
						
		$data['GrafikTransaksiKuesioner'] = $this->db->query($qryTr)->result_array();

    	$data['satu_ya'] 	= $this->db->query($qry01)->result_array();
    	$data['satu_tidak'] = $this->db->query($qry02)->result_array();
    	$data['dua_ya'] 	= $this->db->query($qry03)->result_array();
    	$data['dua_tidak'] 	= $this->db->query($qry04)->result_array();
    	$data['tiga_ya'] 	= $this->db->query($qry05)->result_array();
    	$data['tiga_tidak'] = $this->db->query($qry06)->result_array();

    	$data['empat_internet'] 		= $this->db->query($qry07)->result_array();
    	$data['empat_sosmed'] 			= $this->db->query($qry08)->result_array();
    	$data['empat_radio'] 			= $this->db->query($qry09)->result_array();
    	$data['empat_videotron'] 		= $this->db->query($qry010)->result_array();
    	$data['empat_billboard'] 		= $this->db->query($qry011)->result_array();
    	$data['empat_mediacetak'] 		= $this->db->query($qry012)->result_array();
    	$data['empat_kerabat']	 		= $this->db->query($qry013)->result_array();
    	$data['empat_petugaslayanan'] 	= $this->db->query($qry014)->result_array();
    	$data['empat_lainnya'] 			= $this->db->query($qry015)->result_array();

    	$data['lima_ya'] 		= $this->db->query($qry016)->result_array();
    	$data['lima_tidak'] 	= $this->db->query($qry017)->result_array();
    	$data['enam_ya'] 		= $this->db->query($qry018)->result_array();
    	$data['enam_tidak'] 	= $this->db->query($qry019)->result_array();
    	$data['tujuh_ya'] 		= $this->db->query($qry020)->result_array();
    	$data['tujuh_tidak'] 	= $this->db->query($qry021)->result_array();  
    	$data['wb_ya'] 			= $this->db->query($qry022)->result_array();  
    	$data['wb_tidak']		= $this->db->query($qry023)->result_array();     

		$this->data['sub_judul_form']="Data Transaksi Jawaban Kuesioner";	
		$this->template->load('rorompok','v_transaksi_kuesioner',$data);

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
