<?php
class Ijinmasuk extends CI_Controller{

	public function __construct() 
	{
        parent::__construct();
    }
    
	public function index() 
	{
	
	  /*
		Feature  : Generate Captha
		Author   : Nanda
		Modified :
		Date     : 17-Sep-2018
		Desc     : Generate Session Captcha	
		*/
	
	$original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
    $original_string = implode("", $original_string);
    $captcha = substr(str_shuffle($original_string), 0, 4);
	
	$vals = array(
				'word' => strtoupper($captcha),
				'img_path'	 => './captcha/',
                'img_url'	 => base_url().'captcha/',
				'font_path' => base_url() . 'system/fonts/arial.ttf',
                'img_width'	 => '120',
                'img_height' => 30,
                'border' => 1, 
                'expiration' => 7200
    );
 
    $cap = create_captcha($vals);
	$data['image'] = $cap['image'];
    $data['word'] = $cap['word'];
 
    $this->session->set_userdata('mycaptcha', $cap['word']);
	
	$this->load->view('login',$data);
        
    }
	
	public function masuk() 
	{
	    /*
		Feature  : Login - Masuk
		Author   : Joe
		Modified :
		Date     : 24-Okt-2016
		Desc     : Pengecekan Captcha, Pengecekan User & Pass Login Ke Database, Pencatatan Log Setiap Aksi.		
		*/
	
		if ( strtoupper($this->input->post('userCaptcha')) ==  strtoupper($this->session->userdata('mycaptcha'))) {
		
			$nama_pengguna = $this->input->post('username');
			$kunci_masuk = $this->input->post('password');
		
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('user_password', 'Password', 'required');
			$this->form_validation->set_rules('userCaptcha', 'Captcha', 'required|callback_check_captcha');
			$userCaptcha = $this->input->post('userCaptcha');
			
			if ($this->form_validation->run() == FALSE){
				
				$original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
				$original_string = implode("", $original_string);
				$captcha = substr(str_shuffle($original_string), 0, 4);
				
				$vals = array(
							'word' => strtoupper($captcha),
							'img_path'	 => './captcha/',
							'img_url'	 => base_url().'captcha/',
							'font_path' => base_url() . 'system/fonts/arial.ttf',
							'img_width'	 => '120',
							'img_height' => 30,
							'border' => 1, 
							'expiration' => 7200
				);
				
				$cap = create_captcha($vals);
				$data['image'] = $cap['image'];
				$data['word'] = $cap['word'];
				
				$this->session->set_userdata('mycaptcha', $cap['word']);
				
				$this->load->view('login',$data);
                
			} else {
                        
				$username = $this->input->post('username');
                $user_password = $this->input->post('user_password');

                $sql = "SELECT * FROM ref_users WHERE username=? AND password=?";
                $query = $this->db->query($sql, array($username, $user_password));
                $ceking = $query->num_rows();
				
					if ($ceking==0) {
					
						$data['pesan'] = "Username / Password Salah !!!";
						$original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
						$original_string = implode("", $original_string);
						$captcha = substr(str_shuffle($original_string), 0, 4);
						
						$vals = array(
									'word' => strtoupper($captcha),
									'img_path'	 => './captcha/',
									'img_url'	 => base_url().'captcha/',
									'font_path' => base_url() . 'system/fonts/arial.ttf',
									'img_width'	 => '120',
									'img_height' => 30,
									'border' => 1, 
									'expiration' => 7200
						);
						
						$cap = create_captcha($vals);
						$data['image'] = $cap['image'];
						$data['word'] = $cap['word'];
						
						$this->session->set_userdata('mycaptcha', $cap['word']);
						
						$this->load->view('login',$data);
					
					} else {
					
						$username = $this->input->post('username');

						// create token
						$token1 = openssl_random_pseudo_bytes(16);
						$token = bin2hex($token1);
						$data1 = array('token' => $token);
						$this->db->where('username', $username)->update('ref_users', $data1);


						$data_log = array(
							'username' => $username,
							'modules' => "Login",
							'action' => "Login Success",
							'date' => date("Y-m-d H:i:s"),
							'ip' => $this->input->ip_address()
						);

						$this->db->insert('ref_logs', $data_log);

						$query = $this->db->select('ref_users.*, peg_identpeg.*, ref_unkerja.nunker,ref_unkerja.kunker,ref_unkerja.keselon')
							->where("ref_users.username", $username)
							->limit(1)
							->join("peg_identpeg", "ref_users.nip=peg_identpeg.nip","left")
							->join("peg_tkerja", "peg_identpeg.nip=peg_tkerja.nip","left")
							->join("ref_unkerja", "peg_tkerja.kunker=ref_unkerja.kunker","left")
							->get("ref_users")->result();
						
						$data2 = array(
							'sesi_id_users' => $query[0]->id_users,
							'sesi_username' => $query[0]->username,
							'sesi_user_group' => $query[0]->id_user_group,
							'sesi_nip' => $query[0]->nip,
							'sesi_email' => $query[0]->email,
							'sesi_nama_lengkap' => empty($query[0]->nama)?$query[0]->nama_lengkap:$query[0]->nama,
							'sesi_prov' => $query[0]->id_prov,
							'sesi_kokab' => $query[0]->id_kabkot,
							'sesi_token' => $query[0]->token,
							'sesi_neselon' => $query[0]->keselon,
							'sesi_nunker' => empty($query[0]->nunker)?"Semua Unit Kerja": $query[0]->nunker,
							'sesi_kunker' => empty($query[0]->kunker) ? "0001": $query[0]->kunker,
							'sesi_ijinmasuk' => TRUE
						);
						
						$this->session->set_userdata($data2);
						
						redirect('welcome');
					}
                }
		
		} else {
		
			$data['pesan'] = "Kode Keamanan Yang Anda Masukan Salah !!!";
			$original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
			$original_string = implode("", $original_string);
			$captcha = substr(str_shuffle($original_string), 0, 4);
			
			$vals = array(
						'word' => strtoupper($captcha),
						'img_path'	 => './captcha/',
						'img_url'	 => base_url().'captcha/',
						'font_path' => base_url() . 'system/fonts/arial.ttf',
						'img_width'	 => '120',
						'img_height' => 30,
						'border' => 1, 
						'expiration' => 7200
			);
		 
			$cap = create_captcha($vals);
			$data['image'] = $cap['image'];
			$data['word'] = $cap['word'];
		 
			$this->session->set_userdata('mycaptcha', $cap['word']);
			
			$this->load->view('login',$data);
		}
    }
	
	
	public function keluar()
	{
	    /*
		Feature  : Keluar
		Author   : Nanda
		Modified :
		Date     : 17-Sep-2018
		Desc     : Memutuskan session login dan mencatat log.	
		*/
	   
		$username = $this->session->userdata('sesi_username');

		// create token
		$token1 = openssl_random_pseudo_bytes(16);
		$token = bin2hex($token1);
		$data1 = array('token' => $token);
		$this->db->where('username', $username)->update('ref_users', $data1);


		$data_log = array(
			'username' => $username,
			'modules' => "Logout",
			'action' => "Logout Success",
			'date' => date("Y-m-d H:i:s"),
			'ip' => $this->input->ip_address()
		);

		$this->db->insert('ref_logs', $data_log);

        $this->session->sess_destroy();

        redirect('ijinmasuk');
	}
	
}