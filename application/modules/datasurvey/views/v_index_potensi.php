		<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Daftar Potensi</h1>
					</div>
					<div class="pull-right">
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
                <div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>datasurvey/potensi">Daftar Potensi</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">
								<div align="right">
	                            <a class="btn btn-green" href="<?php echo site_url();?>datasurvey/exportexcelPotensiAll">Export to Excel</a>
	                            <a class="btn btn-primary" href="#" style="background-color: orange;">Export to PDF</a>
	                            </div>

							<?php 
								if (isset($field['id_detail'])) { $dis="disabled";} else { $dis="";}				  
								$katakunci= isset($field['katakunci'])?$field['katakunci']:$this->input->post('katakunci');	
								$id_kategori= isset($field['id_kategori'])?$field['id_kategori']:$this->input->post('id_kategori');				
							?>

								<div class="control-group">
										<label class="control-label" for="textfield">Pencarian</label>
								<div class="controls">

							<form action="<?php echo site_url('datasurvey/lihat_klasifikasiKategoriPotensi'); ?>" method="post" name="form2" class="form-horizontal form-bordered">

									<select name="id_kategori" id="id_kategori" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

											<option value="general">Tampil Kategori</option>

				                             
											<?php foreach($ComboKat as $row2) { ?>
				                             
				                             <option value="<?php echo $row2["id_kategori"]; ?>" <?php if ($id_kategori== $row2['id_kategori']) { echo "selected";} ?>><?php echo $row2['nama_kat']; ?></option>
				                            <?php } ?>

				                            </select>
				                            <button class="btn btn-primary" type="submit">Cari</button>
				                            </div>
										</div>
												
		<table width="100%" class="table table-hover">
	    <thead>
				<tr>
				  <th>TGL.SURVEY</th>
											<th>KODE TRANSAKSI SURVEY</th>
											<th>NAMA PEMILIK</th>
											<th>ALAMAT</th>
											<th>RT</th>
											<th>RW</th>
											<th>KATEGORI</th>
											<th>PERUNTUKKAN</th>
											<th>LUAS BANGUNAN</th>
											<th>KATEGORI</th>
											<th>BRUTO</th>
											<th>NETTO</th>
											<th>Aksi</th>
										    <th>&nbsp;</th>
			    </tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				if (count($potensi) > 0) {
					foreach($potensi as $row)
					{
					
						$data_detil[$i]['brutox'] = number_format($row['bruto'],2,",",".");
						$data_detil[$i]['netox'] = number_format($row['neto'],2,",",".");
						?>

						<tr>
						  <td><?php echo $row['tgl_survey']; ?></td>
											<td><?php echo $row['kode_transaksi']; ?></td>
											<td><?php echo $row['nama_pemilik']; ?></td>
										    <td><?php echo $row['alamat_persil']; ?></td>
										    <td><?php echo $row['rt']; ?></td>
											<td><?php echo $row['rw']; ?></td>
										    <td><?php echo $row['nama_kat']; ?></td>
											<td><?php echo $row['nama_peruntukkan']; ?></td>
											<td><?php echo $row['luas_bangunan']; ?></td>
											<td><?php echo $row['nama_param']; ?></td>
											<td><?php echo 'Rp '.$data_detil[$i]['brutox']; ?></td>
											<td><?php echo 'Rp '.$data_detil[$i]['netox']; ?></td>
								          <td>
											<a class="btn btn-mini btn-primary " href="<?php echo site_url();?>datasurvey/lihat/<?php echo $row['kode_transaksi']; ?>"><i class="icon-eye-open"></i> Lihat Detail</a>											</td>
									      <td>
										  <a class="btn btn-mini btn-danger" href="<?php echo site_url('datasurvey/hapus/'.$row['kode_transaksi']);?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>										  </td>
		      </tr>

				<?php
				
				$paging=(!empty($pagermessage) ? $pagermessage : '');
						
					}
					echo "<tr><td colspan='20'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
				} else {
					echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
				}
				$i++;
				?>
			</tbody>
		</table>									

		</form>	

							</div>
						</div>
					</div>
				</div>
				

