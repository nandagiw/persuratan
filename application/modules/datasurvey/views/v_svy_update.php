			<link rel="stylesheet" href="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="<?php echo base_url(); ?>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="<?php echo base_url(); ?>https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

			<div class="container-fluid" style="margin-top: 45px;">

							<div class="page-header">
					<div class="pull-left">
						<h1>Ubah Data Hasil Survey</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<br>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>datasurvey/indexDataSurveyor">Data Detail Hasil Survey</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>

			</div>

<div class="row-fluid">


<div class="span12">

<div class="box">
<div class="box-title">
								
								
							</div>
                            
 <div class="box-content">
 
								<!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
								<?php echo form_open('datasurvey/simpan_updateDataSurveyor',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-wysiwyg','enctype'=>'multipart/form-data'));?>
									
								<?php 
                                        if ($this->session->flashdata('message_gagal')) {
                                        echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                        }

                                        if ($this->session->flashdata('message_sukses')) {
                                        echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                        }

                                   ?>

									<input type="hidden" name="id_kuesioner" id="id_kuesioner" class="input-xxlarge"  value="<?php echo isset($field['id_kuesioner'])?$field['id_kuesioner']:'';?>">

									<?php 
									if (isset($field['id_kuesioner'])) { $dis="disabled";} else { $dis="";}							  
									$jawaban= isset($field['nama_pil_jawab'])?$field['nama_pil_jawab']:$this->input->post('jawaban'); 
									$status_ketemu_pemilik= isset($field['status_ketemu_pemilik'])?$field['status_ketemu_pemilik']:$this->input->post('status_ketemu_pemilik');
									$kode_kecamatan= isset($field['kode_kecamatan'])?$field['kode_kecamatan']:$this->input->post('kode_kecamatan'); 
									$id_kategori= isset($field['id_kategori'])?$field['id_kategori']:$this->input->post('id_kategori'); 
									$id_jenis_izin= isset($field['id_jenis_izin'])?$field['id_jenis_izin']:$this->input->post('id_jenis_izin'); 
									$id_peruntukkan= isset($field['id_peruntukkan'])?$field['id_peruntukkan']:$this->input->post('id_peruntukkan'); 
									$id_status_piutang= isset($field['id_status_piutang'])?$field['id_status_piutang']:$this->input->post('id_status_piutang'); 
									$id_pil_jawab= isset($field['id_pil_jawab'])?$field['id_pil_jawab']:$this->input->post('id_pil_jawab');
									$id= isset($combokel['id'])?$field['kode_kecamatan']:$this->input->post('kode_kecamatan');
									$kode_transaksi = isset($field['kode_transaksi'])?$field['kode_transaksi']:$this->input->post('kode_transaksi');
									$id_survey = isset($field['id_survey'])?$field['id_survey']:$this->input->post('id_survey');
											
							  		?>

							  		<h4><i class=" icon-plus-sign"></i>Data Survey</h4>
									<br>

                                    <div class="control-group">
											<label for="textfield" class="control-label">Kode Survey</label>
											<div class="controls">
												
												<input type="text" name="kode_transaksi" id="kode_transaksi" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['kode_transaksi'])?$field['kode_transaksi']:'';?>" readonly="readonly">
												
											</div>
										</div>
										
								 <div class="control-group">
											<label for="textfield" class="control-label">Tanggal Survey</label>
											<div class="controls">

												<input type="text" name="tgl_survey" id="tgl_survey" value="<?php echo isset($field['tgl_survey'])?$field['tgl_survey']:'';?>">
												<span class="help-block">format : yyyy-mm-dd</span>
											</div>
										</div>

<!-- 								<div class="control-group">
											<label for="textfield" class="control-label">Petugas Survey</label>
											<div class="controls">
												
												<input type="text" name="nama_lengkap" id="nama_lengkap" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nama_lengkap'])?$field['nama_lengkap']:'';?>">
											</div>
										</div> -->

								<div class="control-group">
											<label for="textfield" class="control-label">Nomor Surat Perintah</label>
											<div class="controls">
												
												<input type="text" name="no_surat_perintah" id="no_surat_perintah" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['no_surat_perintah'])?$field['no_surat_perintah']:'';?>">
											</div>
										</div>
										<br>
										
									<!-- =================================================================== -->
									<h4><i class=" icon-plus-sign"></i>Data Pemilik</h4>
									<br>

									<div class="control-group">
											<label for="textfield" class="control-label">Nama Pemilik</label>
											<div class="controls">
												
												<input type="text" name="nama_pemilik" id="nama_pemilik" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nama_pemilik'])?$field['nama_pemilik']:'';?>">
											</div>
										</div>

									<div class="control-group">
											<label for="textfield" class="control-label">Alamat Pemilik</label>
											<div class="controls">
												
												<input type="text" name="alamat_persil" id="alamat_persil" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['alamat_persil'])?$field['alamat_persil']:'';?>">
											</div>
										</div>
								           
								           								<div class="control-group">
											<label for="textfield" class="control-label">Nama Perusahaan</label>
											<div class="controls">
												
												<input type="text" name="nama_perusahaan" id="nama_perusahaan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nama_perusahaan'])?$field['nama_perusahaan']:'';?>">
											</div>
										</div>  

									<div class="control-group">
											<label for="textfield" class="control-label">Alamat Persil</label>
											<div class="controls">
												
												<input type="text" name="alamat_persil" id="alamat_persil" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['alamat_persil'])?$field['alamat_persil']:'';?>">
											</div>
										</div>  

									<div class="control-group">
											<label for="textfield" class="control-label">Kecamatan</label>
											<div class="controls">

											<select name="kode_kecamatan" id="kode_kecamatan" class="input-xlarge" >

				                              <option value="<?php echo isset($field['kode_kecamatan'])?$field['kode_kecamatan']:'';?>">-- <?php echo $field['nama_kec']; ?> --</option>

				                              <?php foreach($ComboKec as $row1) { ?>
				                             
				                             <option value="<?php echo $row1["kode_kecamatan"]; ?>"><?php echo $row1['nama_kec']; ?></option>
				                            <?php } ?>
				                            
				                            </select>
				                            </div>
				                        </div>

<!-- <script type="text/javascript">
$(document).ready(function(){
	$("#kec_id").change(function(){
		var kode_kecamatan = $("#kec_id").val();
		$.ajax({
			type : "POST",
			url  : "<?php echo site_url(); ?>datasurvey/ambildataKelurahan/",
			data : "kode_kecamatan=" + kode_kecamatan,
			success : function(data){
				$("#kode_kelurahan").html(data);
			}
		});
	});
});

</script> -->

									<div class="control-group">
											<label for="textfield" class="control-label">Kelurahan</label>
											<div class="controls">

											<select name="kode_kelurahan" id="kode_kelurahan" class="input-xlarge" data-rule-required="true">

											<option value="<?php echo isset($field['kode_kelurahan'])?$field['kode_kelurahan']:'';?>">-- <?php echo $field['nama_kel']; ?> --</option>

				                              <?php foreach($ComboKel as $row1) { ?>
				                             
				                             <option value="<?php echo $row1["kode_kelurahan"]; ?>"><?php echo $row1['nama_kel']; ?></option>
				                            <?php } ?>

				                            </select>
				                            </div>
				                        </div>

				                   	<div class="control-group">
											<label for="textfield" class="control-label">Kategori</label>
											<div class="controls">

											<select name="id_kategori" id="id_kategori" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >
											<option value="<?php echo isset($field['id_kategori'])?$field['id_kategori']:'';?>">-- <?php echo $field['nama_kat']; ?> --</option>

											<?php foreach($ComboKat as $row1) { ?>
				                             
				                             <option value="<?php echo $row1["id_kategori"]; ?>"><?php echo $row1['nama_kat']; ?></option>
				                            <?php } ?>

				                            

				                            </select>
				                            </div>
				                        </div>

				                   	<div class="control-group">
											<label for="textfield" class="control-label">Jenis Izin</label>
											<div class="controls">

											<select name="id_jenis_izin" id="id_jenis_izin" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

											 <option value="<?php echo isset($field['id_jenis_izin'])?$field['id_jenis_izin']:'';?>">-- <?php echo $field['nama_jenis']; ?> --</option>

											<?php foreach($ComboJns as $row1) { ?>
				                             
				                             <option value="<?php echo $row1["id_jenis_izin"]; ?>"><?php echo $row1['nama_jenis']; ?></option>
				                            <?php } ?>

				                            </select>
				                            </div>
				                        </div>

									<div class="control-group">
											<label for="textfield" class="control-label">Nomor Izin</label>
											<div class="controls">
												
												<input type="text" name="nomor_izin" id="nomor_izin" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nomor_izin'])?$field['nomor_izin']:'';?>">
											</div>
										</div>

									<div class="control-group">
											<label for="textfield" class="control-label">Tanggal Izin</label>
											<div class="controls">

												<input type="text" name="tanggal_izin" id="tanggal_izin" value="<?php echo isset($field['tanggal_izin'])?$field['tanggal_izin']:'';?>">
												<span class="help-block">format : yyyy-mm-dd</span>

											</div>
										</div>

				                   	<div class="control-group">
											<label for="textfield" class="control-label">Peruntukkan</label>
											<div class="controls">

											<select name="id_peruntukkan" id="id_peruntukkan" class="input-xxlarge" data-rule-required="true" onchange="doShow(this.value);" >

											<option value="<?php echo isset($field['id_peruntukkan'])?$field['id_peruntukkan']:'';?>">-- <?php echo $field['nama_peruntukkan']; ?> --</option>

											<?php foreach($ComboPrtk as $row1) { ?>
				                             
				                             <option value="<?php echo $row1["id_peruntukkan"]; ?>"><?php echo $row1['nama_peruntukkan']; ?></option>
				                            <?php } ?>

				                            </select>
				                            </div>
				                        </div>

									<div class="control-group">
											<label for="textfield" class="control-label">Luas Persil</label>
											<div class="controls">
												
												<input type="text" name="luas_persil" id="luas_persil" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['luas_persil'])?$field['luas_persil']:'';?>">
											</div>
										</div>
								            
								    <div class="control-group">
											<label for="textfield" class="control-label">Nomor Persil</label>
											<div class="controls">
												
												<input type="text" name="no_persil" id="no_persil" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['no_persil'])?$field['no_persil']:'';?>">
											</div>
										</div>

									<div class="control-group">
											<label for="textfield" class="control-label">Jumlah Tingkat</label>
											<div class="controls">
												
												<input type="text" name="jumlah_tingkat" id="jumlah_tingkat" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['jumlah_tingkat'])?$field['jumlah_tingkat']:'';?>">
											</div>
										</div>

									<div class="control-group">
											<label for="textfield" class="control-label">RT</label>
											<div class="controls">
												
												<input type="text" name="rt" id="rt" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['rt'])?$field['rt']:'';?>">
											</div>
										</div>

									<div class="control-group">
											<label for="textfield" class="control-label">RW</label>
											<div class="controls">
												
												<input type="text" name="rw" id="rw" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['rw'])?$field['rw']:'';?>">
											</div>
										</div>

									<div class="control-group">
											<label for="textfield" class="control-label">Luas Bangunan</label>
											<div class="controls">
												
												<input type="text" name="luas_bangunan" id="luas_bangunan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['luas_bangunan'])?$field['luas_bangunan']:'';?>">
											</div>
										</div>

										<div class="control-group">
											<label for="textfield" class="control-label">Status Ketemu Pemilik</label>
												<div class="controls">

											<select name="status_ketemu_pemilik" id="status_ketemu_pemilik" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >
				                            
				                              <option value="1" <?php if ($status_ketemu_pemilik==1) { echo "selected";} ?>>Ketemu</option>
											  <option value="0" <?php if ($status_ketemu_pemilik==0) { echo "selected";} ?>>Tidak Ketemu</option>
         
				                            </select>

				                            </div>
										</div>
<!-- 
										<br> -->
										<!-- -================================================================= -->
										<!-- <h4><i class=" icon-plus-sign"></i>Data Piutang Retribusi</h4>

										<br>
										<div class="control-group">
											<label for="textfield" class="control-label">Nilai Piutang Retribusi</label>
											<div class="controls">
												
												<input type="text" name="nilai_piutang" id="nilai_piutang" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nilai_piutang'])?$field['nilai_piutang']:'';?>">
											</div>
										</div>

										<div class="control-group">
											<label for="textfield" class="control-label">Tanggal SKRD</label>
											<div class="controls">
												
												<input type="text" name="tgl_skrd" id="tgl_skrd" value="<?php echo isset($field['tgl_skrd'])?$field['tgl_skrd']:'';?>">
												<span class="help-block">format : yyyy-mm-dd</span>


											</div>
										</div>

										<div class="control-group">
											<label for="textfield" class="control-label">Nilai Piutang Denda</label>
											<div class="controls">
												
												<input type="text" name="nilai_piutang_denda" id="nilai_piutang_denda" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nilai_piutang_denda'])?$field['nilai_piutang_denda']:'';?>">
											</div>
										</div>

				                   	<div class="control-group">
											<label for="textfield" class="control-label">Status Piutang</label>
											<div class="controls">

											<select name="id_status_piutang" id="id_status_piutang" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

				                           <option value="<?php echo isset($field['id_status_piutang'])?$field['id_status_piutang']:'';?>">-- <?php echo $field['nama_status_piutang']; ?> --</option>

				                           <?php foreach($ComboSp as $row1) { ?>
				                             
				                             <option value="<?php echo $row1["id_status_piutang"]; ?>"><?php echo $row1['nama_status_piutang']; ?></option>
				                            <?php } ?>

				                            </select>
				                            </div>
				                        </div>

										<div class="control-group">
											<label for="textfield" class="control-label">Alasan Belum Bayar</label>
											<div class="controls">
												
												<input type="text" name="alasan_blm_bayar" id="alasan_blm_bayar" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['alasan_blm_bayar'])?$field['alasan_blm_bayar']:'';?>">
											</div>
										</div> -->

																				<br>
										<!-- -================================================================= -->
										<h4><i class=" icon-plus-sign"></i>Pertanyaan Kuesioner</h4>

										<!-- <div class="control-group">
											<p>1.	Apakah anda mengerti dan paham tentang perizinan</p>
											<div class="controls">
										
											<select name="jawaban[]" id="jawaban[]" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

				                           <option value="Ya">Ya</option>
				                           <option value="Tidak">Tidak</option>
				                           </select>
					  						
					  						</div>
										</div>

										<div class="control-group">
											<p>2.	Apakah anda membutuhkan perizinan untuk bangunan atau tempat usaha anda? </p>
											<div class="controls">
										
											<select name="jawaban[]" id="jawaban[]" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

				                           <option value="Ya">Ya</option>
				                           <option value="Tidak">Tidak</option>
				                           </select>
					  						
					  						</div>
										</div>

										<div class="control-group">
											<p>3.	Apakah anda pernah mendengar atau mengetahui pelayanan perijinan? </p>
											<div class="controls">
										
											<select name="jawaban[]" id="jawaban[]" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

				                           <option value="Ya">Ya</option>
				                           <option value="Tidak">Tidak</option>
				                           </select>
					  						
					  						</div>
										</div>

										<div class="control-group">
											<p>4.	Jika Ya, darimana sumber informasi pelayanan perijinan yang anda peroleh? Internet/Sosial </p>
											<div class="controls">
										
											
											<select name="jawaban[]" id="jawaban[]" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >
				                            
				                              <option value="Internet">Internet</option>
											  <option value="Sosial Media">Sosial Media</option>
											  <option value="Radio">Radio</option>
											  <option value="Video Tron">Video Tron</option>
											  <option value="Billboard">Billboard</option>
											  <option value="Sosial Media">Sosial Media</option>
											  <option value="Media Cetak">Media Cetak</option>
											  <option value="Kerabat">Kerabat</option>
											  <option value="Petugas Layanan">Petugas Layanan</option>
											  <option value="Lainnya" >Lainnya</option>
         
				                            </select>
					  						
					  						</div>
										</div>

										<div class="control-group">
											<p>5.	Apakah anda mengetahui tentang aplikasi perizinan on-line dan “GAMPIL”? </p>
											<div class="controls">
										
											<select name="jawaban[]" id="jawaban[]" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

				                           <option value="Ya">Ya</option>
				                           <option value="Tidak">Tidak</option>
				                           </select>
					  						
					  						</div>
										</div>

										<div class="control-group">
											<p>6.	Jika Ya, apakah anda pernah mencoba menggunakan aplikasi tersebut ? </p>
											<div class="controls">
										
											<select name="jawaban[]" id="jawaban[]" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

				                           <option value="Ya">Ya</option>
				                           <option value="Tidak">Tidak</option>
				                           </select>
					  						
					  						</div>
										</div>

										<div class="control-group">
											<p>7.	Jika Ya, apakah aplikasi tersebut memudahkan anda dalam pelayanan </p>
											<div class="controls">
										
											<select name="jawaban[]" id="jawaban[]" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

				                           <option value="Ya">Ya</option>
				                           <option value="Tidak">Tidak</option>
				                           </select>
					  						
					  						</div>
										</div> -->

						<div class="span8">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Pertanyaan
								</h3>
							</div>
							<div class="box-content" style="height: 350px; overflow-y: scroll;">

										<table width="1%" class="table table-hover">
	    <thead>
				<tr>
								<th></th>
								<th></th>
								<th></th>
	    </tr>
		</thead>
		<tbody cellpadding="20">
			<?php
			if (count($ListDataPertanyaan) > 0) {
				foreach($ListDataPertanyaan as $row)
					// foreach($ListDataJawaban as $row1)
				{
					
				?>

				<tr >
								<td><input name="id_kuesioner[]" id="id_kuesioner[]" value="<?php echo isset($row['id_kuesioner'])?$row['id_kuesioner']:'';?>" style="width:12px;" readonly="readonly"></td>
								<td><?php echo $row['pertanyaan']; ?></td>
								</tr>

								<?php
							
							$paging=(!empty($pagermessage) ? $pagermessage : '');
									
								}
								echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
							} else {
								echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
							}
							?>


							</tbody>
						</table>
						</div>
						</div>
						</div>

											<div class="span3" style=" width: 360px; ">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Jawaban
								</h3>
							</div>
							<div class="box-content" style="height: 350px; overflow-y: scroll;">

						<table width="100%" class="table table-hover">
	    <thead>
				<tr>
								<th></th>
	    </tr>
		</thead>
		<tbody  style="padding: 100px;">



				<tr  style="padding: 100px;">
					
								<td>
									1.
									<select name="jawaban[]" id="jawaban[]" class="input-large" data-rule-required="true" onchange="doShow(this.value);" >

									<option value="null" >-- pilih jawaban --</option>
									
										<?php foreach($ListDataJawaban1 as $row1) { ?>
				                           <option value="<?php echo $row1["nama_pil_jawab"]; ?>"><!-- <?php echo $row1["id_pil_jawab"] ?> --><?php echo $row1["nama_pil_jawab"];?></option>
				                           <?php } ?>
				                           </select>
								</td>
								
								</tr>
												<tr>
					
								<td>
									2.
									<select name="jawaban[]" id="jawaban[]" class="input-large" data-rule-required="true" onchange="doShow(this.value);" >

									<option value="null" >-- pilih jawaban --</option>

										<?php foreach($ListDataJawaban2 as $row2) { ?>
				                           <option value="<?php echo $row2["nama_pil_jawab"]; ?>"><!-- <?php echo $row2["id_pil_jawab"] ?> --><?php echo $row2["nama_pil_jawab"];?></option>
				                           <?php } ?>
				                           </select>
								</td>
								
								</tr>

				<tr>
					
								<td>
									3.
									<select name="jawaban[]" id="jawaban[]" class="input-large" data-rule-required="true" onchange="doShow(this.value);" >

									<option value="null" >-- pilih jawaban --</option>

										<?php foreach($ListDataJawaban3 as $row3) { ?>
				                           <option value="<?php echo $row3["nama_pil_jawab"]; ?>"><!-- <?php echo $row3["id_pil_jawab"] ?> --><?php echo $row3["nama_pil_jawab"];?></option>
				                           <?php } ?>
				                           </select>
								</td>
								
								</tr>

												<tr>
					
								<td>
									4.
									<select name="jawaban[]" id="jawaban[]" class="input-large" data-rule-required="true" onchange="doShow(this.value);" >

									<option value="null" >-- pilih jawaban --</option>

										<?php foreach($ListDataJawaban4 as $row4) { ?>
				                           <option value="<?php echo $row4["nama_pil_jawab"]; ?>"><!-- <?php echo $row4["id_pil_jawab"] ?> --><?php echo $row4["nama_pil_jawab"];?></option>
				                           <?php } ?>
				                           </select>
								</td>
								
								</tr>
									<tr>
					
								<td>
									5.
									<select name="jawaban[]" id="jawaban[]" class="input-large" data-rule-required="true" onchange="doShow(this.value);" >

									<option value="null" >-- pilih jawaban --</option>

										<?php foreach($ListDataJawaban5 as $row5) { ?>
				                           <option value="<?php echo $row5["nama_pil_jawab"]; ?>"><!-- <?php echo $row5["id_pil_jawab"] ?> --><?php echo $row5["nama_pil_jawab"];?></option>
				                           <?php } ?>
				                           </select>
								</td>
								
								</tr>

												<tr>
					
								<td>
									6.
									<select name="jawaban[]" id="jawaban[]" class="input-large" data-rule-required="true" onchange="doShow(this.value);" >

									<option value="null" >-- pilih jawaban --</option>

										<?php foreach($ListDataJawaban6 as $row6) { ?>
				                           <option value="<?php echo $row6["nama_pil_jawab"]; ?>"><!-- <?php echo $row6["id_pil_jawab"] ?> --><?php echo $row6["nama_pil_jawab"];?></option>
				                           <?php } ?>
				                           </select>
								</td>
								
								</tr>

						<tr>
					
								<td>
									7.
									<select name="jawaban[]" id="jawaban[]" class="input-large" data-rule-required="true" onchange="doShow(this.value);" >

									<option value="null">-- pilih jawaban --</option>

										<?php foreach($ListDataJawaban7 as $row7) { ?>
				                           <option value="<?php echo $row7["nama_pil_jawab"]; ?>"><!-- <?php echo $row7["id_pil_jawab"] ?> --><?php echo $row7["nama_pil_jawab"];?></option>
				                           <?php } ?>
				                           </select>
								</td>
								
								</tr>

					<!-- 	<tr>
					
								<td>
									8.
									<textarea name="isian" id="isian" class="input-large" data-rule-required="true" value="<?php echo isset($field['alasan_blm_bayar'])?$field['alasan_blm_bayar']:'';?>">
										
									</textarea>
									
								</td>
								
								</tr> -->
							
							</tbody>
						</table>
						</div>
						</div>
						</div>
										
										<br>
										<div>
										<!-- -================================================================= -->
										<h4><i class=" icon-plus-sign"></i>Data Foto</h4>
										<br>

										<div class="control-group">
											<label for="textfield" class="control-label">Cuplikan Foto</label>
											<div class="controls">
												
												<a href="#">
												<img src="<?php echo base_url();?>assets/uploads/<?php echo $field['directory']; ?>" alt="" width="200" height="100">
											</a>
											</div>
										</div>
										</div>

									<div class="form-actions">
										<button class="btn btn-primary" type="submit"  onclick="return confirm('Anda yakin sudah mengisi data dengan benar? mohon periksa kembali'); " formnovalidate="formnovalidate">Simpan</button>
                                        <a class="btn btn-danger" href="<?php echo site_url();?>datasurvey/indexDataSurveyor">Kembali</a>
										
									</div>
								</form>
							</div>       

							</div>

							</div>
						
							</div>

							<script type="text/javascript">
								$.validator.addMethod("valueNotEquals", function(value, element, arg){
							  return arg != value;
							 }, "Value must not equal arg.");

							 // configure your validation
							 $("form").validate({
							  rules: {
							   SelectName: { valueNotEquals: "default" }
							  },
							  messages: {
							   SelectName: { valueNotEquals: "Please select an item!" }
							  }  
							 });
							</script>

			<script type="text/javascript">

							/*
							$(document).ready(function(){
							  $("#lookup_table_div").hide();
							});
							*/



							<?php 
							if ($jawaban==3 or $jawaban==4) { ?>
							$("#lookup_table_div").show();
							<?php } else { ?>
							$("#lookup_table_div").hide();
							<?php } ?>

							function doShow(tipe_isian) {


								
								if (tipe_isian==3 || tipe_isian==4) {
								$("#lookup_table_div").show();
								//document.getElementById("lookup_table_div").style.visibility = "visible"; 
								
								} else {
								$("#lookup_table_div").hide();
								//document.getElementById("lookup_table_div").style.visibility = "hidden"; 
								
								}
								

							}

							</script>

							<script type="text/javascript">
								
								$(document).ready(function() {
								$('#bb').validate({
									rules: {
										tgl_survey: {
											indonesianDate:false
										},
										no_surat_perintah: {
											maxlength: 16
										},
										nama_pemilik: {
											maxlength: 50
										},
										alamat_persil: {
											maxlength: 100
										},
										kode_kecamatan:{
											kode_kecamatan: true
										},
										kode_kelurahan:{
											kode_kelurahan: true
										},
										id_kategori:{
											id_kategori: true
										},
										id_jenis_izin:{
											id_jenis_izin:true
										},
										nomor_izin:{
											nomor_izin: true,
											maxlength: 50
										},
										tanggal_izin:{
											indonesianDate: false
										},
										id_peruntukkan:{
											id_peruntukkan: true
										},
										luas_persil:{

										},
										no_persil:{
											maxlength: 5
										},
										jumlah_tingkat:{

										},
										rt:{
											maxlength: 10
										},
										rw:{
											maxlength: 10
										},
										luas_bangunan:{
											maxlength: 10
										},
										status_ketemu_pemilik:{
											status_ketemu_pemilik: true
										},
										nilai_piutang:{

										},
										tgl_skrd:{
											indonesianDate: false
										},
										nilai_piutang_denda:{

										},
										id_status_piutang:{
											id_status_piutang: true
										},
										alasan_blm_bayar:{
											maxlength: 255
										},
										jawaban:{
											jawaban: true
										}



									},
										messages: {
									tgl_survey: {
										required: "Kolom tanggal survey harus terisi"
									},
									no_surat_perintah: {
										required: "Alamat email harus terisi",
										maxlength: "jangan melebih 16 karakter"
									},
									nama_pemilik: {
										required: "nama pemilik harus terisi",
										maxlength: "jangan melebih 50 karakter"
									},
									alamat_persil:{
										required: "alamat pemilik harus diisi",
										maxlength: "jangan melebih 100 karakter"
									},
									nomor_izin:{
										required: "nomor izin harus terisi",
										maxlength: "jangan melebih 50 karakter"
									},luas_persil:{

									},
									no_persil:{
										required: "nomor persil harus terisi",
										maxlength: "maksimal 5 karakter"
									},
									jumlah_tingkat:{

									},
									rt:{
										required: "kolom RT harus terisi",
										maxlength: "jangan melebih 10 karakter"
									},
									rw:{
										required: "kolom RW harus terisi",
										maxlength: "jangan melebih 10 karakter"
									},
									luas_bangunan:{
										required: "kolom luas bangunan harus terisi",
										maxlength: "jangan melebihi 10 karakter"
									},
									nilai_piutang:{

									},
									tgl_skrd:{
										required: "kolom tanggal SKRD harus terisi"
									},
									nilai_piutang_denda:{
										required: "kolom nilai piutang denda harus terisi"
									},
									alasan_blm_bayar:{
										required: "kolom alasan harus bayar harus terisi"
									},
									jawaban:{
										required: "jawaban harus dipilih salah satu"
									}

								}
								});
							});
							 
							$.validator.addMethod(
								"indonesianDate",
								function(value, element) {
									// put your own logic here, this is just a (crappy) example
									return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
								},
								"Please enter a date in the format YYYY/MM/DD"
							);
							</script>


