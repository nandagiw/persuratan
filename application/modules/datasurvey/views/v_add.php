			<div class="container-fluid" style="margin-top: 45px;">
				<br>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Data Master</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo site_url();?>ref_user_backend">Hasil Survey</a>
							<i class="icon-angle-right"></i>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-title">
							<h3><i class=" icon-plus-sign"></i><?php echo $judul_form." ".$sub_judul_form;?> </h3>		
						</div>
                            
 						<div class="box-content">
							<!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
							<?php echo form_open('datasurvey/simpan',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
									
								<?php 
                                    if ($this->session->flashdata('message_gagal')) {
                                    	echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                    }

                                    if ($this->session->flashdata('message_sukses')) {
                                    	echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                    }

                                   ?>
									
									<input type="hidden" name="id" id="id" class="input-xxlarge"  value="<?php echo isset($field['id'])?$field['id']:'';?>">

								  <?php 
									if (isset($field['id'])) { $dis="disabled";} else { $dis="";}							  
									$tipe_pengguna= isset($field['tipe_pengguna'])?$field['tipe_pengguna']:$this->input->post('tipe_pengguna'); 
									$status_aktif= isset($field['status_aktif'])?$field['status_aktif']:$this->input->post('status_aktif'); 
												
								  ?>
    
                                    <div class="control-group">
										<label for="textfield" class="control-label">Username</label>
										<div class="controls">
											<input type="text" <?php echo $dis; ?> name="nama_pengguna" id="nama_pengguna" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nama_pengguna'])?$field['nama_pengguna']:'';?>">			
										</div>
									</div>
										
								    <div class="control-group">
										<label for="textfield" class="control-label">Password</label>
										<div class="controls">
											<input type="password" <?php echo $dis; ?>  name="kunci_masuk" id="kunci_masuk" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['kunci_masuk'])?$field['kunci_masuk']:'';?>">
										</div>
									</div>
										
								 	<div class="control-group">
										<label for="textfield" class="control-label">Nama</label>
										<div class="controls">
											<input type="text" name="nama_lengkap" id="nama_lengkap" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field['nama_lengkap'])?$field['nama_lengkap']:$this->input->post('nama_lengkap');?>">
										</div>
									</div>
                                    
									<div class="control-group">
										<label for="textfield" class="control-label">Telp.</label>
										<div class="controls">
											<input type="text" name="telp_pengguna" id="telp_pengguna" class="input-xxlarge" data-rule-required="true" data-rule-number="true" value="<?php echo isset($field['telp_pengguna'])?$field['telp_pengguna']:$this->input->post('telp_pengguna');?>">	
										</div>
									</div>
										
									<div class="control-group">
										<label for="textfield" class="control-label">Email</label>
										<div class="controls">
											<input type="text" name="email_pengguna" id="email_pengguna" class="input-xxlarge" data-rule-email="true" data-rule-required="true" value="<?php echo isset($field['email_pengguna'])?$field['email_pengguna']:$this->input->post('email_pengguna');?>">		
										</div>
									</div>
										
									<div class="control-group">
										<label for="textfield" class="control-label">Tipe Hasil Survey</label>
										<div class="controls">
											<select name="tipe_pengguna" id="tipe_pengguna" class="input-xxlarge" data-rule-required="true" onchange="doShow(this.value);" >
                             					<option value="">-Pilih-</option>
				                                <option value="1" <?php if ($tipe_pengguna==1) { echo "selected";} ?>>Administrator</option>
											    <option value="2" <?php if ($tipe_pengguna==2) { echo "selected";} ?>>Executive</option>
											    <option value="3" <?php if ($tipe_pengguna==3) { echo "selected";} ?>>Surveyor</option>
                           					</select>		
										</div>
									</div>
										
										
									<div class="control-group">
										<label for="textfield" class="control-label">Status Aktif</label>
											<div class="controls">
												<select name="status_aktif" id="status_aktif" class="input-xxlarge" data-rule-required="true" >
						                             <option value="">-Pilih-</option>
						                             <option value="Y" <?php if ($status_aktif=="Y") { echo "selected";} ?>>Aktif</option>
													 <option value="N" <?php if ($status_aktif=="N") { echo "selected";} ?>>Tidak Aktif</option>  
                            					</select>		
											</div>
										</div>
										
									<div class="form-actions">
										<button class="btn btn-primary" type="submit">Simpan</button>
                                        <a class="btn btn-danger"  href="<?php echo site_url();?>datasurvey/">Kembali</a>
										
									</div>
								</form>
							</div>       
						</div>
					</div>		
				</div>

				<script type="text/javascript">

					/*
					$(document).ready(function(){
					  $("#lookup_table_div").hide();
					});
					*/



					<?php 
					if ($tipe_pengguna==3 or $tipe_pengguna==4) { ?>
					$("#lookup_table_div").show();
					<?php } else { ?>
					$("#lookup_table_div").hide();
					<?php } ?>

					function doShow(tipe_isian) {


						
						if (tipe_isian==3 || tipe_isian==4) {
						$("#lookup_table_div").show();
						//document.getElementById("lookup_table_div").style.visibility = "visible"; 
						
						} else {
						$("#lookup_table_div").hide();
						//document.getElementById("lookup_table_div").style.visibility = "hidden"; 
						
						}
						
					}
					
				</script>


