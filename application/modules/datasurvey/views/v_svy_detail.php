<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Data Detail Hasil Survey</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
                <div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>datasurvey">Data Hasil Survey</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Data Detail Hasil Survey</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>

				<br>
				

					<div class="span4">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Data Hasil Survey
								</h3>
							</div>
							<div class="box-content">
								
							<?php echo form_open('datasurvey/index',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>



									<input type="hidden" name="kode_transaksi" id="kode_transaksi" class="input-xxlarge"  value="<?php echo isset($field_survey['kode_transaksi'])?$field_survey['kode_transaksi']:'';?>">

                                    <div class="control-group">
											<label for="textfield_survey" class="control-label">Kode Survey</label>
											<div class="controls">
												
												: <?php echo isset($field_survey['kode_transaksi'])?$field_survey['kode_transaksi']:'';?>
												
											</div>
										</div>
										
								  <div class="control-group">
											<label for="textfield_survey" class="control-label">Tgl.Survey</label>
											<div class="controls">
												
											: <?php echo isset($field_survey['tgl_survey'])?$field_survey['tgl_survey']:'';?>
												
											</div>
										</div>
										
								 <div class="control-group">
											<label for="textfield_survey" class="control-label">Petugas Survey</label>
											<div class="controls">
												
												: <?php echo isset($field_survey['nama_lengkap'])?$field_survey['nama_lengkap']:'';?>
											</div>
										</div>
										
								<div class="control-group">
											<label for="textfield_survey" class="control-label">No.Surat Perintah</label>
											<div class="controls">
												
												: <?php echo isset($field_survey['no_surat_perintah'])?$field_survey['no_surat_perintah']:'';?>
											</div>
										</div>	
										</div>
										</form>
									
								</div>    
							</div>

					<div class="span4">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Data Pemilik
								</h3>
							</div>
							<div class="box-content"  style="height: 200px; overflow-y: scroll;">
								<?php echo form_open('datasurvey/index',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>

                                    <div class="control-group">
											<label for="textfield_survey_det" class="control-label">Nama Pemilik</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['nama_pemilik'])?$field_survey_det['nama_pemilik']:'';?>
												
											</div>
										</div>
								
								   <div class="control-group">
											<label for="textfield_survey_det" class="control-label">Alamat Pemilik</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['alamat_persil'])?$field_survey_det['alamat_persil']:'';?>
												
											</div>
										</div>
										
								  <div class="control-group">
											<label for="textfield_survey_det" class="control-label">Nama Perusahaan</label>
											<div class="controls">
												
											: <?php echo isset($field_survey_det['nama_perusahaan'])?$field_survey_det['nama_perusahaan']:'';?>
												
											</div>
										</div>
										
								 <div class="control-group">
											<label for="textfield_survey_det" class="control-label">Alamat Persil</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['alamat_persil'])?$field_survey_det['alamat_persil']:'';?>
											</div>
										</div>
                                    
									<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Kecamatan</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['nama_kec'])?$field_survey_det['nama_kec']:'';?>
											</div>
										</div>
									
									<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Kelurahan</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['nama_kel'])?$field_survey_det['nama_kel']:'';?>
											</div>
										</div>	
										
								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Kategori</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['nama_kat'])?$field_survey_det['nama_kat']:'';?>
											</div>
										</div>	
										
								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Jenis Izin</label>
											<div class="controls">
												: <?php echo isset($field_survey_det['nama_jenis'])?$field_survey_det['nama_jenis']:'';?>				</div>
										</div>	
									
									<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Nomor Izin</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['nomor_izin'])?$field_survey_det['nomor_izin']:'';?>
											</div>
										</div>	
								
								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Tanggal Izin</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['tanggal_izin'])?$field_survey_det['tanggal_izin']:'';?>
											</div>
										</div>	
										
										
										
							<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Peruntukan Bangunan</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['nama_peruntukkan'])?$field_survey_det['nama_peruntukkan']:'';?>
											</div>
										</div>	
										
										
								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Luas Persil</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_det['luas_persil'])?$field_survey_det['luas_persil']:'';?>
											</div>
										</div>	

								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Nomor Persil</label>
											<div class="controls">
												:
												: <?php echo isset($field_survey_det['no_persil'])?$field_survey_det['no_persil']:'';?>
											</div>
										</div>

								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Jumlah Lantai/Tingkat</label>
											<div class="controls">
												:
												<?php echo isset($field_survey_det['jumlah_tingkat'])?$field_survey_det['jumlah_tingkat']:'';?>
											</div>
										</div>

								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">RT</label>
											<div class="controls">
												
												
												: <?php echo isset($field_survey_det['rt'])?$field_survey_det['rt']:'';?>
											</div>
										</div>

								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">RW</label>
											<div class="controls">
												:
												
												<?php echo isset($field_survey_det['rw'])?$field_survey_det['rw']:'';?>
											</div>
										</div>

								<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Luas Bangunan</label>
											<div class="controls">
												:
												
												<?php echo isset($field_survey_det['luas_bangunan'])?$field_survey_det['luas_bangunan']:'';?>
											</div>
										</div>	

																<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Status Ketemu  Pemilik</label>
											<div class="controls">
													:
											
												<?php 
											if ($field_survey_det['status_ketemu_pemilik']==1) { echo "Ketemu";}  
											elseif ($field_survey_det['status_ketemu_pemilik']==0) { echo "Tidak Ketemu";} 
											?>
											</div>
										</div>		
									
								</form>

							</div>    

							</div>
						</div>

						<div class="span4">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Data Piutang Retribusi
								</h3>
							</div>
							<div class="box-content" style="height: 200px; overflow-y: scroll;">
								<?php echo form_open('datasurvey/index',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>
 
                                    <div class="control-group">
											<label for="textfield_survey_det" class="control-label">Nilai Piutang Retribusi</label>
											<div class="controls">
												
												:  <?php echo isset($field_survey_piutang['nilai_piutang'])?$field_survey_piutang['nilai_piutang']:'';?>
												
											</div>
										</div>
										
								  <div class="control-group">
											<label for="textfield_survey_det" class="control-label">Tanggal SKRD</label>
											<div class="controls">
												
											: <?php echo isset($field_survey_piutang['tgl_skrd'])?$field_survey_piutang['tgl_skrd']:'';?>
												
											</div>
										</div>
										
								 <div class="control-group">
											<label for="textfield_survey_det" class="control-label">Nilai Piutang Denda</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_piutang['nilai_piutang_denda'])?$field_survey_piutang['nilai_piutang_denda']:'';?>
											</div>
										</div>
                                    
									
									<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Status piutang</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_piutang['nama_status_piutang'])?$field_survey_piutang['nama_status_piutang']:'';?>
											</div>
										</div>
                                    	
									
										<div class="control-group">
											<label for="textfield_survey_det" class="control-label">Alasan belum membayar</label>
											<div class="controls">
												
												: <?php echo isset($field_survey_piutang['alasan_blm_bayar'])?$field_survey_piutang['alasan_blm_bayar']:'';?>
											</div>
										</div>

								</form>
							
						</div>
					</div>
				</div>

				<div class="span6">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Data Foto
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content" style="height: 500px; overflow-y: scroll;">
								<ul class="gallery">
										
										
										<?php foreach($ListDataFoto as $row1) 	{
										
											$latitude=$row1['latitude'];
											$longitude=$row1['longitude'];
											$directory=$row1['directory'];

										?>
										<li>
											<a href="#">
												<img src="<?php echo base_url();?>assets/uploads/<?php echo $row1['directory']; ?>" alt="" width="200" height="100">
											</a>
											<div class="extras">
												<div class="extras-inner">
													<a href="<?php echo base_url();?>assets/uploads/<?php echo $row1['directory']; ?>" class='colorbox-image' rel="group-1" target="_blank"><i class="icon-search"></i></a>
													
												</div>
											</div>
										</li>
										
										<?php } ?>
							</div>
						</div>
					</div>



				<div class="span6">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Peta 
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content" style="height: 500px; overflow-y: scroll;">
																		
						    <div id="map" style="width: 98%; height:550px;"></div>
						  <link rel="stylesheet" href="<?php echo site_url();?>tema/leaflet/leaflet.css" />
						  <script src="<?php echo site_url();?>tema/leaflet/leaflet.js"></script>
						  
						  <script>
							
							
						   
						   var map = L.map('map').setView([-6.903274, 107.5731161], 13);

								L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
									attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
								}).addTo(map);

								var LeafIcon = L.Icon.extend({
									options: {
										shadowUrl: '<?php echo site_url();?>tema/leaflet/images/marker-shadow.png'
										
									}
								});

								var greenIcon = new LeafIcon({iconUrl: '<?php echo site_url();?>tema/leaflet/images/marker-icon.png'});
									

								L.marker([<?php echo $latitude; ?>, <?php echo $longitude; ?>], {icon: greenIcon}).bindPopup("<img src='<?php echo base_url();?>assets/uploads/<?php echo $directory; ?>' width=200 height=100>").addTo(map);


						</script>
							</div>
						
				</div>
				</div>






