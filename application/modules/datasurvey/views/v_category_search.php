			<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Data Hasil Survey</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
                <div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>datasurvey">Data Hasil Survey</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>

			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">
								<div align="right">
	                            <a class="btn btn-green" href="<?php echo site_url();?>datasurvey/export">Export to Excel</a>
	                            </div>
								<?php 
									if (isset($field['id_detail'])) { $dis="disabled";} else { $dis="";}				  
										$katakunci= isset($field['katakunci'])?$field['katakunci']:$this->input->post('katakunci');				
								?>

	                            <?php 
									if (isset($field['id_detail'])) { $dis="disabled";} else { $dis="";}			  
										$katakunci= isset($field['katakunci'])?$field['katakunci']:$this->input->post('katakunci');
										$kategori= isset($field['kategori'])?$field['kategori']:$this->input->post('kategori');			
								?>
								
								<div class="control-group">
									<!-- 	<label class="control-label" for="textfield">Pencarian</label>
										<div class="controls">
										<input type="text" value="<?php echo $this->session->userdata('katakunci'); ?>" "<?php if ($katakunci== ['katakunci']) { echo "selected";}  ?> class="form-control" name="katakunci" placeholder="Masukan kata kunci..."  >	 -->

									<form action="<?php echo site_url('datasurvey/lihat_klasifikasiKategori'); ?>" method="post" name="form2" class="form-horizontal form-bordered">

										<select name="kategori" id="kategori" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

											<option value="general">Tampil Kategori</option>
				                            <option value="oss" <?php if ($kategori== "izin") { echo "selected";} ?>>Data OSS</option>
				                            <option value="izin" <?php if ($kategori== "izin") { echo "selected";} ?>>Berizin</option>
				                            <option value="tidakizin" <?php if ($kategori== "tidakizin") { echo "selected";} ?>>Tidak Berizin</option>
				                            <option value="lahankosong" <?php if ($kategori== "lahankosong") { echo "selected";}?>>Lahan Kosong</option>
				                            <option value="ketemupemilik" <?php if ($kategori== "ketemupemilik") { echo "selected";} ?>>Surveyor Ketemu Pemilik</option>
				                             <option value="tidakketemupemilik" <?php if ($kategori== "tidakketemupemilik") { echo "selected";} ?>>Surveyor Tidak Ketemu Pemilik</option>
				                             <option value="selesai" <?php if ($kategori== "selesai") { echo "selected";} ?>>Transaksi Selesai</option>
				                             <option value="belumselesai" <?php if ($kategori== "belumselesai") { echo "selected";} ?>>Transaksi Belum Selesai</option>
				                             <option value="gagal" <?php if ($kategori== "gagal") { echo "selected";} ?>>Transaksi Gagal</option>
				                        </select>
				                        
				                        <button class="btn btn-primary" type="submit">Cari</button>
									  	</div>
									</div>
			
										<table width="100%" class="table table-hover">
									    <thead>
											<tr>
											  <th>TGL.SURVEY</th>
											  <th>KODE TRANSAKSI SURVEY</th>
											  <th>SURVEY OLEH</th>
										      <th>NO.SURAT PERINTAH</th>
											  <th>NAMA PEMILIK</th>
										      <th>ALAMAT</th>
											  <th>STATUS KETEMU PEMILIK</th>
											  <th>STATUS TRANSAKSI</th>
											  <th>KATEGORI IZIN</th>
											  <th>Aksi</th>
											  <th>&nbsp;</th>
			    							</tr>
										</thead>
										<tbody>
											<?php
											if (count($ListDataKategori) > 0) {
												foreach($ListDataKategori as $row)
												{
												?>

										<tr>
											<td><?php echo $row['tgl_survey']; ?></td>
											<td><?php echo $row['kode_transaksi']; ?></td>
											<td><?php echo $row['nama_lengkap']; ?></td>
											<td><?php echo $row['no_surat_perintah']; ?></td>
											<td><?php echo $row['nama_pemilik']; ?></td>
										    <td><?php echo $row['alamat_persil']; ?></td>
										    <td><?php 
											if ($row['status_ketemu_pemilik']==1) { echo "Ketemu";}  
											elseif ($row['status_ketemu_pemilik']==0) { echo "Tidak Ketemu";}  
											?></td>
											<td><?php 
											if ($row['status_transaksi']==1) { echo "<a style='color:green; font-weight: bold;' >Selesai<a>";}  
											elseif ($row['status_transaksi']==0) { echo "Gagal";}  
											elseif ($row['status_transaksi']==2) { echo "<a style='color:red; font-weight: bold;' >Belum Selesai<a>";}  
											?></td>
											<td><?php echo $row['nama_kat']; ?></td>
									      <td>
											<a class="btn btn-mini btn-primary " href="<?php echo site_url();?>datasurvey/lihat/<?php echo $row['kode_transaksi']; ?>"><i class="icon-eye-open"></i> Lihat Detail</a>											</td>
									      <td>
										  <a class="btn btn-mini btn-danger" href="<?php echo site_url('datasurvey/hapus/'.$row['kode_transaksi']);?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>										  </td>
		     						 </tr>

										<?php
										
										$paging=(!empty($pagermessage) ? $pagermessage : '');
												
											}
											echo "<tr><td colspan='20'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
										} else {
											echo "<tbody><tr><td colspan='20' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
										}
										?>
										</tbody>
									</table>									
								</form>		
							</div>
						</div>
					</div>
				</div> 
