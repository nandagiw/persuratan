<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Data Hasil Survey</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
                <div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>datasurvey">Data Hasil Survey</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				
			</div>
			

			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">
								
							

							<div align="right">
                            <a class="btn btn-green" href="<?php echo site_url();?>datasurvey/exportExcelOSS">Export to Excel</a>
                            </div>

                            
							<?php 
								if (isset($field['id_detail'])) { $dis="disabled";} else { $dis="";}				  
								$katakunci= isset($field['katakunci'])?$field['katakunci']:$this->input->post('katakunci');				
							?>

                            <?php 
								if (isset($field['id_detail'])) { $dis="disabled";} else { $dis="";}			  
								$katakunci= isset($field['katakunci'])?$field['katakunci']:$this->input->post('katakunci');
								$kategori= isset($field['kategori'])?$field['kategori']:$this->input->post('kategori');			
							?>
								
									<div class="control-group">
									<!-- 	<label class="control-label" for="textfield">Pencarian</label>
										<div class="controls">
										<input type="text" value="<?php echo $this->session->userdata('katakunci'); ?>" "<?php if ($katakunci== ['katakunci']) { echo "selected";}  ?> class="form-control" name="katakunci" placeholder="Masukan kata kunci..."  >	 -->

										<form action="<?php echo site_url('datasurvey/lihat_klasifikasiKategori'); ?>" method="post" name="form2" class="form-horizontal form-bordered">

									<select name="kategori" id="kategori" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

											<option value="general">Tampil Kategori</option>
				                             
				                             <option value="oss" <?php if ($kategori== "oss") { echo "selected";} ?>>Data OSS</option>
				                             <option value="izin" <?php if ($kategori== "izin") { echo "selected";} ?>>Berizin</option>
				                             <option value="tidakizin" <?php if ($kategori== "tidakizin") { echo "selected";} ?>>Tidak Berizin</option>
				                             <option value="lahankosong" <?php if ($kategori== "lahankosong") { echo "selected";} ?>>Lahan Kosong</option>
				                             <option value="ketemupemilik" <?php if ($kategori== "ketemupemilik") { echo "selected";} ?>>Surveyor Ketemu Pemilik</option>
				                             <option value="tidakketemupemilik" <?php if ($kategori== "tidakketemupemilik") { echo "selected";} ?>>Surveyor Tidak Ketemu Pemilik</option>

				                            </select>
				                            <button class="btn btn-primary" type="submit">Cari</button>
									  	</div>
									</div>
								
						<div class="span11">
						<div class="box box-color box-bordered" >
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Data OSS IMB
								</h3>
							</div>
							<div class="box-content" style="height: 350px; width: 120
							0px;overflow-y: scroll;">	
		
											
		<table width="100%" class="table table-hover">
	    <thead>
				<tr>
				  <th>NAMA PEMOHON</th>
											<th>ALAMAT PEMOHON KABKOT</th>
											<th>ALAMAT PEMOHON JALAN</th>
											<th>ALAMAT PEMOHON NO</th>
											<th>ALAMAT PEMOHON RT</th>
											<th>ALAMAT PEMOHON RW</th>
											<th>LOKASI PERSIL KEC</th>
											<th>LOKASI_PERSIL KEL</th>
											<th>LOKASI PERSIL JALAN</th>
											<th>LOKASI PERSIL NO</th>
										    <th>LOKASI PERSIL RT</th>
										    <th>LOKASI PERSIL RW</th>
											<th>LUAS TANAH</th>
											<th>LUAS BANGUNAN</th>
											<th>JUMLAH TINGKAT</th>
											<th>PERUNTUKKAN</th>
											<th>FUNGSI</th>
										    <th>ID</th>
			    </tr>
			</thead>
			<tbody>
				<?php
				if (count($ListDataKategori) > 0) {
					foreach($ListDataKategori as $row)
					{
					
						
						?>

						<tr>
						  <td><?php echo $row['NAMA_PEMOHON']; ?></td>
											<td><?php echo $row['ALAMAT_PEMOHON_KABKOT']; ?></td>
											<td><?php echo $row['ALAMAT_PEMOHON_JALAN']; ?></td>
											<td><?php echo $row['ALAMAT_PEMOHON_NO']; ?></td>
											<td><?php echo $row['ALAMAT_PEMOHON_RT']; ?></td>
										    <td><?php echo $row['ALAMAT_PEMOHON_RW']; ?></td>
										    <td><?php echo $row['LOKASI_PERSIL_KEC']; ?></td>
											<td><?php echo $row['LOKASI_PERSIL_KEL']; ?></td>
											<td><?php echo $row['LOKASI_PERSIL_JALAN']; ?></td>
											<td><?php echo $row['LOKASI_PERSIL_NO']; ?></td>
										    <td><?php echo $row['LOKASI_PERSIL_RT']; ?></td>
										    <td><?php echo $row['LOKASI_PERSIL_RW']; ?></td>
											<td><?php echo $row['LUAS_TANAH']; ?></td>
											<td><?php echo $row['LUAS_BANGUNAN']; ?></td>
											<td><?php echo $row['JUMLAH_TINGKAT']; ?></td>
										    <td><?php echo $row['PERUNTUKKAN']; ?></td>
										    <td><?php echo $row['FUNGSI']; ?></td>
										    <td><?php echo $row['ID']; ?></td>
		      </tr>

				<?php
				
				$paging=(!empty($pagermessage) ? $pagermessage : '');
						
					}
					echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
				} else {
					echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
				}
				?>
			</tbody>
		</table>
		</div>
		</div>
		</div>									
											
											
											
											
							
			
	
		
		
		
		</form>	
								
								
								
							</div>
						</div>
					</div>
				</div> 
