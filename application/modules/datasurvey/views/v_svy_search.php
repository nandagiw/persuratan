		<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Data Hasil Survey</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
                <div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>datasurvey">Data Hasil Survey</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<h5>Surveyor 		   		: <?php echo $this->session->userdata('sesi_nama_lengkap'); ?></h5>
				
<!-- 				<?php
				if (count($yes) > 0) {
					foreach($yes as $row)
					{
						?>
						<h5>Total Data Transaksi 		: <?php echo $row['jml_transaksi']; ?></h5>
				<h5>Transaksi Selesai 		: <?php echo $row['jml_selesai']; ?></h5>
				<h5>Transaksi Belum Selesai 				: <?php echo $row['jml_tidakselesai']; ?></h5>
				<h5>Transaksi Gagal 				: <?php echo $row['jml_gagal']; ?></h5>
				<?php 
					}
				}
				 ?> -->

				
			</div>
			

			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">
								
								<form action="<?php echo site_url('datasurvey/cariNamaSurveyor'); ?>" method="post" name="form1" class="form-horizontal form-bordered">

									<?php 
                                    if ($this->session->flashdata('message_gagal')) {
                                    echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                                    }

                                    if ($this->session->flashdata('message_sukses')) {
                                    echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                                    }

                                   ?>

							<!-- <?php 
								if (isset($field['id_detail'])) { $dis="disabled";} else { $dis="";}				  
								$alamat= isset($field['alamat'])?$field['alamat']:$this->input->post('alamat');				
							?>

                            <?php 
								if (isset($field['id_detail'])) { $dis="disabled";} else { $dis="";}			  
								$alamat= isset($field['alamat'])?$field['alamat']:$this->input->post('katakunci');			
							?> -->
								
									<div class="control-group">
										<label class="control-label" for="textfield">Pencarian</label>
										<div class="controls">
										<input type="text" value="<?php echo $this->session->userdata('alamat'); ?>" "<?php if ($alamat== ['alamat']) { echo "selected";}  ?> class="form-control" name="alamat" placeholder="Masukan kata kunci..."  >	
									  	</div>
									</div>
												
		<table width="100%" class="table table-hover">
	    <thead>
				<tr>
											<th>KODE TRANSAKSI SURVEY</th>
											<th>TANGGAL SURVEY</th>
											<th>NAMA PEMILIK</th>
											<th>KECAMATAN</th>
											<th>KELURAHAN</th>
											<th>ALAMAT</th>
											<th>STATUS KETEMU PEMILIK</th>
											<th>STATUS TRANSAKSI</th>
											<th>AKSI</th>
										    <th>&nbsp;</th>
										    <th>&nbsp;</th>
			    </tr>
			</thead>
			<tbody>
				<?php
				if (count($ListDataSearch) > 0) {
					foreach($ListDataSearch as $row)
					{

						?>

						<tr>
						 					<td><?php echo $row['kode_transaksi']; ?></td>
											<td><?php echo $row['tgl_survey']; ?></td>
											<td><?php echo $row['nama_pemilik']; ?></td>
											<td><?php echo $row['nama_kec']; ?></td>
											<td><?php echo $row['nama_kel']; ?></td>
											<td><?php echo $row['alamat_persil']; ?></td>
											<td><?php 
											if ($row['status_ketemu_pemilik']==1) { echo "Ketemu";}  
											elseif ($row['status_ketemu_pemilik']==0) { echo "Tidak Ketemu";}  
											?></td>
											<td><?php 
											if ($row['status_transaksi']==1) { echo "<a style='color:green; font-weight: bold;' >Selesai<a>";}  
											elseif ($row['status_transaksi']==0) { echo "Gagal";}  
											elseif ($row['status_transaksi']==2) { echo "<a style='color:red; font-weight: bold;' >Belum Selesai<a>";}  
											?></td>
											<td>
											<a class="btn btn-mini btn-primary" href="<?php echo site_url('datasurvey/lihatDetailSurveyor/'.$row['kode_transaksi']);?>"><i class="icon-eye-open"></i>Lihat Detail</a>											
											</td>
											<td>
											<a class="btn btn-mini btn-warning" href="<?php echo site_url('datasurvey/ubahDataSurveyor/'.$row['kode_transaksi']);?>"><i class="icon-eye-open"></i>Perbarui Data</a>											
											</td>
											<td>
										  <a class="btn btn-mini btn-danger" href="<?php echo site_url('datasurvey/hapusDataSurveyor/'.$row['kode_transaksi']);?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>										  </td>
		      </tr>

				<?php
				
				$paging=(!empty($pagermessage) ? $pagermessage : '');
						
					}
					echo "<tr><td colspan='10'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
				} else {
					echo "<tbody><tr><td colspan='10' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
				}
				?>
			</tbody>
		</table>									

		</form>	

							</div>
						</div>
					</div>
				</div>
				

