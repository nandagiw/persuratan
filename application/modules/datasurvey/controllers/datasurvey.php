<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datasurvey extends CI_Controller{

    public function __construct() {

        parent::__construct();
		$this->load->library('encrypt');
		$this->atos_tiasa_leubeut();
		$this->load->library("Excel/PHPExcel");
		$this->load->library('Excel/IOFactory');
		// $this->load->library('fpdf/fpdf');
		$this->load->library('tools');
        $this->load->helper(array('form','url'));
        $this->load->database();

    }
      
	public function atos_tiasa_leubeut(){
	
	   /*
		Author   : Joe
		Modified :
		Date     : 4-Nov-2016
		Desc     : Data Survey
		
		*/

		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}

    }
	
	public function index( $offset = 0 ) {
	
		/*
		Author   : Joe
		Modified :
		Date     : 4-Nov-2016
		Desc     : Menampilkan List Data dan Hasil Pencarian		
		*/
	
		if (isset($_POST['cari_global'])) {
		$data1 = array('s_cari_global' => $_POST['cari_global']);
		$this->session->set_userdata($data1);			
		}
			
		$per_page = 20;  
		$qry = "SELECT DISTINCT 
				A.*,
				B.nama_lengkap,
				C.nama_pemilik,
				C.alamat_persil,
				C.status_ketemu_pemilik,
				A.status_transaksi,
				D.nama_kat
				FROM
				tr_survey A
				LEFT JOIN sis_pengguna B ON A.id_user=B.id
				LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
				LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
				WHERE
				A.status_transaksi !='3' ";
							
		
		if ($this->session->userdata('s_cari_global')!="") {
			$qry.="  and (A.no_surat_perintah 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			or B.nama_lengkap 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			)
			";
		} 
		elseif ($this->session->userdata('s_cari_global')=="") {
			$this->session->unset_userdata('s_cari_global');
		} 	

		$qry.= " order by A.tgl_survey desc
		"; 
		//echo $qry;
	
		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 3;
		$config['base_url']= base_url().'/datasurvey/index'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(3);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		
		$qry .= " limit {$per_page} offset {$offset} ";
		$this->data['ListData'] = $this->db->query($qry)->result_array();     
	
		$this->data['sub_judul_form']="Data Hasil Survey ";	
		$this->template->load('rorompok','v_index',$this->data);	
	
   }

   public function lihat() {
   
		/*
		Author   : Joe
		Modified :
		Date     : 4-Nov-2016
		Desc     : Proses Lihat Detail Data Survey
		*/
   
	   $kode_transaksi=$this->uri->segment(3);
	   
	   $sql = "SELECT 
				A.*,
				B.nama_lengkap,
				C.nama_pemilik,
				C.alamat_persil
				FROM
				tr_survey A
				LEFT JOIN sis_pengguna B ON A.id_user=B.id
				LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
				WHERE
				A.status_transaksi !='3' AND A.kode_transaksi='".$kode_transaksi."' ";

	   $query = $this->db->query($sql);
	   $data['field_survey'] = $query->row_array();

	   $sql_d = "SELECT 
					A.*,
					B.nama_kec ,
					C.nama_kel,
					D.nama_kat,
					E.nama_peruntukkan,
					F.nama_jenis
					FROM
					tr_detail_survey A
					LEFT JOIN ref_kategori D ON A.kat_izin=D.id_kategori
					LEFT JOIN ref_peruntukkan E ON A.id_peruntukkan=E.id_peruntukkan
					LEFT JOIN ref_kecamatan B ON A.kode_kecamatan=B.kode_kecamatan
					LEFT JOIN ref_jenis_izin F ON A.id_jenis_izin=F.id_jenis_izin
					LEFT JOIN ref_kelurahan C ON A.kode_kelurahan=C.kode_kelurahan AND A.kode_kecamatan=C.kode_kecamatan 
					WHERE
					A.kode_transaksi='".$kode_transaksi."' ";

	   $query_d = $this->db->query($sql_d);
	   $data['field_survey_det'] = $query_d->row_array();

	   $sql_foto = "select * from tr_foto where kode_transaksi='".$kode_transaksi."' ";
	   $data['ListDataFoto'] = $this->db->query($sql_foto)->result_array();  

       $sql_piutang = "SELECT 
					A.*,
					B.nama_status_piutang
					FROM
					tr_piutang_retribusi A, ref_status_piutang B
					WHERE
					A.id_status_piutang=B.id_status_piutang AND
					A.kode_transaksi='".$kode_transaksi."' ";

	   $query_piutang = $this->db->query($sql_piutang);
	   $data['field_survey_piutang'] = $query_piutang->row_array();
   
   
	   $data['judul_form']="Data Hasil Survey ";
	   
	   $this->template->load('rorompok','v_view_detail',$data);
   
   }

   public function potensi( $offset = 0 ){

	    /*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 05-Des-2016
		Desc     : menampilkan daftar potensi (data yang tidak berizin)
		*/

		if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);			
		}
			
		$per_page = 10;  
		$qry = "select A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, C.status_ketemu_pemilik, A.status_transaksi, M.nama_peruntukkan, C.rt, C.rw, C.luas_bangunan, 
			N.nama_param, E.nama_kat, 

			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(10,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto, 

			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(10,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah) * sum(N.pers_netto_berizin) / 100) as neto 

			from tr_detail_survey C, m_parameter_potensi N
				,tr_survey A, ref_kecamatan B, ref_kelurahan D, ref_kategori E, ref_peruntukkan M 

				where C.kode_transaksi=A.kode_transaksi and A.id_survey=C.id_survey and C.kat_izin=E.id_kategori
				and B.kode_kecamatan=C.kode_kecamatan and D.kode_kelurahan=C.kode_kelurahan and C.id_peruntukkan=M.id_peruntukkan and M.id_param=N.id_param and C.luas_bangunan not like '%-%' and C.luas_bangunan != '0'
				GROUP BY A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, C.status_ketemu_pemilik, A.status_transaksi, M.nama_peruntukkan, C.luas_bangunan, N.nama_param, E.nama_kat, C.rt, C.rw ";

		if ($this->session->userdata('s_cari_global')!="") {
			$qry.="  and (A.no_surat_perintah 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			or B.nama_lengkap 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			)
			";
		} 
		elseif ($this->session->userdata('s_cari_global')=="") {
			$this->session->unset_userdata('s_cari_global');
		} 	

		//echo $qry;

		$offset = ($this->uri->segment(4) != '' ? $this->uri->segment(4):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/datasurvey/potensi/index'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(3);      
		$this->data['offset'] = $offset;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		$qry .= " limit {$per_page} offset {$offset} ";

		$combo_kat = "SELECT R.* FROM ref_kategori R";
		$this->data['ComboKat'] = $this->db->query($combo_kat)->result_array();
		$this->data['potensi'] = $this->db->query($qry)->result_array();
		// $this->data['brutoResult'] = $this->db->query($qry2)->result_array();
	
		$this->data['sub_judul_form']="Data Hasil Survey ";	
		$this->template->load('rorompok','v_index_potensi',$this->data);

   }

   public function lihat_klasifikasiKategori(){

	    /*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 02-Des-2016
		Desc     : Menampilkan Data Survey OSS, Tidak Berizin, dan tidak ketemu pemilik
		*/

		$kategori = $this->input->post('kategori');

		if ($kategori == "oss") {
			
			$per_page = 20;  
					$qry = "SELECT DISTINCT 
							A.*
							FROM
							oss_imb A ";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_index_oss',$data);

		}elseif ($kategori == "general") {
			
			redirect('datasurvey');

		}elseif ($kategori == "izin") {
			
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					C.kat_izin = 1";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);

		}elseif ($kategori == "tidakizin") {
			
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					C.kat_izin = 2";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);

		}elseif ($kategori == "lahankosong") {
			
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					C.kat_izin = 3";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);

		}elseif ($kategori == "tidakketemupemilik") {
				
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					C.status_ketemu_pemilik ='0' ";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);

		}elseif ($kategori == "ketemupemilik") {
				
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					C.status_ketemu_pemilik ='1' ";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);
		
		}elseif ($kategori == "selesai") {
				
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					A.status_transaksi ='1' ";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);
		
		}elseif ($kategori == "belumselesai") {
				
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					A.status_transaksi ='2' ";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);
		
		}elseif ($kategori == "gagal") {
				
			$per_page = 20;  
			$qry = "SELECT DISTINCT 
					A.*,
					B.nama_lengkap,
					C.nama_pemilik,
					C.alamat_persil,
					C.status_ketemu_pemilik,
					D.nama_kat
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN ref_kategori D ON C.kat_izin=D.id_kategori
					WHERE
					A.status_transaksi ='0' ";

			$data['ListDataKategori'] = $this->db->query($qry)->result_array();  
			$this->template->load('rorompok','v_category_search',$data);
		
		}

   }

    public function lihat_klasifikasiKategoriPotensi( $offset = 0 ){

	    /*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 02-Des-2016
		Desc     : Menampilkan Data Survey OSS, Tidak Berizin, dan tidak ketemu pemilik pada Menu Potensi
		*/

		$id_kategori = $this->input->post('id_kategori');

		if ($id_kategori == "general") {
			redirect('datasurvey/potensi');

		}elseif ($id_kategori == "1") {
				
			redirect('datasurvey/klasifikasiKategoriPotensi1');

		}elseif ($id_kategori == "2") {

			redirect('datasurvey/klasifikasiKategoriPotensi2');
		
		}elseif ($id_kategori == "3") {

			redirect('datasurvey/klasifikasiKategoriPotensi3');

		}

   }

   public function klasifikasiKategoriPotensi1(){

   		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 02-Des-2016
		Desc     : Menampilkan kategori wilayah berizin pada menu potensi
		*/

   		$per_page = 10;  
			$qry = "select A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, C.status_ketemu_pemilik, C.rt, C.rw, A.status_transaksi, M.nama_peruntukkan, C.luas_bangunan, N.nama_param, E.nama_kat, 

			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto,

			 ((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(12,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah) * sum(N.pers_netto_berizin) / 100) as neto from tr_detail_survey C, m_parameter_potensi N
					,tr_survey A, ref_kecamatan B, ref_kelurahan D, ref_kategori E, ref_peruntukkan M where C.kat_izin = 1 AND A.status_transaksi !='3' and C.kode_transaksi=A.kode_transaksi and A.id_survey=C.id_survey and B.kode_kecamatan=C.kode_kecamatan and D.kode_kelurahan=C.kode_kelurahan and C.kat_izin=E.id_kategori and C.id_peruntukkan=M.id_peruntukkan and M.id_param=N.id_param and C.luas_bangunan not like '%-%' and C.luas_bangunan != '0'
					GROUP BY A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, C.status_ketemu_pemilik, A.status_transaksi, M.nama_peruntukkan, C.luas_bangunan, N.nama_param, E.nama_kat, C.rt, C.rw
					";

		$offset = ($this->uri->segment(4) != '' ? $this->uri->segment(5):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/datasurvey/klasifikasiKategoriPotensi1/index'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(4);      
		$this->data['offset'] = $offset;
		
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		
		$qry .= " limit {$per_page} offset {$offset} ";
	
		$combo_kat = "SELECT R.* FROM ref_kategori R";
		$this->data['ComboKat'] = $this->db->query($combo_kat)->result_array();

		$this->data['SearchPotensi'] = $this->db->query($qry)->result_array();  
		$this->template->load('rorompok','v_category_search_potensi1',$this->data);

   }

	public function klasifikasiKategoriPotensi2(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 02-Des-2016
		Desc     : Menampilkan kategori wilayah tidak berizin pada menu potensi
		*/

		$per_page = 10;  
		$qry = "select A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, C.status_ketemu_pemilik, A.status_transaksi, M.nama_peruntukkan, C.luas_bangunan, C.rt, C.rw, N.nama_param, E.nama_kat, ((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(8,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto, 

		(sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(8,2))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah) * 

				(select (count(z.id_jawab_kuesioner) * 100) from tr_jawab_kuesioner z, tr_survey y, tr_detail_survey b, ref_peruntukkan c, ref_kategori d, m_parameter_potensi t, ref_kecamatan g, m_kuesioner p where z.jawaban='Tidak' and y.status_transaksi !='3' and b.kat_izin=2 and z.kode_transaksi=b.kode_transaksi and b.id_peruntukkan=c.id_peruntukkan and c.id_param=t.id_param and d.id_kategori=b.kat_izin and z.kode_transaksi=y.kode_transaksi and b.kode_kecamatan=g.kode_kecamatan and z.id_kuesioner=p.id_kuesioner)

		/ (select count(z.id_jawab_kuesioner) from tr_jawab_kuesioner z, tr_survey y, tr_detail_survey b, ref_peruntukkan c, ref_kategori d, m_parameter_potensi t, ref_kecamatan g, m_kuesioner p where b.kat_izin=2 and y.status_transaksi !='3' and z.kode_transaksi=b.kode_transaksi and b.id_peruntukkan=c.id_peruntukkan and c.id_param=t.id_param and d.id_kategori=b.kat_izin and z.kode_transaksi=y.kode_transaksi and b.kode_kecamatan=g.kode_kecamatan and z.id_kuesioner=p.id_kuesioner) / 100) as neto 

		from tr_detail_survey C, m_parameter_potensi N
					,tr_survey A, ref_kecamatan B, ref_kelurahan D, ref_kategori E, ref_peruntukkan M where C.kat_izin = 2 AND A.status_transaksi !='3' and C.kode_transaksi=A.kode_transaksi and C.kat_izin=E.id_kategori and A.id_survey=C.id_survey
					and B.kode_kecamatan=C.kode_kecamatan and D.kode_kelurahan=C.kode_kelurahan and C.id_peruntukkan=M.id_peruntukkan and M.id_param=N.id_param and C.luas_bangunan not like '%-%' and C.luas_bangunan != '0'
					GROUP BY A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, C.status_ketemu_pemilik, A.status_transaksi, M.nama_peruntukkan, C.luas_bangunan, N.nama_param, E.nama_kat, C.rt, C.rw ";

		$offset = ($this->uri->segment(4) != '' ? $this->uri->segment(5):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/datasurvey/klasifikasiKategoriPotensi2/index'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(4);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		$qry .= " limit {$per_page} offset {$offset} ";

		$combo_kat = "SELECT R.* FROM ref_kategori R";
		$this->data['ComboKat'] = $this->db->query($combo_kat)->result_array();

		$this->data['SearchPotensi'] = $this->db->query($qry)->result_array();  
		$this->template->load('rorompok','v_category_search_potensi2',$this->data);

   }   

   public function klasifikasiKategoriPotensi3(){

   		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 02-Des-2016
		Desc     : Menampilkan kategori wilayah lahan kosong pada menu potensi
		*/

   		$per_page = 20;  
		$qry = "select A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, C.status_ketemu_pemilik, A.status_transaksi, M.nama_peruntukkan, C.luas_bangunan, C.rt, C.rw, N.nama_param, E.nama_kat, ((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto, ((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(12,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah) * sum(N.pers_netto_berizin) / 100) as neto from tr_detail_survey C, m_parameter_potensi N, tr_survey A, ref_kecamatan B, ref_kelurahan D, ref_kategori E, ref_peruntukkan M where C.kat_izin = 3 AND A.status_transaksi ='1' and C.kode_transaksi=A.kode_transaksi and A.id_survey=C.id_survey
			and B.kode_kecamatan=C.kode_kecamatan and D.kode_kelurahan=C.kode_kelurahan and C.kat_izin=E.id_kategori and 
			C.id_peruntukkan=M.id_peruntukkan and M.id_param=N.id_param and C.luas_bangunan not like '%-%'		
			GROUP BY A.tgl_survey, C.alamat_persil, C.kode_transaksi, C.luas_bangunan, C.nama_pemilik, 
			C.status_ketemu_pemilik, A.status_transaksi, M.nama_peruntukkan, C.luas_bangunan, N.nama_param, E.nama_kat, C.rt, C.rw
					";

		$offset = ($this->uri->segment(5) != '' ? $this->uri->segment(5):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/datasurvey/klasifikasiKategoriPotensi3/index'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(4);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		$qry .= " limit {$per_page} offset {$offset} ";

		$combo_kat = "SELECT R.* FROM ref_kategori R";
		$this->data['ComboKat'] = $this->db->query($combo_kat)->result_array();

		$this->data['SearchPotensi'] = $this->db->query($qry)->result_array();  
		$this->template->load('rorompok','v_category_search_potensi3',$this->data);

   }

   public function lihatDetailSurveyor() {
   
   
		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 25-Nov-2016
		Desc     : Proses Lihat Detail (Surveyor)
		*/
   
	   $kode_transaksi=$this->uri->segment(3);
	   
	   $sql = "SELECT 
				A.*,
				B.nama_lengkap,
				C.nama_pemilik,
				C.alamat_persil
				FROM
				tr_survey A
				LEFT JOIN sis_pengguna B ON A.id_user=B.id
				LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
				WHERE
				A.status_transaksi!='3' AND A.kode_transaksi='".$kode_transaksi."' ";

	   $query = $this->db->query($sql);
	   $data['field_survey'] = $query->row_array();

	   $sql_d = "SELECT 
				A.*,
				B.nama_kec ,
				C.nama_kel,
				D.nama_kat,
				E.nama_peruntukkan,
				F.nama_jenis
				FROM
				tr_detail_survey A
				LEFT JOIN ref_kategori D ON A.kat_izin=D.id_kategori
				LEFT JOIN ref_peruntukkan E ON A.id_peruntukkan=E.id_peruntukkan
				LEFT JOIN ref_kecamatan B ON A.kode_kecamatan=B.kode_kecamatan
				LEFT JOIN ref_jenis_izin F ON A.id_jenis_izin=F.id_jenis_izin
				LEFT JOIN ref_kelurahan C ON A.kode_kelurahan=C.kode_kelurahan AND A.kode_kecamatan=C.kode_kecamatan 
				WHERE
				A.kode_transaksi='".$kode_transaksi."' ";

	   $query_d = $this->db->query($sql_d);
	   $data['field_survey_det'] = $query_d->row_array();

	   $sql_foto = "select * from tr_foto where kode_transaksi='".$kode_transaksi."' ";
	   $data['ListDataFoto'] = $this->db->query($sql_foto)->result_array();  

	   $sql_piutang = "SELECT 
						A.*,
						B.nama_status_piutang
						FROM
						tr_piutang_retribusi A, ref_status_piutang B
						WHERE
						A.id_status_piutang=B.id_status_piutang AND
						A.kode_transaksi='".$kode_transaksi."' ";

	   $query_piutang = $this->db->query($sql_piutang);
	   $data['field_survey_piutang'] = $query_piutang->row_array();
	   
	   $data['judul_form']="Data Hasil Survey ";
	   
	   $this->template->load('rorompok','v_svy_detail',$data);
   
   }
   
   public function hapus() {
   
   		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 24-November-2016
		Desc     : Proses Menghapus Data By ID
		*/
  
		$kode_transaksi=$this->uri->segment(3);

		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_detail_survey');	

		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_foto');
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_piutang_retribusi');
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_survey');
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_jawab_kuesioner');

		redirect('datasurvey');
   
   }

   public function hapusDataSurveyor() {
   
   		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 24-November-2016
		Desc     : Proses Menghapus Data surveyor By ID
		*/
  
		$kode_transaksi=$this->uri->segment(3);

		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_detail_survey');	

		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_foto');
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_piutang_retribusi');
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->delete('tr_survey');


		redirect('datasurvey/indexDataSurveyor');
   
   }

   public function ambildataKelurahan(){

   		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 24-November-2016
		Desc     : Proses Mengambil kode kecamatan
		*/

   		$kode_kecamatan = $this->input->post('kode_kecamatan');

   		$qry = "SELECT
				A.*
				FROM ref_kelurahan A
				LEFT JOIN ref_kecamatan B ON A.kode_kecamatan=B.kode_kecamatan
				WHERE A.kode_kecamatan = '".$kode_kecamatan."'
				";

   		$data['result'] = $this->db->query($qry)->result_array();  

   		if($result->num_rows() > 0){
   			return $result->result_array();
   		}else{
   			return array();
   		}

   		$data .= "<option value=''>--pilih--</option>";
   		foreach ($result as $key) {
   			$data .= "<option value='$key[kode_kelurahan]'>$key[nama_kel]</option>";
   		}
   		echo $data;

   }
   
   public function index_log( $offset = 0 ) {
			
		/*
		Author   : Joe
		Modified :
		Date     : 24-Okt-2016
		Desc     : Menampilkan List Data Log By User ID	
		*/
			
		$id_user=$this->uri->segment(3);
			
		if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);			
		}
			
		$per_page = 30;  
		$qry = "select b.* from log  b where b.id_pengguna ='".$id_user."'";
		
		
		if ($this->session->userdata('s_cari_global')!="") {
			$qry.="  and (b.action 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			or b.modules 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			) ";
		} 
		elseif ($this->session->userdata('s_cari_global')=="") {
			$this->session->unset_userdata('s_cari_global');
		} 	

		$qry.= " order by b.tgl desc,b.time desc
		"; 
		//echo $qry;
	

		$offset = ($this->uri->segment(4) != '' ? $this->uri->segment(4):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 4;
		$config['base_url']= base_url().'/datasurvey/index_log/'.$id_user.'/'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(4);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		$qry .= " limit {$per_page} offset {$offset} ";
		$this->data['ListData'] = $this->db->query($qry)->result_array();     
	
		$sql = "SELECT * FROM sis_pengguna  WHERE id='".$id_user."' ";
		$query = $this->db->query($sql);
		$this->data['field'] = $query->row_array();

		$this->data['sub_judul_form']="Data Log Hasil Survey  ";	
		$this->template->load('rorompok','v_index_log',$this->data);	

   }

   public function importdata(){

        $tb=$this->input->post('tb1');
        $fl=$this->input->post('file');
        $br=$this->input->post('mulai');
        $config['upload_path'] = './';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
 
        $this->load->library('upload', $config);
        
        
        if ($this->upload->do_upload('file'))        
        {
            $data = $this->upload->data();
            $nama=$data['file_name'];
            if(file_exists("./".$nama))
            {
                $file="./".$nama;
                $this->tools->importdata($file,$tb,$br,TRUE);
                unlink($file);
                echo json_encode('Berhasil import data');                
            }else{
                echo json_encode('Gagal, karena kesalahan file atau file tidak ditemukan');
            }
        }else{
            echo json_encode('Gagal upload file');
        }
        
    }

    public function indexDataSurveyor(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 23-Nov-2016
		Desc     : Menampilkan List Data Hasil Surveyor (user: surveyor)		
		*/

		if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);			
		}
			// $nama = $this->input->post('sesi_nama_lengkap');

		$this->data['nama_pengguna'] = $this->session->userdata('sesi_nama_lengkap');

		$nama = $this->data['nama_pengguna'];

		$per_page = 20;  
		$qry = "SELECT
				A.*,
				B.nama_lengkap,
				C.alamat_persil,
				C.nama_pemilik,
				C.status_ketemu_pemilik,
				D.nama_kec,
				E.nama_kel
				FROM
				tr_survey A
				LEFT JOIN sis_pengguna B ON A.id_user=B.id
				LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
				LEFT JOIN ref_kecamatan D ON C.kode_kecamatan = D.kode_kecamatan
				LEFT JOIN ref_kelurahan E ON C.kode_kelurahan = E.kode_kelurahan
				WHERE
				B.nama_lengkap='".$nama."' ";

		$qry2 = "select 
				a.nama_lengkap,
				(select count(z.id_detail) as jml_transaksi from tr_detail_survey z,
				tr_survey y where y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi),
				(select count(z.id_detail) as jml_gagal from tr_detail_survey z,
				tr_survey y where y.status_transaksi ='0' and y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi),
				(select count(z.id_detail) as jml_tidakselesai from tr_detail_survey z,
				tr_survey y where y.status_transaksi!='1' and y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi),
				(select count(z.id_detail) as jml_selesai from tr_detail_survey z,
				tr_survey y where y.status_transaksi!='2' and y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi)
				from sis_pengguna a
				where a.nama_lengkap = '".$nama."'";

		$this->data['totalTransaksi'] = $this->db->query($qry2)->result_array();  

		if ($this->session->userdata('s_cari_global')!="") {
			$qry.="  and (A.no_surat_perintah 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			or B.nama_lengkap 
			like upper('%".$this->db->escape_like_str($this->session->userdata('s_cari_global'))."%')
			)";
		} 
		elseif ($this->session->userdata('s_cari_global')=="") {
			$this->session->unset_userdata('s_cari_global');
		} 	

		$qry.= " order by A.tgl_survey desc
		"; 
		//echo $qry;
	

		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 3;
		$config['base_url']= base_url().'/datasurvey/indexDataSurveyor'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(3);      
		$this->data['offset'] = $offset ;
		if($this->data['paginglinks']!= '') {
		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
		}   
		$qry .= " limit {$per_page} offset {$offset} ";
		$this->data['ListDataSurveyor'] = $this->db->query($qry)->result_array();     
	
		$this->data['sub_judul_form']="Data Hasil Survey ";	
		$this->template->load('rorompok','v_svy_index',$this->data);

    }

    public function lihatDataSurveyor(){

    }

    public function tambahDataSurveyor(){

    }

    public function ubahDataSurveyor(){

    	/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 09-Jan-2017
		Desc     : Proses mengubah data surveyor		
		*/

    	$kode_transaksi=$this->uri->segment(3);
		$id_kuesioner = $this->input->post('id_kuesioner');
   		$data['judul_form']="Ubah Data Hasil Survey";
		$data['sub_judul_form']="";

		$this->data['nama_pengguna'] = $this->session->userdata('sesi_nama_lengkap');

		$nama = $this->data['nama_pengguna'];

		$sql1 = "SELECT
				A.id_survey,
				A.tgl_survey,
				A.kode_transaksi,
				B.nama_lengkap,
				A.no_surat_perintah,
				C.nama_pemilik,
				C.alamat_persil,
				C.nama_perusahaan,
				I.nama_kec,
				J.nama_kel,
				D.nama_kat,
				K.nama_jenis,
				C.nomor_izin,
				C.tanggal_izin,
				E.nama_peruntukkan,
				C.luas_persil,
				C.no_persil,
				C.jumlah_tingkat,
				C.rt,
				C.rw,
				C.luas_bangunan,
				C.status_ketemu_pemilik,
				L.nilai_piutang,
				L.tgl_skrd,
				L.nilai_piutang_denda,
				M.nama_status_piutang,
				L.alasan_blm_bayar,
				N.directory
				FROM
				tr_survey A
				LEFT JOIN sis_pengguna B ON A.id_user=B.id
				LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
				LEFT JOIN ref_kategori D ON A.id_kategori=D.id_kategori
				LEFT JOIN ref_peruntukkan E ON C.id_peruntukkan=E.id_peruntukkan
				LEFT JOIN tr_foto F ON A.kode_transaksi=F.kode_transaksi
				LEFT JOIN tr_piutang_retribusi G ON A.kode_transaksi=G.kode_transaksi
				LEFT JOIN ref_status_piutang H ON G.id_status_piutang=H.id_status_piutang
				LEFT JOIN ref_kecamatan I ON C.kode_kecamatan=I.kode_kecamatan
				LEFT JOIN ref_kelurahan J ON C.kode_kelurahan=J.kode_kelurahan AND C.kode_kecamatan=J.kode_kecamatan
				LEFT JOIN ref_jenis_izin K ON C.id_jenis_izin=K.id_jenis_izin
				LEFT JOIN tr_piutang_retribusi L ON A.id_survey=L.id_survey
				LEFT JOIN ref_status_piutang M ON L.id_status_piutang=M.id_status_piutang
				LEFT JOIN tr_foto N ON A.id_survey=N.id_survey
				WHERE
				C.kode_transaksi = '".$kode_transaksi."' ";

		$combo_kec = "select
    			  y.*,
    			  (select z.kode_kecamatan 
    			  from tr_detail_survey z
    			  left join ref_kecamatan y on z.kode_kecamatan=y.kode_kecamatan
    			  where z.kode_transaksi in ('".$kode_transaksi."'))
    			  FROM ref_kecamatan y ORDER BY y.nama_kec ASC";

    	// $combo_kec = "SELECT
    	// 			y.*,
    	// 			z.kode_kecamatan
    	// 			FROM tr_detail_survey z
    	// 			LEFT JOIN ref_kecamatan y ON z.kode_kecamatan=y.kode_kecamatan
    	// 			ORDER BY y.nama_kec ASC";

    	$combo_kel = "select
    			  x.*,
    			  (select z.kode_kecamatan from tr_detail_survey z, ref_kelurahan x where z.kode_transaksi='".$kode_transaksi."' and z.kode_kelurahan=x.kode_kelurahan )
    			  FROM ref_kelurahan x ORDER BY x.nama_kel ASC";

    	// $combo_kel = "SELECT
    	// 			x.*,
    	// 			z.kode_kelurahan
    	// 			FROM tr_detail_survey z
    	// 			LEFT JOIN ref_kelurahan x ON z.kode_kelurahan=x.kode_kelurahan
    	// 			ORDER BY x.nama_kel ASC";

    	$combo_kat = "SELECT
    			  X.*
    			  FROM ref_kategori X";

    	$combo_jns = "SELECT
    			  W.*
    			  FROM ref_jenis_izin W";

    	$combo_prtk = "SELECT
    			  P.*
    			  FROM ref_peruntukkan P
    			  ORDER BY P.nama_peruntukkan ASC";

    	$combo_sp = "SELECT
    			  S.*
    			  FROM ref_status_piutang S";

    	$kues  = "SELECT DISTINCT
    			  Q.*
    			  FROM m_kuesioner Q 
    			  ORDER BY Q.id_kuesioner ASC";

    	//=================================================================

    	$jwb1  = "SELECT
    			  V.*
    			  FROM m_pilihan_jawaban V
    			  LEFT JOIN m_kuesioner K ON V.id_kuesioner=K.id_kuesioner
    			  WHERE V.id_kuesioner=1";

    	$jwb2  = "SELECT
				  V.*
				  FROM m_pilihan_jawaban V
				  LEFT JOIN m_kuesioner K ON V.id_kuesioner=K.id_kuesioner
				  WHERE V.id_kuesioner=2 ";

      	$jwb3  = "SELECT
				  V.*
				  FROM m_pilihan_jawaban V
				  LEFT JOIN m_kuesioner K ON V.id_kuesioner=K.id_kuesioner
				  WHERE V.id_kuesioner=3 ";

      	$jwb4  = "SELECT
				  V.*
				  FROM m_pilihan_jawaban V
				  LEFT JOIN m_kuesioner K ON V.id_kuesioner=K.id_kuesioner
				  WHERE V.id_kuesioner=4 ";

      	$jwb5  = "SELECT
				  V.*
				  FROM m_pilihan_jawaban V
				  LEFT JOIN m_kuesioner K ON V.id_kuesioner=K.id_kuesioner
				  WHERE V.id_kuesioner=5 ";

      	$jwb6  = "SELECT
				  V.*
				  FROM m_pilihan_jawaban V
				  LEFT JOIN m_kuesioner K ON V.id_kuesioner=K.id_kuesioner
				  WHERE V.id_kuesioner=6 ";

      	$jwb7  = "SELECT
				  V.*
				  FROM m_pilihan_jawaban V
				  LEFT JOIN m_kuesioner K ON V.id_kuesioner=K.id_kuesioner
				  WHERE V.id_kuesioner=7 ";
	
		$data['ComboKec'] 			= $this->db->query($combo_kec)->result_array();
		$data['ComboKel'] 			= $this->db->query($combo_kel)->result_array();
		$data['ComboKat'] 			= $this->db->query($combo_kat)->result_array();
		$data['ComboJns'] 			= $this->db->query($combo_jns)->result_array();
		$data['ComboPrtk'] 			= $this->db->query($combo_prtk)->result_array();
		$data['ComboSp'] 			= $this->db->query($combo_sp)->result_array();
		$data['ListDataPertanyaan'] = $this->db->query($kues)->result_array();

		$data['ListDataJawaban1'] = $this->db->query($jwb1)->result_array();
		$data['ListDataJawaban2'] = $this->db->query($jwb2)->result_array();
		$data['ListDataJawaban3'] = $this->db->query($jwb3)->result_array();
		$data['ListDataJawaban4'] = $this->db->query($jwb4)->result_array();
		$data['ListDataJawaban5'] = $this->db->query($jwb5)->result_array();
		$data['ListDataJawaban6'] = $this->db->query($jwb6)->result_array();
		$data['ListDataJawaban7'] = $this->db->query($jwb7)->result_array();

		$query1 = $this->db->query($sql1);
   		$data['field'] = $query1->row_array();
		$this->template->load('rorompok','v_svy_update',$data);

    }

    function simpanDataSurveyor(){

    }

    function simpan_updateDataSurveyor(){

    	/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 23-Nov-2016
		Desc     : Operasi Update / Perbarui Data hasil Surveyor 
		*/

		 try {

				$id_user  		= $this->uri->segment(3);
				$jawaban 		= $this->input->post('jawaban');
				$id_pil_jawab 	= $this->input->post('id_pil_jawab');
				$kode_transaksi	= $this->input->post('kode_transaksi');

				$query = $this->db->query("SELECT
					A.*,
					B.*,
					C.*,
					D.*,
					E.*,
					F.*
					FROM
					tr_survey A
					LEFT JOIN sis_pengguna B ON A.id_user=B.id
					LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
					LEFT JOIN tr_piutang_retribusi D ON A.kode_transaksi=D.kode_transaksi
					LEFT JOIN tr_foto E ON A.kode_transaksi=E.kode_transaksi
					LEFT JOIN tr_jawab_kuesioner F ON A.id_survey=F.id_survey
					WHERE
					C.kode_transaksi = '".$kode_transaksi."' 
					 ");

				// $cek=$query->num_rows(); 

				$id_detail_survey		= $this->input->post('id_detail');
				$id_survey 				= $this->input->post('id_survey');
				$id_piutang_retribusi	= $this->input->post('id_piutang_retribusi');
				$id_jawab_kuesioner		= $this->input->post('id_jawab_kuesioner');
				$id_kategori			= $this->input->post('id_kategori');
				$id_jenis_izin			= $this->input->post('id_jenis_izin');
				$kode_transaksi 		= $this->input->post('kode_transaksi');

				// table tr_detail_survey
				$data1 = array(
				'kode_transaksi' 		=> $this->input->post('kode_transaksi'),
				'nama_pemilik' 			=> $this->input->post('nama_pemilik'),
				'alamat_persil' 		=> $this->input->post('alamat_persil'),
				'kode_kecamatan' 		=> $this->input->post('kode_kecamatan'),
				'kode_kelurahan' 		=> $this->input->post('kode_kelurahan'),
				'id_jenis_izin' 		=> $this->input->post('id_jenis_izin'),
				'nomor_izin' 			=> $this->input->post('nomor_izin'),
				'tanggal_izin' 			=> $this->input->post('tanggal_izin'),
				'id_peruntukkan' 		=> $this->input->post('id_peruntukkan'),
				'luas_persil' 			=> $this->input->post('luas_persil'),
				'no_persil' 			=> $this->input->post('no_persil'),
				'jumlah_tingkat' 		=> $this->input->post('jumlah_tingkat'),
				'rt' 					=> $this->input->post('rt'),
				'rw' 					=> $this->input->post('rw'),
				'luas_bangunan' 		=> $this->input->post('luas_bangunan'),
				'status_ketemu_pemilik' => $this->input->post('status_ketemu_pemilik')
				);

				$this->db->where('kode_transaksi', $kode_transaksi);
				$this->db->update('tr_detail_survey', $data1);

				//table  tr_survey
				$data2 = array(
				'no_surat_perintah' 	=> $this->input->post('no_surat_perintah'),
				'tgl_survey' 			=> $this->input->post('tgl_survey'),
				'id_kategori' 			=> $this->input->post('id_kategori'),
				'status_transaksi' 		=> '1');
				
				$this->db->where('kode_transaksi', $kode_transaksi);
				$this->db->update('tr_survey', $data2);

				//table tr_piutang_retribusi
				// $data3 = array(
				// 'nilai_piutang' 		=> $this->input->post('nilai_piutang'),
				// 'tgl_skrd' 				=> $this->input->post('tgl_skrd'),
				// 'nilai_piutang_denda' 	=> $this->input->post('nilai_piutang_denda'),
				// 'id_status_piutang' 	=> $this->input->post('id_status_piutang'), //-----
				// 'alasan_blm_bayar' 		=> $this->input->post('alasan_blm_bayar')
				// );
				
				// $this->db->where('kode_transaksi', $kode_transaksi);
				// $this->db->update('tr_piutang_retribusi', $data3);

				//table tr_jawab_kuesioner

				$arrayjwb 	= $_POST["jawaban"];
				$arraykues 	= $_POST["id_kuesioner"];
				// $data4 = array(
				// 'jawaban' => $this->input->post('jawaban')
				// );

				print_r($arrayjwb);
				echo "<br>";
				print_r($arraykues);
				echo "<br>";

				for($i=0;$i<count($arrayjwb); $i++){

					if ($arrayjwb[$i] == "Ya") {
					$x = $i + $i + 1;
					}elseif ($arrayjwb[$i] == "Tidak") {
					$x = $i + $i + 2;
					}elseif ($arrayjwb[$i] == "Internet"){
					$x = $i + $i + 1;
					}elseif ($arrayjwb[$i] == "Sosial Media"){
					$x = $i + $i + 2;
					}elseif ($arrayjwb[$i] == "Radio"){
					$x = $i + $i + 3;
					}elseif ($arrayjwb[$i] == "Videotron"){
					$x = $i + $i + 4;
					}elseif ($arrayjwb[$i] == "Billboard"){
					$x = $i + $i + 5;
					}elseif ($arrayjwb[$i] == "Media Cetak"){
					$x = $i + $i + 6;
					}elseif ($arrayjwb[$i] == "Kerabat"){
					$x = $i + $i + 7;
					}elseif ($arrayjwb[$i] == "Petugas Layanan"){
					$x = $i + $i + 8;
					}elseif ($arrayjwb[$i] == "Lainnya"){
					$x = $i + $i + 9;
					}else{
						echo "nothing";
					}

					$data4 = array(		
					'id_kuesioner'		=> $arraykues[$i],
					'id_pil_jawab'		=> $x,
					'jawaban' 			=> $arrayjwb[$i],
					'kode_transaksi' 	=> $this->input->post('kode_transaksi'));
				
					$this->db->insert('tr_jawab_kuesioner', $data4);
					
				}

				$this->session->set_flashdata('message_sukses', 'data berhasil diubah');

				redirect('datasurvey/indexDataSurveyor');


			}
			catch(Exception $err)
			{
				$this->session->set_flashdata('message_gagal', 'data gagal diubah');
				log_message("error",$err->getMessage());
				return show_error($err->getMessage());
			}

    }

    public function cari($offset = 0){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 21-Nov-2016
		Desc     : Operasi mencari data kuesioner berdasarkan keyword	
		*/

		$this->data['sub_judul_form']="Hasil Pencarian";

		$katakunci = $_POST['katakunci'];

			$per_page = 20;
			$yuhu = "SELECT 
			A.*,
			B.*,
			C.*,
			D.*
			FROM
			tr_detail_survey A
			LEFT JOIN tr_survey B ON A.id_survey=B.id_survey
			LEFT JOIN sis_pengguna C ON B.id_user=C.id
			LEFT JOIN ref_kategori D ON B.id_kategori=D.id_kategori
			WHERE A.nama_pemilik LIKE '%$katakunci%'
			OR C.nama_lengkap LIKE '%$katakunci%' ";
 	
		$yuhu.= " order by B.tgl_survey desc
		"; 

		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
		$config['total_rows'] = $this->db->query($yuhu)->num_rows();
		$config['per_page']= $per_page;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['uri_segment'] = 3;
		$config['base_url']= base_url().'/datasurvey/cari'; 
		$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
		$this->pagination->initialize($config);
		$this->data['paginglinks'] = $this->pagination->create_links();    
		$this->data['per_page'] = $this->uri->segment(3);      
		$this->data['offset'] = $offset ;

		if($this->data['paginglinks']!= '') {

		  $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($yuhu)->num_rows();

		}   

		$yuhu .= " limit {$per_page} offset {$offset} ";

		$this->data['ListData1'] = $this->db->query($yuhu)->result_array();  
		$this->template->load('rorompok','v_search',$this->data);

	}

	public function cariNamaSurveyor(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : Operasi mencari data nama surveyor berdasarkan keyword	
		*/

		$id_user=$this->uri->segment(3);

		$alamat = $this->input->post('alamat');
		$this->data['nama_pengguna'] = $this->session->userdata('sesi_nama_lengkap');

		
		$nama = $this->data['nama_pengguna'];
		
		$qry = "SELECT
		A.*,
		B.nama_lengkap,
		C.nama_pemilik,
		C.alamat_persil,
		C.status_ketemu_pemilik,
		D.nama_kec,
		E.nama_kel
		FROM
		tr_survey A
		LEFT JOIN sis_pengguna B ON A.id_user=B.id
		LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
		LEFT JOIN ref_kecamatan D ON C.kode_kecamatan = D.kode_kecamatan
		LEFT JOIN ref_kelurahan E ON C.kode_kelurahan = E.kode_kelurahan
		WHERE
		upper(C.alamat_persil) LIKE '%$alamat%' 
		OR lower(C.alamat_persil) LIKE '%$alamat%'
		AND B.nama_lengkap='".$nama."'
		";

		$qry3 = "select 
				a.nama_lengkap,
				(select count(z.id_detail) as jml_transaksi from tr_detail_survey z,
				tr_survey y where y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi),
				(select count(z.id_detail) as jml_gagal from tr_detail_survey z,
				tr_survey y where y.status_transaksi ='0' and y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi),
				(select count(z.id_detail) as jml_tidakselesai from tr_detail_survey z,
				tr_survey y where y.status_transaksi!='1' and y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi),
				(select count(z.id_detail) as jml_selesai from tr_detail_survey z,
				tr_survey y where y.status_transaksi!='2' and y.id_user=a.id
				and z.kode_transaksi=y.kode_transaksi)
				from sis_pengguna a
				where a.nama_lengkap = '".$nama."'";

		$data['yes'] = $this->db->query($qry3)->result_array();

		$data['ListDataSearch'] = $this->db->query($qry)->result_array();  
		$this->template->load('rorompok','v_svy_search',$data);

	}

	public function exportexcel(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : Operasi mencari data nama surveyor berdasarkan keyword	
		*/

		$query = $this->db->query('SELECT DISTINCT
			A.id_survey,
			A.tgl_survey,
			A.kode_transaksi,
			B.nama_lengkap,
			A.no_surat_perintah,
			C.nama_pemilik,
			C.alamat_persil,
			C.nama_perusahaan,
			I.nama_kec,
			J.nama_kel,
			D.nama_kat,
			K.nama_jenis,
			C.nomor_izin,
			C.tanggal_izin,
			E.nama_peruntukkan,
			C.luas_persil,
			C.no_persil,
			C.jumlah_tingkat,
			C.rt,
			C.rw,
			C.luas_bangunan,
			C.status_ketemu_pemilik,
			L.nilai_piutang,
			L.tgl_skrd,
			L.nilai_piutang_denda,
			M.nama_status_piutang,
			L.alasan_blm_bayar,
			N.directory
			FROM
			tr_survey A
			LEFT JOIN sis_pengguna B ON A.id_user=B.id
			LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
			LEFT JOIN ref_kategori D ON A.id_kategori=D.id_kategori
			LEFT JOIN ref_peruntukkan E ON C.id_peruntukkan=E.id_peruntukkan
			LEFT JOIN tr_foto F ON A.kode_transaksi=F.kode_transaksi
			LEFT JOIN tr_piutang_retribusi G ON A.kode_transaksi=G.kode_transaksi
			LEFT JOIN ref_status_piutang H ON G.id_status_piutang=H.id_status_piutang
			LEFT JOIN ref_kecamatan I ON C.kode_kecamatan=I.kode_kecamatan
			LEFT JOIN ref_kelurahan J ON C.kode_kelurahan=J.kode_kelurahan AND C.kode_kecamatan=J.kode_kecamatan
			LEFT JOIN ref_jenis_izin K ON C.id_jenis_izin=K.id_jenis_izin
			LEFT JOIN tr_piutang_retribusi L ON A.id_survey=L.id_survey
			LEFT JOIN ref_status_piutang M ON L.id_status_piutang=M.id_status_piutang
			LEFT JOIN tr_foto N ON A.id_survey=N.id_survey');		  
 
        if(!$query)
            return false;
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objsheet = $objPHPExcel->setActiveSheetIndex(0);

        $objsheet->setTitle('Data Hasil Survey');

        $objsheet->getStyle('A5:AB5')->getFont()->setBold(true)->setSize(12);

        $objPHPExcel->getActiveSheet()->getStyle(
    	'A5:AB5'
		)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

		$objsheet->getCell('A5')->setValue('ID Survey');
		$objsheet->getCell('B5')->setValue('Tanggal Survey');
		$objsheet->getCell('C5')->setValue('Kode Transaksi');
		$objsheet->getCell('D5')->setValue('Nama Surveyor');
		$objsheet->getCell('E5')->setValue('Nomor Surat Perintah');
		$objsheet->getCell('F5')->setValue('Nama Pemilik');
		$objsheet->getCell('G5')->setValue('Alamat Pemilik');
		$objsheet->getCell('H5')->setValue('Nama Perusahaan');
		$objsheet->getCell('I5')->setValue('Nama Kecamatan');
		$objsheet->getCell('J5')->setValue('Nama Kelurahan');
		$objsheet->getCell('K5')->setValue('Kategori');
		$objsheet->getCell('L5')->setValue('Jenis Izin');
		$objsheet->getCell('M5')->setValue('Nomor Izin');
		$objsheet->getCell('N5')->setValue('Tanggal Izin');
		$objsheet->getCell('O5')->setValue('Peruntukkan Bangunan');
		$objsheet->getCell('P5')->setValue('Luar Persil');
		$objsheet->getCell('Q5')->setValue('Nomor Persil');
		$objsheet->getCell('R5')->setValue('Jumlah Tingkat');
		$objsheet->getCell('S5')->setValue('RT');
		$objsheet->getCell('T5')->setValue('RW');
		$objsheet->getCell('U5')->setValue('Luas Bangunan');
		$objsheet->getCell('V5')->setValue('Status Ketemu Pemilik');
		$objsheet->getCell('W5')->setValue('Nilai Piutang');
		$objsheet->getCell('X5')->setValue('Tanggal SKRD');
		$objsheet->getCell('Y5')->setValue('Nilai Piutang Denda');
		$objsheet->getCell('Z5')->setValue('Status Piutang');
		$objsheet->getCell('AA5')->setValue('Alasan Belum Bayar');
		$objsheet->getCell('AB5')->setValue('Lokasi Foto');

		$objsheet->getColumnDimension('A')->setAutoSize(true);
		$objsheet->getColumnDimension('B')->setAutoSize(true);
		$objsheet->getColumnDimension('C')->setAutoSize(true);
		$objsheet->getColumnDimension('D')->setAutoSize(true);
		$objsheet->getColumnDimension('E')->setAutoSize(true);
		$objsheet->getColumnDimension('F')->setAutoSize(true);
		$objsheet->getColumnDimension('G')->setAutoSize(true);
		$objsheet->getColumnDimension('H')->setAutoSize(true);
		$objsheet->getColumnDimension('I')->setAutoSize(true);
		$objsheet->getColumnDimension('J')->setAutoSize(true);
		$objsheet->getColumnDimension('K')->setAutoSize(true);
		$objsheet->getColumnDimension('L')->setAutoSize(true);
		$objsheet->getColumnDimension('M')->setAutoSize(true);
		$objsheet->getColumnDimension('N')->setAutoSize(true);
		$objsheet->getColumnDimension('O')->setAutoSize(true);
		$objsheet->getColumnDimension('P')->setAutoSize(true);
		$objsheet->getColumnDimension('Q')->setAutoSize(true);
		$objsheet->getColumnDimension('R')->setAutoSize(true);
		$objsheet->getColumnDimension('S')->setAutoSize(true);
		$objsheet->getColumnDimension('T')->setAutoSize(true);
		$objsheet->getColumnDimension('U')->setAutoSize(true);
		$objsheet->getColumnDimension('V')->setAutoSize(true);
		$objsheet->getColumnDimension('W')->setAutoSize(true);
		$objsheet->getColumnDimension('X')->setAutoSize(true);
		$objsheet->getColumnDimension('Y')->setAutoSize(true);
		$objsheet->getColumnDimension('Z')->setAutoSize(true);
		$objsheet->getColumnDimension('AA')->setAutoSize(true);
		$objsheet->getColumnDimension('AB')->setAutoSize(true);

        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data Hasil Survey_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');

	}

	public function exportExcelOSS(){

		$query = $this->db->query('SELECT
			A."NAMA_PEMOHON",
			A."ALAMAT_PEMOHON_KABKOT",
			A."ALAMAT_PEMOHON_JALAN",
			A."ALAMAT_PEMOHON_NO",
			A."ALAMAT_PEMOHON_RT",
			A."ALAMAT_PEMOHON_RW",
			A."LOKASI_PERSIL_KEC",
			A."LOKASI_PERSIL_KEL",
			A."LOKASI_PERSIL_JALAN",
			A."LOKASI_PERSIL_NO",
			A."LOKASI_PERSIL_RT",
			A."LOKASI_PERSIL_RW",
			A."LUAS_TANAH",
			A."LUAS_BANGUNAN",
			A."JUMLAH_TINGKAT",
			A."PERUNTUKKAN",
			A."FUNGSI",
			A."ID"
			FROM
			oss_imb A ');		  
 
        if(!$query)
            return false;
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objsheet = $objPHPExcel->setActiveSheetIndex(0);

        $objsheet->setTitle('Data OSS');

        $objsheet->getStyle('A5:R5')->getFont()->setBold(true)->setSize(12);

        $objPHPExcel->getActiveSheet()->getStyle(
    	'A5:R5'
		)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

		$objsheet->getCell('A5')->setValue('NAMA PEMOHON');
		$objsheet->getCell('B5')->setValue('ALAMAT PEMOHON KABUPATEN/KOTA');
		$objsheet->getCell('C5')->setValue('ALAMAT PEMOHON JALAN');
		$objsheet->getCell('D5')->setValue('NO ALAMAT PEMOHON');
		$objsheet->getCell('E5')->setValue('RT ALAMAT PEMOHON');
		$objsheet->getCell('F5')->setValue('RW ALAMAT PEMOHON');
		$objsheet->getCell('G5')->setValue('KECAMATAN ALAMAT PERSIL');
		$objsheet->getCell('H5')->setValue('KELURAHAN ALAMAT PERSIL');
		$objsheet->getCell('I5')->setValue('JALAN LOKASI PERSIL');
		$objsheet->getCell('J5')->setValue('NO LOKASI PERSIL');
		$objsheet->getCell('K5')->setValue('RT LOKASI PERSIL');
		$objsheet->getCell('L5')->setValue('RW LOKASI PERSIL');
		$objsheet->getCell('M5')->setValue('LUAS TANAH');
		$objsheet->getCell('N5')->setValue('LUAS BANGUNAN');
		$objsheet->getCell('O5')->setValue('JUMLAH TINGKAT');
		$objsheet->getCell('P5')->setValue('PERUNTUKKAN');
		$objsheet->getCell('Q5')->setValue('FUNGSI');
		$objsheet->getCell('R5')->setValue('ID');

		$objsheet->getColumnDimension('A')->setAutoSize(true);
		$objsheet->getColumnDimension('B')->setAutoSize(true);
		$objsheet->getColumnDimension('C')->setAutoSize(true);
		$objsheet->getColumnDimension('D')->setAutoSize(true);
		$objsheet->getColumnDimension('E')->setAutoSize(true);
		$objsheet->getColumnDimension('F')->setAutoSize(true);
		$objsheet->getColumnDimension('G')->setAutoSize(true);
		$objsheet->getColumnDimension('H')->setAutoSize(true);
		$objsheet->getColumnDimension('I')->setAutoSize(true);
		$objsheet->getColumnDimension('J')->setAutoSize(true);
		$objsheet->getColumnDimension('K')->setAutoSize(true);
		$objsheet->getColumnDimension('L')->setAutoSize(true);
		$objsheet->getColumnDimension('M')->setAutoSize(true);
		$objsheet->getColumnDimension('N')->setAutoSize(true);
		$objsheet->getColumnDimension('O')->setAutoSize(true);
		$objsheet->getColumnDimension('P')->setAutoSize(true);
		$objsheet->getColumnDimension('Q')->setAutoSize(true);
		$objsheet->getColumnDimension('R')->setAutoSize(true);

        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data_OSS_IMB'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');

	}

	public function exportexcelbyID(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : Export data ke format excel berdasarkan ID yang dipilih	
		*/

		$kode_transaksi=$this->uri->segment(3);

		$query = $this->db->query("SELECT DISTINCT
			A.id_survey,
			A.tgl_survey,
			A.kode_transaksi,
			B.nama_lengkap,
			A.no_surat_perintah,
			C.nama_pemilik,
			C.alamat_persil,
			C.nama_perusahaan,
			I.nama_kec,
			J.nama_kel,
			D.nama_kat,
			K.nama_jenis,
			C.nomor_izin,
			C.tanggal_izin,
			E.nama_peruntukkan,
			C.luas_persil,
			C.no_persil,
			C.jumlah_tingkat,
			C.rt,
			C.rw,
			C.luas_bangunan,
			C.status_ketemu_pemilik,
			L.nilai_piutang,
			L.tgl_skrd,
			L.nilai_piutang_denda,
			M.nama_status_piutang,
			L.alasan_blm_bayar,
			N.directory
			FROM
			tr_survey A
			LEFT JOIN sis_pengguna B ON A.id_user=B.id
			LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
			LEFT JOIN ref_kategori D ON A.id_kategori=D.id_kategori
			LEFT JOIN ref_peruntukkan E ON C.id_peruntukkan=E.id_peruntukkan
			LEFT JOIN tr_foto F ON A.kode_transaksi=F.kode_transaksi
			LEFT JOIN tr_piutang_retribusi G ON A.kode_transaksi=G.kode_transaksi
			LEFT JOIN ref_status_piutang H ON G.id_status_piutang=H.id_status_piutang
			LEFT JOIN ref_kecamatan I ON C.kode_kecamatan=I.kode_kecamatan
			LEFT JOIN ref_kelurahan J ON C.kode_kelurahan=J.kode_kelurahan AND C.kode_kecamatan=J.kode_kecamatan
			LEFT JOIN ref_jenis_izin K ON C.id_jenis_izin=K.id_jenis_izin
			LEFT JOIN tr_piutang_retribusi L ON A.id_survey=L.id_survey
			LEFT JOIN ref_status_piutang M ON L.id_status_piutang=M.id_status_piutang
			LEFT JOIN tr_foto N ON A.id_survey=N.id_survey
			WHERE
			C.kode_transaksi='".$kode_transaksi."' ");		  
 
        if(!$query)
            return false;
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objsheet = $objPHPExcel->setActiveSheetIndex(0);

        $objsheet->setTitle('Data Hasil Survey');

        $objsheet->getStyle('A5:AB5')->getFont()->setBold(true)->setSize(12);

        $rowCount = 1;
		$customTitle = array ('test a');
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $customTitle[0]);

		$objsheet->getCell('A5')->setValue('ID Survey');
		$objsheet->getCell('B5')->setValue('Tanggal Survey');
		$objsheet->getCell('C5')->setValue('Kode Transaksi');
		$objsheet->getCell('D5')->setValue('Nama Surveyor');
		$objsheet->getCell('E5')->setValue('Nomor Surat Perintah');
		$objsheet->getCell('F5')->setValue('Nama Pemilik');
		$objsheet->getCell('G5')->setValue('Alamat Pemilik');
		$objsheet->getCell('H5')->setValue('Nama Perusahaan');
		$objsheet->getCell('I5')->setValue('Nama Kecamatan');
		$objsheet->getCell('J5')->setValue('Nama Kelurahan');
		$objsheet->getCell('K5')->setValue('Kategori');
		$objsheet->getCell('L5')->setValue('Jenis Izin');
		$objsheet->getCell('M5')->setValue('Nomor Izin');
		$objsheet->getCell('N5')->setValue('Tanggal Izin');
		$objsheet->getCell('O5')->setValue('Peruntukkan Bangunan');
		$objsheet->getCell('P5')->setValue('Luar Persil');
		$objsheet->getCell('Q5')->setValue('Nomor Persil');
		$objsheet->getCell('R5')->setValue('Jumlah Tingkat');
		$objsheet->getCell('S5')->setValue('RT');
		$objsheet->getCell('T5')->setValue('RW');
		$objsheet->getCell('U5')->setValue('Luas Bangunan');
		$objsheet->getCell('V5')->setValue('Status Ketemu Pemilik');
		$objsheet->getCell('W5')->setValue('Nilai Piutang');
		$objsheet->getCell('X5')->setValue('Tanggal SKRD');
		$objsheet->getCell('Y5')->setValue('Nilai Piutang Denda');
		$objsheet->getCell('Z5')->setValue('Status Piutang');
		$objsheet->getCell('AA5')->setValue('Alasan Belum Bayar');
		$objsheet->getCell('AB5')->setValue('Lokasi Foto');

		$objsheet->getColumnDimension('A')->setAutoSize(true);
		$objsheet->getColumnDimension('B')->setAutoSize(true);
		$objsheet->getColumnDimension('C')->setAutoSize(true);
		$objsheet->getColumnDimension('D')->setAutoSize(true);
		$objsheet->getColumnDimension('E')->setAutoSize(true);
		$objsheet->getColumnDimension('F')->setAutoSize(true);
		$objsheet->getColumnDimension('G')->setAutoSize(true);
		$objsheet->getColumnDimension('H')->setAutoSize(true);
		$objsheet->getColumnDimension('I')->setAutoSize(true);
		$objsheet->getColumnDimension('J')->setAutoSize(true);
		$objsheet->getColumnDimension('K')->setAutoSize(true);
		$objsheet->getColumnDimension('L')->setAutoSize(true);
		$objsheet->getColumnDimension('M')->setAutoSize(true);
		$objsheet->getColumnDimension('N')->setAutoSize(true);
		$objsheet->getColumnDimension('O')->setAutoSize(true);
		$objsheet->getColumnDimension('P')->setAutoSize(true);
		$objsheet->getColumnDimension('Q')->setAutoSize(true);
		$objsheet->getColumnDimension('R')->setAutoSize(true);
		$objsheet->getColumnDimension('S')->setAutoSize(true);
		$objsheet->getColumnDimension('T')->setAutoSize(true);
		$objsheet->getColumnDimension('U')->setAutoSize(true);
		$objsheet->getColumnDimension('V')->setAutoSize(true);
		$objsheet->getColumnDimension('W')->setAutoSize(true);
		$objsheet->getColumnDimension('X')->setAutoSize(true);
		$objsheet->getColumnDimension('Y')->setAutoSize(true);
		$objsheet->getColumnDimension('Z')->setAutoSize(true);
		$objsheet->getColumnDimension('AA')->setAutoSize(true);
		$objsheet->getColumnDimension('AB')->setAutoSize(true);

        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data Hasil Survey_'.date('dMy').'_'.$kode_transaksi.'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');

	}

	public function exportexcelPotensiAll(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : export semua data menu potensi ke dalam bentuk file excel	
		*/

		$id_kategori=$this->uri->segment(3);

		$query = $this->db->query("SELECT  
			C.nama_pemilik,
			C.alamat_persil,
			C.rt,
			C.rw,
			E.nama_kat,
			M.nama_peruntukkan,
			C.luas_bangunan, 
			N.nama_param,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah) * sum(N.pers_netto_berizin) / 100) as neto
			from tr_detail_survey C
			LEFT JOIN tr_survey A ON A.id_survey=C.id_survey
			LEFT JOIN ref_kecamatan B ON B.kode_kecamatan=C.kode_kecamatan 
			LEFT JOIN ref_kelurahan D ON D.kode_kelurahan=C.kode_kelurahan 
			LEFT JOIN ref_kategori E ON A.id_kategori=E.id_kategori
			LEFT JOIN ref_peruntukkan M ON C.id_peruntukkan=M.id_peruntukkan 
			LEFT JOIN m_parameter_potensi N ON M.id_param=N.id_param
			where A.status_transaksi='1'
			and C.luas_bangunan not like '%-%'
			GROUP BY A.tgl_survey, 
			C.alamat_persil, 
			C.kode_transaksi, 
			C.luas_bangunan, 
			C.nama_pemilik, 
			C.status_ketemu_pemilik, 
			A.status_transaksi, 
			M.nama_peruntukkan, 
			C.luas_bangunan, 
			N.nama_param, 
			E.nama_kat,
			C.rt,
			C.rw");		  
 
        if(!$query)
            return false;
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objsheet = $objPHPExcel->setActiveSheetIndex(0);

        $currentDate = date("j/n/Y");

        $objsheet->setTitle('Daftar Potensi');
        $customTitle = array ('Print on '.$currentDate);
        $objsheet->getStyle('A1')->getFont()->setBold(true)->setSize(12);
        $objsheet->getColumnDimension('A')->setAutoSize(true);

        $customTitle = array ('DAFTAR POTENSI RETRIBUSI BPPT KOTA BANDUNG');

        $objsheet->getStyle('A3')->getFont()->setBold(true)->setSize(12);
        $objsheet->getColumnDimension('A')->setAutoSize(true);

        $objsheet->getStyle('A5:J5')->getFont()->setBold(true)->setSize(12);

        $rowCount = 1;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $customTitle[0]);

		$objsheet->getCell('A5')->setValue('NAMA PEMILIK');
		$objsheet->getCell('B5')->setValue('ALAMAT PERSIL');
		$objsheet->getCell('C5')->setValue('RT');
		$objsheet->getCell('D5')->setValue('RW');
		$objsheet->getCell('E5')->setValue('KATEGORI IZIN');
		$objsheet->getCell('F5')->setValue('PERUNTUKKAN');
		$objsheet->getCell('G5')->setValue('LUAS BANGUNAN');
		$objsheet->getCell('H5')->setValue('JENIS IZIN');
		$objsheet->getCell('I5')->setValue('BRUTTO');
		$objsheet->getCell('J5')->setValue('NETTO');

		$objsheet->getColumnDimension('A')->setAutoSize(true);
		$objsheet->getColumnDimension('B')->setAutoSize(true);
		$objsheet->getColumnDimension('C')->setAutoSize(true);
		$objsheet->getColumnDimension('D')->setAutoSize(true);
		$objsheet->getColumnDimension('E')->setAutoSize(true);
		$objsheet->getColumnDimension('F')->setAutoSize(true);
		$objsheet->getColumnDimension('G')->setAutoSize(true);
		$objsheet->getColumnDimension('H')->setAutoSize(true);
		$objsheet->getColumnDimension('I')->setAutoSize(true);
		$objsheet->getColumnDimension('J')->setAutoSize(true);

        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
                
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);

        
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

 
        // Sending headers to force the user to download the file        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Daftar Potensi Retribusi BPPT Kota Bandung_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');
	}


	public function exportexcelPotensiBerizin(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : export data potensi dengan wilayah berizin ke dalam bentuk file excel	
		*/

		$id_kategori	=$this->input->post('id_kategori');

		$query = $this->db->query("SELECT  
			C.nama_pemilik,
			C.alamat_persil,
			C.rt,
			C.rw,
			M.nama_peruntukkan,
			C.luas_bangunan, 
			N.nama_param,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(12,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah) * sum(N.pers_netto_berizin) / 100) as neto
			from tr_detail_survey C
			LEFT JOIN tr_survey A ON A.id_survey=C.id_survey
			LEFT JOIN ref_kecamatan B ON B.kode_kecamatan=C.kode_kecamatan 
			LEFT JOIN ref_kelurahan D ON D.kode_kelurahan=C.kode_kelurahan 
			LEFT JOIN ref_kategori E ON A.id_kategori=E.id_kategori
			LEFT JOIN ref_peruntukkan M ON C.id_peruntukkan=M.id_peruntukkan 
			LEFT JOIN m_parameter_potensi N ON M.id_param=N.id_param
			where C.kat_izin= 1 
			and A.status_transaksi='1'
			and C.luas_bangunan not like '%-%'
			GROUP BY A.tgl_survey, 
			C.alamat_persil, 
			C.kode_transaksi, 
			C.luas_bangunan, 
			C.nama_pemilik, 
			C.status_ketemu_pemilik, 
			A.status_transaksi, 
			M.nama_peruntukkan, 
			C.luas_bangunan, 
			N.nama_param, 
			E.nama_kat,
			C.rt,
			C.rw");		  
 
        if(!$query)
            return false;
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objsheet = $objPHPExcel->setActiveSheetIndex(0);

        $currentDate = date("j/n/Y");

        $objsheet->setTitle('Daftar Potensi');
        $customTitle = array ('Print on '.$currentDate);

        $objPHPExcel->getActiveSheet()->setCellValue('A2', "KATEGORI : BERIZIN");
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);

	    $objPHPExcel->getActiveSheet()->setCellValue('B3', "DAFTAR POTENSI RETRIBUSI BPPT KOTA BANDUNG");
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setWrapText(true);

		$objsheet->getStyle('B3:E3')->getFont()->setBold(true)->setSize(20);
        $objsheet->getColumnDimension('B')->setAutoSize(true);

        $objsheet->getStyle('A5:I5')->getFont()->setBold(true)->setSize(12);

        $rowCount = 1;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $customTitle[0]);

		$objsheet->getCell('A5')->setValue('NAMA PEMILIK');
		$objsheet->getCell('B5')->setValue('ALAMAT PERSIL');
		$objsheet->getCell('C5')->setValue('RT');
		$objsheet->getCell('D5')->setValue('RW');
		$objsheet->getCell('E5')->setValue('PERUNTUKKAN');
		$objsheet->getCell('F5')->setValue('LUAS BANGUNAN');
		$objsheet->getCell('G5')->setValue('JENIS IZIN');
		$objsheet->getCell('H5')->setValue('BRUTTO');
		$objsheet->getCell('I5')->setValue('NETTO');

		$objsheet->getColumnDimension('A')->setAutoSize(true);
		$objsheet->getColumnDimension('B')->setAutoSize(true);
		$objsheet->getColumnDimension('C')->setAutoSize(true);
		$objsheet->getColumnDimension('D')->setAutoSize(true);
		$objsheet->getColumnDimension('E')->setAutoSize(true);
		$objsheet->getColumnDimension('F')->setAutoSize(true);
		$objsheet->getColumnDimension('G')->setAutoSize(true);
		$objsheet->getColumnDimension('H')->setAutoSize(true);
		$objsheet->getColumnDimension('I')->setAutoSize(true);

        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Daftar Potensi Retribusi BPPT Kota Bandung_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');

	}

	function exportexcelPotensiTidakBerizin(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : export data potensi dengan wilayah tidak berizin ke dalam bentuk file excel	
		*/

		$id_kategori = $this->input->post('id_kategori');

		$query = $this->db->query("SELECT  
			C.nama_pemilik,
			C.alamat_persil,
			C.rt,
			C.rw,
			M.nama_peruntukkan,
			C.luas_bangunan, 
			N.nama_param,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as neto
			from tr_detail_survey C
			LEFT JOIN tr_survey A ON A.id_survey=C.id_survey
			LEFT JOIN ref_kecamatan B ON B.kode_kecamatan=C.kode_kecamatan 
			LEFT JOIN ref_kelurahan D ON D.kode_kelurahan=C.kode_kelurahan 
			LEFT JOIN ref_kategori E ON A.id_kategori=E.id_kategori
			LEFT JOIN ref_peruntukkan M ON C.id_peruntukkan=M.id_peruntukkan 
			LEFT JOIN m_parameter_potensi N ON M.id_param=N.id_param
			where C.kat_izin= 2 
			and A.status_transaksi='1'
			and C.luas_bangunan not like '%-%'
			GROUP BY A.tgl_survey, 
			C.alamat_persil, 
			C.kode_transaksi, 
			C.luas_bangunan, 
			C.nama_pemilik, 
			C.status_ketemu_pemilik, 
			A.status_transaksi, 
			M.nama_peruntukkan, 
			C.luas_bangunan, 
			N.nama_param, 
			E.nama_kat,
			C.rt,
			C.rw");		  
 
        if(!$query)
            return false;
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objsheet = $objPHPExcel->setActiveSheetIndex(0);

        $currentDate = date("j/n/Y");

        $objsheet->setTitle('Daftar Potensi');
        $customTitle = array ('Print on '.$currentDate);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "KATEGORI : TIDAK BERIZIN");
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);

	    $objPHPExcel->getActiveSheet()->setCellValue('B3', "DAFTAR POTENSI RETRIBUSI BPPT KOTA BANDUNG");
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setWrapText(true);

		$objsheet->getStyle('B3:E3')->getFont()->setBold(true)->setSize(20);
        $objsheet->getColumnDimension('B')->setAutoSize(true);

        $objsheet->getStyle('A5:I5')->getFont()->setBold(true)->setSize(12);

        $rowCount = 1;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $customTitle[0]);

		$objsheet->getCell('A5')->setValue('NAMA PEMILIK');
		$objsheet->getCell('B5')->setValue('ALAMAT PERSIL');
		$objsheet->getCell('C5')->setValue('RT');
		$objsheet->getCell('D5')->setValue('RW');
		$objsheet->getCell('E5')->setValue('PERUNTUKKAN');
		$objsheet->getCell('F5')->setValue('LUAS BANGUNAN');
		$objsheet->getCell('G5')->setValue('JENIS IZIN');
		$objsheet->getCell('H5')->setValue('BRUTTO');
		$objsheet->getCell('I5')->setValue('NETTO');

		$objsheet->getColumnDimension('A')->setAutoSize(true);
		$objsheet->getColumnDimension('B')->setAutoSize(true);
		$objsheet->getColumnDimension('C')->setAutoSize(true);
		$objsheet->getColumnDimension('D')->setAutoSize(true);
		$objsheet->getColumnDimension('E')->setAutoSize(true);
		$objsheet->getColumnDimension('F')->setAutoSize(true);
		$objsheet->getColumnDimension('G')->setAutoSize(true);
		$objsheet->getColumnDimension('H')->setAutoSize(true);
		$objsheet->getColumnDimension('I')->setAutoSize(true);

        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Daftar Potensi Retribusi BPPT Kota Bandung_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');

	}

	function exportexcelPotensiLahanKosong(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : export data potensi dengan wilayah lahan kosong ke dalam bentuk file excel	
		*/

		$id_kategori = $this->input->post('id_kategori');

		$query = $this->db->query("SELECT  
			C.nama_pemilik,
			C.alamat_persil,
			C.rt,
			C.rw,
			E.nama_kat,
			M.nama_peruntukkan,
			C.luas_bangunan, 
			N.nama_param,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as bruto,
			((sum(cast(replace(C.luas_bangunan, ',', '.') as decimal(18,2)))) * sum(N.nilai_koefisien) * sum(N.nilai_rupiah)) as neto
			from tr_detail_survey C
			LEFT JOIN tr_survey A ON A.id_survey=C.id_survey
			LEFT JOIN ref_kecamatan B ON B.kode_kecamatan=C.kode_kecamatan 
			LEFT JOIN ref_kelurahan D ON D.kode_kelurahan=C.kode_kelurahan 
			LEFT JOIN ref_kategori E ON A.id_kategori=E.id_kategori
			LEFT JOIN ref_peruntukkan M ON C.id_peruntukkan=M.id_peruntukkan 
			LEFT JOIN m_parameter_potensi N ON M.id_param=N.id_param
			where C.kat_izin= 3 
			and A.status_transaksi='1'
			and C.luas_bangunan not like '%-%'
			GROUP BY A.tgl_survey, 
			C.alamat_persil, 
			C.kode_transaksi, 
			C.luas_bangunan, 
			C.nama_pemilik, 
			C.status_ketemu_pemilik, 
			A.status_transaksi, 
			M.nama_peruntukkan, 
			C.luas_bangunan, 
			N.nama_param, 
			E.nama_kat,
			C.rt,
			C.rw");		  
 
        if(!$query)
            return false;
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objsheet = $objPHPExcel->setActiveSheetIndex(0);

        $currentDate = date("j/n/Y");

        $objsheet->setTitle('Daftar Potensi');
        $customTitle = array ('Print on '.$currentDate);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "KATEGORI : LAHAN KOSONG");
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);

	    $objPHPExcel->getActiveSheet()->setCellValue('B3', "DAFTAR POTENSI RETRIBUSI BPPT KOTA BANDUNG");
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setWrapText(true);

		$objsheet->getStyle('B3:E3')->getFont()->setBold(true)->setSize(20);
        $objsheet->getColumnDimension('B')->setAutoSize(true);

        $objsheet->getStyle('A5:K5')->getFont()->setBold(true)->setSize(12);

        $rowCount = 1;
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $customTitle[0]);

		$objsheet->getCell('A5')->setValue('NAMA PEMILIK');
		$objsheet->getCell('B5')->setValue('ALAMAT PERSIL');
		$objsheet->getCell('C5')->setValue('RT');
		$objsheet->getCell('D5')->setValue('RW');
		$objsheet->getCell('E5')->setValue('KATEGORI IZIN');
		$objsheet->getCell('F5')->setValue('PERUNTUKKAN');
		$objsheet->getCell('G5')->setValue('LUAS BANGUNAN');
		$objsheet->getCell('H5')->setValue('JENIS IZIN');
		$objsheet->getCell('I5')->setValue('BRUTO');

		$objsheet->getColumnDimension('A')->setAutoSize(true);
		$objsheet->getColumnDimension('B')->setAutoSize(true);
		$objsheet->getColumnDimension('C')->setAutoSize(true);
		$objsheet->getColumnDimension('D')->setAutoSize(true);
		$objsheet->getColumnDimension('E')->setAutoSize(true);
		$objsheet->getColumnDimension('F')->setAutoSize(true);
		$objsheet->getColumnDimension('G')->setAutoSize(true);
		$objsheet->getColumnDimension('H')->setAutoSize(true);
		$objsheet->getColumnDimension('I')->setAutoSize(true);

        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 10, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 6;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Daftar Potensi Retribusi BPPT Kota Bandung_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');

	}

	public function exportpdfbyID(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : export data survey  ke dalam bentuk file pdf berdasarkan kode transaksi yang dipilih
		*/

		$query = $this->db->query('SELECT DISTINCT
			
			C.kode_transaksi,
			A.tgl_survey,
			B.nama_lengkap,
			A.no_surat_perintah
			FROM
			tr_survey A
			LEFT JOIN sis_pengguna B ON A.id_user=B.id
			LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
			');

		$data = array();

		foreach($query->result() as $row ){
           array_push($data, $row);
        } 
		 
		#setting judul laporan dan header tabel
		$judul1 = "LAPORAN DETAIL HASIL SURVEY";
		$judul2 = "POTENSI RETRIBUSI PERIZINAN";
		$judul3 = "BPPT KOTA BANDUNG";
		$line 	= "___________________________________________________________";
		$currentDate = date("j/n/Y");
		
		#sertakan library FPDF dan bentuk objek
		$pdf = new FPDF();
		$pdf->AddPage('P', 'A4');
		$pdf->SetMargins(18,40,30,30);
		$pdf->SetXY(20, 30);		 
		#tampilkan judul laporan

		$pdf->Image('logopotret.png',15,15,26);


		$pdf->SetMargins(18,100,30,30);
		$pdf->SetXY(20, 270);
		$pdf->SetFont('Arial','I','9');
		$pdf->Cell(0,5, 'print on: '.$currentDate, '0', 1, 'L');

		$pdf->SetMargins(20,20,30,30);
		$pdf->SetXY(20, 20);

		$pdf->SetFont('Arial','B','16');
		$pdf->Cell(10);		
		$pdf->Cell(0,0, $judul1, '0', 1, 'C');
		$pdf->SetFont('Arial','B','16');
		$pdf->Cell(10);		
		$pdf->Cell(0,15, $judul2, '0', 1, 'C');
		$pdf->Cell(10);		
		$pdf->Cell(0,0, $judul3, '0', 1, 'C');
		$pdf->Cell(5);		
		$pdf->Cell(0,9, $line, '0', 1, 'C');
		
		$pdf->Ln();

		$kode_transaksi = $this->uri->segment(3);

		$query2 = $this->db->query("SELECT DISTINCT
			C.kode_transaksi,
			A.tgl_survey,
			D.nama_lengkap,
			A.no_surat_perintah
			FROM
			tr_survey A
			LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
			LEFT JOIN sis_pengguna D ON A.id_user=D.id
			WHERE C.kode_transaksi = '".$kode_transaksi."'
			");

		$data2 = array();

		foreach($query2->result() as $row2 ){
           array_push($data2, $row2);
        } 

		$header2 = array(
				array("label"=>"Kode Survey               :", "length"=>20, "align"=>"L"),
				array("label"=>"Tgl. Survey                 :", "length"=>35, "align"=>"L"),
				array("label"=>"Petugas Survey          :", "length"=>40, "align"=>"L"),
				array("label"=>"Nomor Surat Perintah :", "length"=>30, "align"=>"L")
			);

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(12, 55);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,20, 'DATA HASIL SURVEY', '0', 1, 'L');

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(20, 70);
		$pdf->SetFont('Arial','','10');
		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		// $pdf->SetDrawColor(0,0,0);
		// $pdf->Cell(-1);
		foreach ($header2 as $kolom2) {
			$pdf->Cell(-8);
			$pdf->Cell($kolom2['length'], 10, $kolom2['label'], 0, '-10', $kolom2['align'], true);
			$pdf->Ln();
		}
		$pdf->Ln();

		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
		$fill=false;


		$pdf->SetMargins(70,40,30,30);
		$pdf->SetXY(50, 70);
		foreach ($data2 as $baris2) {
		$i = 0;
		$pdf->Cell(-1);
		foreach ($baris2 as $cell2) {
			// $pdf->Cell(-1);
			$pdf->SetX(50);
			$pdf->Cell($header2[$i]['length'], 10, $cell2, 0, '-10', $kolom2['align'], $fill);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();

		}
		
		 
		$query3 = $this->db->query("SELECT 
					A.nilai_piutang,
					A.tgl_skrd,
					A.nilai_piutang_denda,
					B.nama_status_piutang,
					A.alasan_blm_bayar
					FROM
					tr_piutang_retribusi A, ref_status_piutang B
					WHERE
					A.id_status_piutang=B.id_status_piutang AND
					A.kode_transaksi='".$kode_transaksi."' ");

		$cek=$query3->num_rows(); 

		$data3 = array();

		if ($cek != 0 ) {

		foreach($query3->result() as $row3 ){
           array_push($data3, $row3);
        } 

		$header3 = array(
				array("label"=>"Nilai Piutang Retribusi :", "length"=>20, "align"=>"L"),
				array("label"=>"Tgl. SKRD                   :", "length"=>35, "align"=>"L"),
				array("label"=>"Nilai Piutang Denda    :", "length"=>40, "align"=>"L"),
				array("label"=>"Status Piutang            :", "length"=>30, "align"=>"L"),
				array("label"=>"Alasan Belum Bayar   :", "length"=>30, "align"=>"L")
			);

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(12, 107);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,20, 'DATA PIUTANG RETRIBUSI', '0', 1, 'L');

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(20, 122);
		$pdf->SetFont('Arial','','10');
		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		// $pdf->SetDrawColor(0,0,0);
		// $pdf->Cell(-1);
		foreach ($header3 as $kolom3) {
			$pdf->Cell(-8);
			$pdf->Cell($kolom3['length'], 10, $kolom3['label'], 0, '-10', $kolom3['align'], true);
			$pdf->Ln();
		}
		$pdf->Ln();

		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
		$fill=false;


		$pdf->SetMargins(70,40,30,30);
		$pdf->SetXY(50, 122);
		foreach ($data3 as $baris3) {
		$i = 0;
		$pdf->Cell(-1);
		foreach ($baris3 as $cell3) {
			// $pdf->Cell(-1);
			$pdf->SetX(50);
			$pdf->Cell($header3[$i]['length'], 10, $cell3, 0, '-10', $kolom3['align'], $fill);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();


		}

				$pdf->SetMargins(20,120,30,30);
		$pdf->SetXY(12, 178);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,0, 'DATA FOTO', '0', 1, 'L');

		$pdf->Ln();

		$query6 = $this->db->query("SELECT 
					A.directory
					FROM tr_foto A WHERE A.kode_transaksi='".$kode_transaksi."' ");

		$data6 = array();

		$imagenurl = "assets/uploads/";


			foreach($query6->result() as $row6 ){
           array_push($data6, $row6);
          
        } 

        $pdf->SetMargins(160,40,30,30);
		$pdf->SetXY(12, 185);
		
		foreach ($data6 as $baris6) {
		$i = 0;
		$pdf->Cell(-1);

		foreach ($baris6 as $cell6) {
			// $pdf->Cell(-1);
			 $filename = $cell6;
			 $filepath = $imagenurl.$filename;
			$pdf->SetX(130);
			$pdf->Image($filepath,14,187,50);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();

		}

	}else{

		$pdf->SetMargins(20,115,30,30);
		$pdf->SetXY(12, 120);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,0, 'DATA FOTO', '0', 1, 'L');

		$pdf->Ln();

		$query6 = $this->db->query("SELECT 
					A.directory
					FROM tr_foto A WHERE A.kode_transaksi='".$kode_transaksi."' ");

		$data6 = array();

		$imagenurl = "assets/uploads/";


			foreach($query6->result() as $row6 ){
           array_push($data6, $row6);
          
        } 

        $pdf->SetMargins(160,40,30,30);
		$pdf->SetXY(12, 185);
		
		foreach ($data6 as $baris6) {
		$i = 0;
		$pdf->Cell(-1);

		foreach ($baris6 as $cell6) {
			// $pdf->Cell(-1);
			 $filename = $cell6;
			 $filepath = $imagenurl.$filename;
			$pdf->SetX(130);
			$pdf->Image($filepath,14,130,50);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();

		}

	}

		$query5 = $this->db->query("SELECT 
				A.nama_pemilik,
				A.alamat_persil,
				A.nama_perusahaan,
				B.nama_kec,
				C.nama_kel,
				D.nama_kat,
				F.nama_jenis,
				A.nomor_izin,
				A.tanggal_izin,
				E.nama_peruntukkan,
				A.luas_persil,
				A.no_persil,
				A.jumlah_tingkat,
				A.rt,
				A.rw,
				A.luas_bangunan,
				A.status_ketemu_pemilik
				FROM
				tr_detail_survey A
				LEFT JOIN ref_kategori D ON A.kat_izin=D.id_kategori
				LEFT JOIN ref_peruntukkan E ON A.id_peruntukkan=E.id_peruntukkan
				LEFT JOIN ref_kecamatan B ON A.kode_kecamatan=B.kode_kecamatan
				LEFT JOIN ref_jenis_izin F ON A.id_jenis_izin=F.id_jenis_izin
				LEFT JOIN ref_kelurahan C ON A.kode_kelurahan=C.kode_kelurahan AND A.kode_kecamatan=C.kode_kecamatan 
				WHERE
				A.kode_transaksi='".$kode_transaksi."' ");

		$data5 = array();

		foreach($query5->result() as $row5 ){
           array_push($data5, $row5);
        } 

		$header5 = array(
				array("label"=>"Nama Pemilik                 :", "length"=>20, "align"=>"L"),
				array("label"=>"Alamat Pemilik               :", "length"=>35, "align"=>"L"),
				array("label"=>"Nama Perusahaan         :", "length"=>40, "align"=>"L"),
				array("label"=>"Kecamatan                     :", "length"=>30, "align"=>"L"),
				array("label"=>"Kelurahan                      :", "length"=>30, "align"=>"L"),
				array("label"=>"Kategori                         :", "length"=>35, "align"=>"L"),
				array("label"=>"Jenis Izin                        :", "length"=>40, "align"=>"L"),
				array("label"=>"Nomor Izin                      :", "length"=>30, "align"=>"L"),
				array("label"=>"Tanggal Izin                   :", "length"=>30, "align"=>"L"),
				array("label"=>"Peruntukkan Bangunan :", "length"=>35, "align"=>"L"),
				array("label"=>"Luas Persil                    :", "length"=>40, "align"=>"L"),
				array("label"=>"Nomor Persil                  :", "length"=>30, "align"=>"L"),
				array("label"=>"Jumlah Tingakt              :", "length"=>30, "align"=>"L"),
				array("label"=>"RT                                 :", "length"=>35, "align"=>"L"),
				array("label"=>"RW                                :", "length"=>40, "align"=>"L"),
				array("label"=>"Luas Bangunan             :", "length"=>30, "align"=>"L"),
				array("label"=>"Status Ketemu Pemilik  :", "length"=>30, "align"=>"L")
			);

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(90, 55);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,20, 'DATA PEMILIK', '0', 1, 'L');

		$pdf->SetMargins(100,40,30,30);
		$pdf->SetXY(100, 70);
		$pdf->SetFont('Arial','','10');
		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		// $pdf->SetDrawColor(0,0,0);
		// $pdf->Cell(-1);
		foreach ($header5 as $kolom5) {
			$pdf->Cell(-10);
			$pdf->Cell($kolom5['length'], 10, $kolom5['label'], 0, '-10', $kolom5['align'], true);
			$pdf->Ln();
		}
		$pdf->Ln();

		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
		$fill=false;


		$pdf->SetMargins(160,40,30,30);
		$pdf->SetXY(10, 70);
		foreach ($data5 as $baris5) {
		$i = 0;
		$pdf->Cell(-1);
		foreach ($baris5 as $cell5) {
			// $pdf->Cell(-1);
			$pdf->SetX(130);
			$pdf->Cell($header5[$i]['length'], 10, $cell5, 0, '-10', $kolom5['align'], $fill);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();


		}

		$pdf->SetMargins(100,220,30,30);
		$pdf->SetXY(135, 263);

		$pdf->SetFont('Arial','I','9');
		$pdf->Cell(0,0, 'Status Ketemu pemilik = 0 (tidak ketemu)', '0', 1, 'L');
		$pdf->SetXY(135, 268);
		$pdf->Cell(0,0, 'Status Ketemu pemilik = 1 (ketemu)', '0', 1, 'L');

		#output file PDF
		$pdf->Output();

	}

	public function exportpdf(){

		/*
		Author   : Nanda Prasetyo
		Modified :
		Date     : 9-Jan-2017
		Desc     : export seluruh data survey  ke dalam bentuk file pdf	
		*/

		$query = $this->db->query('SELECT DISTINCT
			
		C.kode_transaksi,
		A.tgl_survey,
		B.nama_lengkap,
		A.no_surat_perintah
		FROM
		tr_survey A
		LEFT JOIN sis_pengguna B ON A.id_user=B.id
		LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
		');

		$data = array();

		foreach($query->result() as $row ){
           array_push($data, $row);
        } 
		 
		#setting judul laporan dan header tabel
		$judul1 = "LAPORAN DETAIL HASIL SURVEY";
		$judul2 = "POTENSI RETRIBUSI PERIZINAN";
		$judul3 = "BPPT KOTA BANDUNG";
		$line 	= "___________________________________________________________";
		$currentDate = date("j/n/Y");
		
		#sertakan library FPDF dan bentuk objek
		$pdf = new FPDF();
		$pdf->AddPage('P', 'A4');
		$pdf->SetMargins(18,40,30,30);
		$pdf->SetXY(20, 30);		 
		#tampilkan judul laporan

		$pdf->Image('logopotret.png',15,15,26);


		$pdf->SetMargins(18,100,30,30);
		$pdf->SetXY(20, 270);
		$pdf->SetFont('Arial','I','9');
		$pdf->Cell(0,5, 'print on: '.$currentDate, '0', 1, 'L');

		$pdf->SetMargins(20,20,30,30);
		$pdf->SetXY(20, 20);

		$pdf->SetFont('Arial','B','16');
		$pdf->Cell(10);		
		$pdf->Cell(0,0, $judul1, '0', 1, 'C');
		$pdf->SetFont('Arial','B','16');
		$pdf->Cell(10);		
		$pdf->Cell(0,15, $judul2, '0', 1, 'C');
		$pdf->Cell(10);		
		$pdf->Cell(0,0, $judul3, '0', 1, 'C');
		$pdf->Cell(5);		
		$pdf->Cell(0,9, $line, '0', 1, 'C');
		
		$pdf->Ln();

		$kode_transaksi = $this->uri->segment(3);

		$query2 = $this->db->query("SELECT DISTINCT
			C.kode_transaksi,
			A.tgl_survey,
			D.nama_lengkap,
			A.no_surat_perintah
			FROM
			tr_survey A
			LEFT JOIN tr_detail_survey C ON A.kode_transaksi=C.kode_transaksi
			LEFT JOIN sis_pengguna D ON A.id_user=D.id
			WHERE C.kode_transaksi = '".$kode_transaksi."'
			");

		$data2 = array();

		foreach($query2->result() as $row2 ){
           array_push($data2, $row2);
        } 

		$header2 = array(
				array("label"=>"Kode Survey               :", "length"=>20, "align"=>"L"),
				array("label"=>"Tgl. Survey                 :", "length"=>35, "align"=>"L"),
				array("label"=>"Petugas Survey          :", "length"=>40, "align"=>"L"),
				array("label"=>"Nomor Surat Perintah :", "length"=>30, "align"=>"L")
			);

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(12, 55);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,20, 'DATA HASIL SURVEY', '0', 1, 'L');

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(20, 70);
		$pdf->SetFont('Arial','','10');
		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		// $pdf->SetDrawColor(0,0,0);
		// $pdf->Cell(-1);
		foreach ($header2 as $kolom2) {
			$pdf->Cell(-8);
			$pdf->Cell($kolom2['length'], 10, $kolom2['label'], 0, '-10', $kolom2['align'], true);
			$pdf->Ln();
		}
		$pdf->Ln();

		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
		$fill=false;


		$pdf->SetMargins(70,40,30,30);
		$pdf->SetXY(50, 70);
		foreach ($data2 as $baris2) {
		$i = 0;
		$pdf->Cell(-1);
		foreach ($baris2 as $cell2) {
			// $pdf->Cell(-1);
			$pdf->SetX(50);
			$pdf->Cell($header2[$i]['length'], 10, $cell2, 0, '-10', $kolom2['align'], $fill);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();

		}
		
		 
		$query3 = $this->db->query("SELECT 
					A.nilai_piutang,
					A.tgl_skrd,
					A.nilai_piutang_denda,
					B.nama_status_piutang,
					A.alasan_blm_bayar
					FROM
					tr_piutang_retribusi A, ref_status_piutang B
					WHERE
					A.id_status_piutang=B.id_status_piutang AND
					A.kode_transaksi='".$kode_transaksi."' ");

		$cek=$query3->num_rows(); 

		$data3 = array();

		if ($cek != 0 ) {

		foreach($query3->result() as $row3 ){
           array_push($data3, $row3);
        } 

		$header3 = array(
				array("label"=>"Nilai Piutang Retribusi :", "length"=>20, "align"=>"L"),
				array("label"=>"Tgl. SKRD                   :", "length"=>35, "align"=>"L"),
				array("label"=>"Nilai Piutang Denda    :", "length"=>40, "align"=>"L"),
				array("label"=>"Status Piutang            :", "length"=>30, "align"=>"L"),
				array("label"=>"Alasan Belum Bayar   :", "length"=>30, "align"=>"L")
			);

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(12, 107);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,20, 'DATA PIUTANG RETRIBUSI', '0', 1, 'L');

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(20, 122);
		$pdf->SetFont('Arial','','10');
		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		// $pdf->SetDrawColor(0,0,0);
		// $pdf->Cell(-1);
		foreach ($header3 as $kolom3) {
			$pdf->Cell(-8);
			$pdf->Cell($kolom3['length'], 10, $kolom3['label'], 0, '-10', $kolom3['align'], true);
			$pdf->Ln();
		}
		$pdf->Ln();

		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
		$fill=false;


		$pdf->SetMargins(70,40,30,30);
		$pdf->SetXY(50, 122);
		foreach ($data3 as $baris3) {
		$i = 0;
		$pdf->Cell(-1);
		foreach ($baris3 as $cell3) {
			// $pdf->Cell(-1);
			$pdf->SetX(50);
			$pdf->Cell($header3[$i]['length'], 10, $cell3, 0, '-10', $kolom3['align'], $fill);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();


		}

				$pdf->SetMargins(20,120,30,30);
		$pdf->SetXY(12, 178);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,0, 'DATA FOTO', '0', 1, 'L');

		$pdf->Ln();

		$query6 = $this->db->query("SELECT 
					A.directory
					FROM tr_foto A WHERE A.kode_transaksi='".$kode_transaksi."' ");

		$data6 = array();

		$imagenurl = "assets/uploads/";


			foreach($query6->result() as $row6 ){
           array_push($data6, $row6);
          
        } 

        $pdf->SetMargins(160,40,30,30);
		$pdf->SetXY(12, 185);
		
		foreach ($data6 as $baris6) {
		$i = 0;
		$pdf->Cell(-1);

		foreach ($baris6 as $cell6) {
			// $pdf->Cell(-1);
			 $filename = $cell6;
			 $filepath = $imagenurl.$filename;
			$pdf->SetX(130);
			$pdf->Image($filepath,14,187,50);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();

		}

	}else{

		$pdf->SetMargins(20,115,30,30);
		$pdf->SetXY(12, 120);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,0, 'DATA FOTO', '0', 1, 'L');

		$pdf->Ln();

		$query6 = $this->db->query("SELECT 
					A.directory
					FROM tr_foto A WHERE A.kode_transaksi='".$kode_transaksi."' ");

		$data6 = array();

		$imagenurl = "assets/uploads/";


			foreach($query6->result() as $row6 ){
           array_push($data6, $row6);
          
        } 

        $pdf->SetMargins(160,40,30,30);
		$pdf->SetXY(12, 185);
		
		foreach ($data6 as $baris6) {
		$i = 0;
		$pdf->Cell(-1);

		foreach ($baris6 as $cell6) {
			// $pdf->Cell(-1);
			 $filename = $cell6;
			 $filepath = $imagenurl.$filename;
			$pdf->SetX(130);
			$pdf->Image($filepath,14,130,50);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();

		}

	}

		$query5 = $this->db->query("SELECT 
				A.nama_pemilik,
				A.alamat_persil,
				A.nama_perusahaan,
				B.nama_kec,
				C.nama_kel,
				D.nama_kat,
				F.nama_jenis,
				A.nomor_izin,
				A.tanggal_izin,
				E.nama_peruntukkan,
				A.luas_persil,
				A.no_persil,
				A.jumlah_tingkat,
				A.rt,
				A.rw,
				A.luas_bangunan,
				A.status_ketemu_pemilik
				FROM
				tr_detail_survey A
				LEFT JOIN ref_kategori D ON A.kat_izin=D.id_kategori
				LEFT JOIN ref_peruntukkan E ON A.id_peruntukkan=E.id_peruntukkan
				LEFT JOIN ref_kecamatan B ON A.kode_kecamatan=B.kode_kecamatan
				LEFT JOIN ref_jenis_izin F ON A.id_jenis_izin=F.id_jenis_izin
				LEFT JOIN ref_kelurahan C ON A.kode_kelurahan=C.kode_kelurahan AND A.kode_kecamatan=C.kode_kecamatan 
				WHERE
				A.kode_transaksi='".$kode_transaksi."' ");

		$data5 = array();

		foreach($query5->result() as $row5 ){
           array_push($data5, $row5);
        } 

		$header5 = array(
				array("label"=>"Nama Pemilik                 :", "length"=>20, "align"=>"L"),
				array("label"=>"Alamat Pemilik               :", "length"=>35, "align"=>"L"),
				array("label"=>"Nama Perusahaan         :", "length"=>40, "align"=>"L"),
				array("label"=>"Kecamatan                     :", "length"=>30, "align"=>"L"),
				array("label"=>"Kelurahan                      :", "length"=>30, "align"=>"L"),
				array("label"=>"Kategori                         :", "length"=>35, "align"=>"L"),
				array("label"=>"Jenis Izin                        :", "length"=>40, "align"=>"L"),
				array("label"=>"Nomor Izin                      :", "length"=>30, "align"=>"L"),
				array("label"=>"Tanggal Izin                   :", "length"=>30, "align"=>"L"),
				array("label"=>"Peruntukkan Bangunan :", "length"=>35, "align"=>"L"),
				array("label"=>"Luas Persil                    :", "length"=>40, "align"=>"L"),
				array("label"=>"Nomor Persil                  :", "length"=>30, "align"=>"L"),
				array("label"=>"Jumlah Tingakt              :", "length"=>30, "align"=>"L"),
				array("label"=>"RT                                 :", "length"=>35, "align"=>"L"),
				array("label"=>"RW                                :", "length"=>40, "align"=>"L"),
				array("label"=>"Luas Bangunan             :", "length"=>30, "align"=>"L"),
				array("label"=>"Status Ketemu Pemilik  :", "length"=>30, "align"=>"L")
			);

		$pdf->SetMargins(20,40,30,30);
		$pdf->SetXY(90, 55);

		$pdf->SetFont('Arial','B','12');
		$pdf->Cell(0,20, 'DATA PEMILIK', '0', 1, 'L');

		$pdf->SetMargins(100,40,30,30);
		$pdf->SetXY(100, 70);
		$pdf->SetFont('Arial','','10');
		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		// $pdf->SetDrawColor(0,0,0);
		// $pdf->Cell(-1);
		foreach ($header5 as $kolom5) {
			$pdf->Cell(-10);
			$pdf->Cell($kolom5['length'], 10, $kolom5['label'], 0, '-10', $kolom5['align'], true);
			$pdf->Ln();
		}
		$pdf->Ln();

		$pdf->SetFillColor(400,400,400);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
		$fill=false;


		$pdf->SetMargins(160,40,30,30);
		$pdf->SetXY(10, 70);
		foreach ($data5 as $baris5) {
		$i = 0;
		$pdf->Cell(-1);
		foreach ($baris5 as $cell5) {
			// $pdf->Cell(-1);
			$pdf->SetX(130);
			$pdf->Cell($header5[$i]['length'], 10, $cell5, 0, '-10', $kolom5['align'], $fill);
			$i++;
			$pdf->Ln();

		}
			$fill = !$fill;
			$pdf->Ln();


		}

		$pdf->SetMargins(100,220,30,30);
		$pdf->SetXY(135, 263);

		$pdf->SetFont('Arial','I','9');
		$pdf->Cell(0,0, 'Status Ketemu pemilik = 0 (tidak ketemu)', '0', 1, 'L');
		$pdf->SetXY(135, 268);
		$pdf->Cell(0,0, 'Status Ketemu pemilik = 1 (ketemu)', '0', 1, 'L');

		#output file PDF
		$pdf->Output($default_name);


	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
