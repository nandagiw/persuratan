
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row purchace-popup">
            <div class="col-12">

              <span class="d-block d-md-flex align-items-center">
                <p>Selamat datang di sistem informasi persuratan elektronik Kementerian Desa Republik Indonesia 2018.</p>
                <!-- <a class="btn ml-auto download-button d-none d-md-block" href="https://github.com/BootstrapDash/StarAdmin-Free-Bootstrap-Admin-Template" target="_blank">Download Free Version</a> -->
                <!-- <a class="btn purchase-button mt-4 mt-md-0" href="https://www.bootstrapdash.com/product/star-admin-pro/" target="_blank">Upgrade To Pro</a> -->
                <!-- <i class="mdi mdi-close popup-dismiss d-none d-md-block"></i> -->
              </span>

            </div>
          </div>

          <div class="row">
            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Grafik Jumlah Surat Masuk</h4>
                  <canvas id="barChart" style="height:230px"></canvas>
                </div>
              </div>
            </div>
            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Grafik Jumlah Surat Keluar</h4>
                  <canvas id="yuhu" style="height:230px"></canvas>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Tracking Disposisi</h4>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>No</th>

                          <th>Nomor Surat</th>
                          <th>Asal Surat</th>
                          <th>Tanggal dibuat</th>
                          <th>dibuat oleh</th>
                          <th>status tracking</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php for($i=1; $i<6 ; $i++){ ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td>841/966/REN/0<?php echo $i; ?></td>
                          <td>BIRO KEUANGAN</td>
                          <td>12 May 2017</td>
                          <td>Admin Aplikasi</td>
                          <td>
                            <label class="badge badge-danger">Pending</label>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
