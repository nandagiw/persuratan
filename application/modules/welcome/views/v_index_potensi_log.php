			<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Beranda</h1>
					</div>
					<div class="pull-right">
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="<?php echo site_url();?>welcome">Beranda</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Statistik Bangunan Berizin , Tidak Berizin & Potensi Retribusi
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content">
								<?php echo form_open('welcome/tampil_klasifikasi',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));
								?>

								<?php 							  
								$kode_kecamatan= isset($field['kode_kecamatan'])?$field['kode_kecamatan']:$this->input->post('kode_kecamatan');
							  	?>

								<select name="kode_kecamatan" id="kode_kecamatan" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >
									<option value="all">Tampil Data Kelurahan berdasarkan Kecamatan</option>
									<?php foreach($ComboKec as $row1) { ?>
	                             	<option value="<?php echo $row1["kode_kecamatan"]; ?>"><?php echo $row1['nama_kec']; ?></option>
	                            <?php } ?>
	                            </select>

				                <div>
									<br>
									<button class="btn btn-primary" type="submit" name="kecamatan">Cari</button>
								</div>

								<script src="<?php echo base_url()?>tema/amcharts/amcharts.js" type="text/javascript"></script>
								<script src="<?php echo base_url()?>tema/amcharts/serial.js" type="text/javascript"></script>
								<script src="<?php echo base_url()?>tema/amcharts/pie.js" type="text/javascript"></script>
										
								<script type="text/javascript">

									var chart = AmCharts.makeChart("chartdiv", {
								    "type": "serial",
									"theme": "light",
								    "legend": {
								        "horizontalGap": 10,
								        "maxColumns": 1,
								        "position": "right",
										"useGraphSettings": true,
										"markerSize": 10
								    },
								    "dataProvider": [
									<?php $s=1; foreach($ListData1 as $row)
													{ ?>

									{
					                    "year": "<?php echo $row['nama_kec']; ?>",
					                    "KOL1": <?php echo $row['jml_berizin']; ?>,
					                    "KOL2": <?php echo $row['jml_tidak_berizin']; ?>,
										"KOL3": <?php echo $row['jml_lahan_kosong']; ?>
								                } ,
										<?php #if ($s==$x) { echo "";} else { echo ",";} ?>
												
										<?php $s++; } ?>
									
									],
								    "valueAxes": [{
								        "stackType": "regular",
								        "axisAlpha": 0.3,
								        "gridAlpha": 0
								    }],
								    "graphs": [{
								        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
								        "fillAlphas": 0.8,
								        "labelText": "[[value]]",
								        "lineAlpha": 0.3,
								        "title": "Jumlah Berizin",
								        "type": "column",
										"color": "#000000",
								        "valueField": "KOL1"
								    }, {
								        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
								        "fillAlphas": 0.8,
								        "labelText": "[[value]]",
								        "lineAlpha": 0.3,
								        "title": "Jumlah Tidak Berizin",
								        "type": "column",
										"color": "#000000",
								        "valueField": "KOL2"
								    }, {
								        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
								        "fillAlphas": 0.8,
								        "labelText": "[[value]]",
								        "lineAlpha": 0.3,
								        "title": "Jumlah Lahan Kosong",
								        "type": "column",
										"color": "#000000",
								        "valueField": "KOL3"
								    }],
								    "categoryField": "year",
								    "categoryAxis": {
								        "gridPosition": "start",
								        "axisAlpha": 0,
								        "gridAlpha": 0,
								        "position": "left",
										"labelRotation": 45
								    },
								    "export": {
								    	"enabled": true
								     }
								});
								</script>
			   					<div id="chartdiv" style="width: 100%; height: 500px;"></div>
			   				</div>
						</div>
					</div>	
				</div>
				<br>
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Statistik Kategori Izin Berdasarkan Kecamatan
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table width="100%" class="table table-hover">
	    							<thead>
										<tr>
											<th>ID Kecamatan</th>
											<th>Nama Kecamatan</th>
											<th>Jumlah Berizin</th>
											<th>Jumlah Tidak Berizin</th>
											<th>Jumlah Lahan Kosong</th>
											<th>Jumlah Piutang</th>
			    						</tr>
									</thead>
									<tbody>
										<?php
										if (count($ListData1) > 0) {
											foreach($ListData1 as $row)
											{
												?>
										<tr>
						  					<td><?php echo $row['id_kecamatan']; ?></td>
											<td><?php echo $row['nama_kec']; ?></td>
											<td><?php echo $row['jml_berizin']; ?></td>
											<td><?php echo $row['jml_tidak_berizin']; ?></td>
											<td><?php echo $row['jml_lahan_kosong']; ?></td>
											<td><?php echo $row['jml_piutang']; ?></td>
		      							</tr>

										<?php
										
										$paging=(!empty($pagermessage) ? $pagermessage : '');
												
											}
											echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
										} else {
											echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
										}
										?>
									</tbody>
								</table>	
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
								<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Statistik Jumlah Potensi 
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content">
					
								<?php echo form_open('welcome/tampil_klasifikasi',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-validate form-wysiwyg','enctype'=>'multipart/form-data'));
								?>

								<?php 							  
								$id_param= isset($field['id_param'])?$field['id_param']:$this->input->post('id_param');
											
							  	?>
								<select name="id_param" id="id_param" class="input-xlarge" data-rule-required="true" onchange="doShow(this.value);" >

								<option value="all">Tampil Kategori Parameter</option>

								<?php foreach($ComboKat as $row2) { ?>
	                             
	                             <option value="<?php echo $row2["id_param"]; ?>" <?php if ($id_param== $row2['id_param']) { echo "selected";} ?>><?php echo $row2['nama_param']; ?></option>
	                            <?php } ?>

	                            </select>

				                <div>
									<br>
									<button class="btn btn-primary" type="submit" name="potensi">Cari</button>
								</div>
			
								<script src="<?php echo base_url()?>tema/amcharts/amcharts.js" type="text/javascript"></script>
								<script src="<?php echo base_url()?>tema/amcharts/serial.js" type="text/javascript"></script>
								<script src="<?php echo base_url()?>tema/amcharts/pie.js" type="text/javascript"></script>

								<script type="text/javascript">

									var chart = AmCharts.makeChart("chartdivpotensi", {
								    "type": "serial",
									"theme": "light",
								    "legend": {
								        "horizontalGap": 10,
								        "maxColumns": 1,
								        "position": "right",
										"useGraphSettings": true,
										"markerSize": 10
								    },
								    "dataProvider": [
									<?php $s=1; foreach($ListData1 as $row)
													{ ?>

									{
					                    "years": "<?php echo $row['nama_kec']; ?>",
					                    "KOL10": "<?php echo $row['bruto_berizin']; ?>",
					                    "KOL20": "<?php echo $row['bruto_tidakberizin']; ?>",
					                    "KOL30": "<?php echo $row['bruto_lahankosong']; ?>",
								    } ,
									<?php #if ($s==$x) { echo "";} else { echo ",";} ?>
												
									<?php $s++; } ?>
									
									],
								    "valueAxes": [{
								        "stackType": "regular",
								        "axisAlpha": 0.3,
								        "gridAlpha": 0
								    }],
								    "graphs": [{
								        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
								        "fillAlphas": 0.8,
								        "labelText": "[[value]]",
								        "lineAlpha": 0.3,
								        "title": "Total Bruto Berizin",
								        "type": "column",
										"color": "#000000",
								        "valueField": "KOL10"
								    }, {
								        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
								        "fillAlphas": 0.8,
								        "labelText": "[[value]]",
								        "lineAlpha": 0.3,
								        "title": "Total Bruto Tidak Berizin",
								        "type": "column",
										"color": "#000000",
								        "valueField": "KOL20"
								    
								    }, {
								    	"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
								        "fillAlphas": 0.8,
								        "labelText": "[[value]]",
								        "lineAlpha": 0.3,
								        "title": "Jumlah Lahan Kosong",
								        "type": "column",
										"color": "#000000",
								        "valueField": "KOL30"
								    }],
								    "categoryField": "years",
								    "categoryAxis": {
								        "gridPosition": "start",
								        "axisAlpha": 0,
								        "gridAlpha": 0,
								        "position": "left",
										"labelRotation": 45
								    },
								    "export": {
								    	"enabled": true
								     }

								});
								</script>
			   					<div id="chartdivpotensi" style="width: 100%; height: 500px;"></div>
							</div>
						</div>
					</div>
				</div>



				
				
				