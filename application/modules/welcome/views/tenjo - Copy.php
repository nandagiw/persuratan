			<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Dashboard</h1>
					</div>
					<div class="pull-right">
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Dashboard</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									Statistik Bangunan Berizin , Tidak Berizin & Potensi Retribusi
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content">
								<!-- Styles -->
							<style>
								#chartdiv {
									width: 100%;
									height: 500px;
								}
							</style>

							<!-- Resources -->
							<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
							<script src="https://www.amcharts.com/lib/3/serial.js"></script>
							<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
							<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
							<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

							<!-- Chart code -->
							<script>
							var chart = AmCharts.makeChart("chartdiv", {
							    "type": "serial",
								"theme": "light",
							    "legend": {
							        "horizontalGap": 10,
							        "maxColumns": 1,
							        "position": "right",
									"useGraphSettings": true,
									"markerSize": 10
							    },
							    "dataProvider": [
								
								<?php foreach($ListData as $row1) 	{ ?>
								{
							        "kec": "<?php echo $row1['nama_kec']; ?>",
							        "berizin": <?php echo $row1['jml_berizin']; ?>,
							        "tidak_berizin": <?php echo $row1['jml_tidak_berizin']; ?>,
							        "lahan_kosong": <?php echo $row1['jml_lahan_kosong']; ?>
							    }, 
								<?php } ?>
								
								{
							        "kec": "ANTAPANI",
							        "berizin": 3,
							        "tidak_berizin": 4,
							        "lahan_kosong": 5
							    }, {
							        "kec": "ARCAMANIK",
							        "berizin": 7,
							        "tidak_berizin": 9,
							        "lahan_kosong": 10
							    }, {
							        "kec": "ASTANA ANYAR",
							        "berizin": 3,
							        "tidak_berizin": 4,
							        "lahan_kosong": 5
							    }, {
							        "kec": "BOJONGLOA",
							        "berizin": 3,
							        "tidak_berizin": 4,
							        "lahan_kosong": 5
							    }
								
								],
							    "valueAxes": [{
							        "stackType": "regular",
							        "axisAlpha": 0.3,
							        "gridAlpha": 0
							    }],
							    "graphs": [{
							        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
							        "fillAlphas": 0.8,
							        "labelText": "[[value]]",
							        "lineAlpha": 0.3,
							        "title": "BERIZIN",
							        "type": "column",
									"color": "#000000",
							        "valueField": "berizin"
							    }, {
							        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
							        "fillAlphas": 0.8,
							        "labelText": "[[value]]",
							        "lineAlpha": 0.3,
							        "title": "TIDAK BERIZIN",
							        "type": "column",
									"color": "#000000",
							        "valueField": "tidak_berizin"
							    }, {
							        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
							        "fillAlphas": 0.8,
							        "labelText": "[[value]]",
							        "lineAlpha": 0.3,
							        "title": "POTENSI RETRIBUSI",
							        "type": "column",
									"color": "#000000",
							        "valueField": "lahan_kosong"
							    }],
							    "categoryField": "kec",
							    "categoryAxis": {
							        "gridPosition": "start",
							        "axisAlpha": 0,
							        "gridAlpha": 0,
							        "position": "left"
							    },
							    "export": {
							    	"enabled": true
							     }

							});
							</script>

							<!-- HTML -->
								<div id="chartdiv"></div>				
							</div>
						</div>
					</div>	
				</div>
			</div>
				
				
				