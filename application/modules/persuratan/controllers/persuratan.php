<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Persuratan extends CI_Controller{
//put your code here
    public function __construct() {
        parent::__construct();
		// $this->atos_tiasa_leubeut();
    }
    
    
	public function atos_tiasa_leubeut(){
		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}
    }
	
    public function index() 
    {	
		$this->template->load('rorompok','v_users');	
	}

	public function suratmasuk()
	{
		$this->template->load('rorompok','v_surat_masuk');
    }

	public function laporansuratmasuk()
	{
		$this->template->load('rorompok','v_laporan_surat_masuk');
	}
	
	public function suratkeluar()
	{
		$this->template->load('rorompok','v_surat_keluar');
    }
    
    public function laporansuratkeluar()
	{
		$this->template->load('rorompok','v_laporan_surat_keluar');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
