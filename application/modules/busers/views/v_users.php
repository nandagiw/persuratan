<div class="main-panel">
        <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Daftar Kelola Users</h4>
                  <div class="table-responsive">
                  <div class="form-group">
                      <input type="text" name="keyword" id="keyword" class="file-upload-default">
                      <div class="input-group col-lg-4">
                        <input type="text" class="form-control">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-info" type="button">Search</button>
                        </span>
                      </div>
                      <div class="float-right">
                        <button type="button" class="btn btn-success btn-fw">Tambah Data</button>
                    </div>
                    </div>
                    
                    <table class="table">
                      <thead>
                        <tr>
                          <th>No</th>

                          <th>Nama User</th>
                          <th>NIP</th>
                          <th>Nama</th>
                          <th>User Group</th>
                          <!-- <th>Tgl Register</th> -->
                          <th>Aksi</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php for($i=1; $i<6 ; $i++){ ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td>Admin Aplikasi</td>
                          <td>199405272015072001</td>
                          <td>NUR FADILAH DZULHIJJAH</td>
                          <td>Staff Pegawai</td>
                          <!-- <td>08-11-2017 03:07:19</td> -->
                          <td align="right">
                            <!-- <label class="badge badge-danger">Pending</label> -->
                            <button type="button" class="btn btn-icons btn-rounded btn-warning">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-icons btn-rounded btn-danger">
                                <i class="mdi mdi-close"></i>
                            </button>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>