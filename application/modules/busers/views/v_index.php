		<div class="container-fluid" style="margin-top: 45px;">
				<div class="page-header">
					<div class="pull-left">
						<h1>Data Pengguna</h1>
					</div>
					<div class="pull-right">
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">October 20, 2016</span>
									<span>Thursday, 11:17</span>
								</div>
							</li>
						</ul>
					</div>
				</div>

				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Setting</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo site_url();?>busers">Pengguna</a>
							<i class="icon-angle-right"></i>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
			</div>

			<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-content">
								<form action="<?php echo site_url('busers/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
									<div align="right">
		                       		<a class="btn btn-green" href="<?php echo site_url();?>busers/tambah"><i class="icon-plus-sign"></i> Tambah Data</a>
		                        	</div>
									
									<div class="control-group">
										<label class="control-label" for="textfield">Pencarian</label>
										<div class="controls">
										<input type="text" value="<?php echo $this->session->userdata('s_cari_global'); ?>" class="form-control" name="cari_global" placeholder="Masukan kata kunci..."  >	
									  	</div>
									</div>
			
									<table width="100%" class="table table-hover">
									    <thead>
											<tr>
												<th>Tipe Pengguna</th>
												<th>Username</th>
												<th>Nama</th>
												<th>Email</th>
												<th>Telp</th>
												<th>Status Aktif</th>
												<th>Aksi</th>
											    <th>&nbsp;</th>
											    <th>&nbsp;</th>
											</tr>
										</thead>
										
										<tbody>
										<?php
										if (count($ListData) > 0) {
											foreach($ListData as $row)
											{
											$enc_idx=$this->encrypt->encode($row['id']);
												$enc_idx=str_replace(array('+', '/', '='), array('-', '_', '~'), $enc_idx);
												
												?>

												<tr>
													<td>
													<?php 
													if ($row['tipe_pengguna']==1) { echo "Administrator";}  
													elseif ($row['tipe_pengguna']==2) { echo "Eksekutif";}  
													elseif ($row['tipe_pengguna']==3) { echo "Surveyor";}   
													elseif ($row['tipe_pengguna']==4) { echo "Fasilitator";}  
													
													?>											</td>
													<td><?php echo $row['nama_pengguna']; ?></td>
													<td><?php echo $row['nama_lengkap']; ?></td>
													<td><?php echo $row['email_pengguna']; ?></td>
												    <td><?php echo $row['telp_pengguna']; ?></td>
											        <td><?php echo $row['status_aktif']; ?></td>
										          <td>
													<a class="btn btn-mini btn-warning " href="<?php echo site_url();?>busers/ubah/<?php echo $row['id']; ?>"><i class="icon-pencil"></i>Ubah</a>
													</td>
											      <td>
												  <a class="btn btn-mini btn-danger" href="<?php echo site_url('busers/hapus/'.$row['id']);?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>										  
												  </td>
											      <td><a class="btn btn-mini btn-primary " href="<?php echo site_url();?>busers/index_log/<?php echo $row['id']; ?>"><i class="icon-eye-open"></i> Lihat Log</a></td>
									  			</tr>

											<?php
											
											$paging=(!empty($pagermessage) ? $pagermessage : '');
													
												}
												echo "<tr><td colspan='9'><div style='background:000; float:right;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
											} else {
												echo "<tbody><tr><td colspan='9' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
											}
											?>
										</tbody>
									</table>									
								</form>	
							</div>
						</div>
					</div>
				</div> 
