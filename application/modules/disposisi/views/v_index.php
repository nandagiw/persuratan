<div class="main-panel">
        <div class="content-wrapper">
        <div class="row">

        <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Pencarian Data</h4>
                  <form class="form-sample">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Tanggal Surat</label>
                          <div class="col-sm-9">
                          <input class="form-control" placeholder="dd/mm/yyyy" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sampai</label>
                          <div class="col-sm-9">
                          <input class="form-control" placeholder="dd/mm/yyyy" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Tanggal Terima</label>
                          <div class="col-sm-9">
                            <input class="form-control" placeholder="dd/mm/yyyy" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sampai</label>
                          <div class="col-sm-9">
                            <input class="form-control" placeholder="dd/mm/yyyy" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Keyword</label>
                          <div class="col-sm-9">
                            <input class="form-control" placeholder="ID / Nomor Surat / Perihal" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label"></label>
                          <div class="col-sm-4">
                            <div class="form-radio">
                              <label class="form-check-label">
                              <button type="submit" class="btn btn-success mr-2">Search</button>
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-5">
                            <div class="form-radio">
                              <label class="form-check-label">
                              <button class="btn btn-light">Clear</button>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                  </form>
                </div>
              </div>
            </div>
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Daftar Disposisi</h4>
                    <div class="float-right">
                        <button type="button" class="btn btn-success btn-fw">Tambah Data</button>
                    </div>
                  <div class="table-responsive">
                  <div class="form-group">
                      
                  </div>
                    
                    <table class="table">
                      <thead>
                        <tr>
                          <th>No</th>

                          <th>Nomor Surat</th>
                          <th>Asal Surat</th>
                          <th>Tanggal dibuat</th>
                          <th>dibuat oleh</th>
                          <th>status tracking</th>
                          <!-- <th>Tgl Register</th> -->
                          <th>Aksi</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php for($i=1; $i<6 ; $i++){ ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td>841/966/REN/0<?php echo $i; ?></td>
                          <td>BIRO KEUANGAN</td>
                          <td>12 May 2017</td>
                          <td>Admin Aplikasi</td>
                          <td>
                            <label class="badge badge-danger">Pending</label>
                          </td>
                          <td align="right">
                          <button type="button" class="btn btn-icons btn-rounded btn-primary">
                                <i class="mdi mdi-search-web"></i>
                            </button>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>