<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Disposisi extends CI_Controller{
//put your code here
    public function __construct() {
        parent::__construct();
		// $this->atos_tiasa_leubeut();
    }
    
    
	public function atos_tiasa_leubeut(){
		if(!$this->session->userdata('atos_tiasa_leubeut')){
			redirect('ijinmasuk');
		}
    }
	
    public function index() 
    {	
		$this->template->load('rorompok','v_index');	
    }
    
    public function trackingdisposisi()
    {
        $this->template->load('rorompok','v_tracking_disposisi');
    }

	public function laporandisposisi()
	{
		$this->template->load('rorompok','v_laporan_disposisi');
    }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
