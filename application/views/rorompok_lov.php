<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>Madani SInangkis</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- PageGuide -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/pageguide/pageguide.css">
	<!-- Fullcalendar -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/fullcalendar/fullcalendar.print.css" media="print">
	<!-- chosen -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/chosen/chosen.css">
	<!-- select2 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/select2/select2.css">
	<!-- icheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/plugins/icheck/all.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>tema/css/themes.css">


	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>tema/js/jquery.min.js"></script>
	
	
	<!-- Nice Scroll -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.draggable.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	<!-- Touch enable for jquery UI -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/touch-punch/jquery.touch-punch.min.js"></script>
	<!-- slimScroll -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url(); ?>tema/js/bootstrap.min.js"></script>
	<!-- vmap -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/vmap/jquery.vmap.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/vmap/jquery.vmap.world.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/vmap/jquery.vmap.sampledata.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Flot -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/flot/jquery.flot.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/flot/jquery.flot.bar.order.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/flot/jquery.flot.pie.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/flot/jquery.flot.resize.min.js"></script>
	<!-- imagesLoaded -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	<!-- PageGuide -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/pageguide/jquery.pageguide.js"></script>
	<!-- FullCalendar -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/fullcalendar/fullcalendar.min.js"></script>
	<!-- Chosen -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/chosen/chosen.jquery.min.js"></script>
	<!-- select2 -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/select2/select2.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/icheck/jquery.icheck.min.js"></script>

	<!-- Theme framework -->
	<script src="<?php echo base_url(); ?>tema/js/eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo base_url(); ?>tema/js/application.min.js"></script>
	<!-- Just for demonstration -->
	<script src="<?php echo base_url(); ?>tema/js/demonstration.min.js"></script>
	
	
	<script src="<?php echo base_url(); ?>tema/js/plugins/jquery-ui/jquery.ui.spinner.js"></script>
	
	
	<!-- dataTables -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/datatable/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/datatable/TableTools.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/datatable/ColReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/datatable/ColVis.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/datatable/FixedColumns.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/datatable/dataTables.scroller.min.js"></script>
	
	<!--[if lte IE 9]>
		<script src="js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>tema/img/favicon.ico" />
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>tema/img/apple-touch-icon-precomposed.png" />
	
	
	<!-- Validation -->
	<script src="<?php echo base_url(); ?>tema/js/plugins/validation/jquery.validate.min.js"></script>
	<script src="<?php echo base_url(); ?>tema/js/plugins/validation/additional-methods.min.js"></script>
	
	
	<script src="<?=base_url()?>tema/js/plugins/datepicker/bootstrap-datepicker.js"></script>
		<link rel="stylesheet" href="<?=base_url()?>tema/css/plugins/datepicker/datepicker.css">


</head>

<body>

	
	<div class="container-fluid nav-hidden" id="content">
		
		<div id="row-fluid"></div> 
		
		<div id="main" style="margin-left:0px;">
			<div class="container-fluid">
				
				
				<?php
					ini_set('memory_limit', '512M');
					echo $contents;
				?>
				
				
			
				
				
				
			</div>
		</div>
		
		</div>
		
	</body>

	</html>




