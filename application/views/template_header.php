<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Sinangkis">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>tema_depan/assets/images/discover-mobile-350x350-16.png" type="image/x-icon">
  <meta name="description" content="bootstrap carousel">
  <title>POTRET - BPPT KOTA BANDUNG</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&amp;subset=cyrillic,latin,greek,vietnamese">
  <link rel="stylesheet" href="<?php echo base_url()?>tema_depan/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>tema_depan/assets/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>tema_depan/assets/socicon/css/socicon.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>tema_depan/assets/mobirise/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>tema_depan/assets/mobirise-slider/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>tema_depan/assets/mobirise-gallery/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>tema_depan/assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
</head>
<body>

<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--transparent mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-20">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="#"><img class="mbr-navbar__brand-img mbr-brand__img" src="<?php echo base_url()?>tema_depan/assets/images/discover-mobile-350x350-53.png" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="#">Madani Sinangkis</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger text-white"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
						<ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active">
						<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="<?php echo site_url();?>">BERANDA</a></li> 
						<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="<?php echo site_url();?>">TENTANG</a></li>
						<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="<?php echo site_url();?>database">DATA REKAPITULASI</a></li>
						<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="<?php echo site_url();?>database">DATA AGREGAT RUMAH TANGGA</a></li>
						<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="<?php echo site_url();?>database">DATA AGREGAT INDIVIDU</a></li>
						<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="<?php echo site_url();?>">PETA</a></li>
						<li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="<?php echo site_url();?>">KONTAK</a></li>
						</ul>
						</div>
                        <!--<div class="mbr-navbar__column">
						<ul class="mbr-navbar__items mbr-navbar__items--right mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-inverse mbr-buttons--active">
						<li class="mbr-navbar__item"><a class="mbr-buttons__btn btn btn-default" href="https://mobirise.com/bootstrap-template/mobirise-free-template.zip">DOWNLOAD</a></li>
						</ul>
						</div>-->
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
		
	
	
<?php
					ini_set('memory_limit', '512M');
					echo $contents;
				?>
                            